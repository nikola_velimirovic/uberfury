package com.velidev.uberfury.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.velidev.uberfury.game.UberFury;

public class DesktopLauncher {
	public static void main (String[] arg) {
        //TexturePacker.Settings settings = new TexturePacker.Settings();
        //settings.maxHeight = 1024;
        //settings.maxWidth = 1024;
       // TexturePacker.process(settings, "data", "data/out", "pack");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        //config.fullscreen = true;

        config.width = 1024;
        config.height = 768;
        new LwjglApplication(new UberFury(), config);
    }
}
