package com.velidev.uberfury.gameobjects.arena;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.FixtureUserData;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;

public class Arena extends GameObject {
    protected float platformWidth, platformHeight;

    public Arena(World b2World) {
        super(b2World);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.ARENA;
    }

    public float surfaceHeight() {
        float worldHeight = body.getPosition().y;
        float surfaceHeight = worldHeight + platformHeight / 2;
        return surfaceHeight;
    }

    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        FixtureType ftype = ((FixtureUserData) fb.getUserData()).getType();
        if(ftype == FixtureType.DOWN_JUMP_SENSOR) {
            ((Player)g).cameraHandler.attachToPlatform(this);
            ((Player)g).getLegs().jumped = false;
        }
    }
}
