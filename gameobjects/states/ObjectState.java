package com.velidev.uberfury.gameobjects.states;

import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;

public class ObjectState implements Serializable {
    public Vector2 position = new Vector2();
}
