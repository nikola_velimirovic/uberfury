package com.velidev.uberfury.gameobjects;

import com.badlogic.gdx.physics.box2d.*;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.Updateable;
import com.velidev.uberfury.physics.contact.PhysicsContactListener;

/**
 * Klasa GameObject predstavlja fizicke objekat u igrici.
 */
public abstract class GameObject implements Updateable {
    /**Referenca na box2d svet. Instanca klase GameObject sadrzi ovo da bi svaki pojedinacni objekat mogao da koristi svet direktno*/
    protected World b2World;

    /**body predstavlja telo u box2d-u. Telo ima neke osobine kao sto su masa, ali ne i oblik. Nad telom se kreiraju fixture koje
     * predstavljaju oblik tela, i koje se sudaraju jedne s drugima. Telo moze biti sastavljeno iz vise fixtura.*/
    protected Body body;

    /**Ovo je default konstruktor za sad, svaki objekat zna koji je njegov "svet" u box2d-u*/
    public GameObject(World b2World) {
        this.b2World = b2World;
    }

    /**Ovu metodu implementiraju neke opstije klase, kako bi odredile kojoj grupi, tj. tipu pripada neki objekat. Npr. MeleeWeapon je
     * GameObject, implementira ovu metodu i vraca tip MELEE_WEAPON (tipovi se definisu u enumu ObjectTypes), dok njegove podklase
     * ne redefinisu dalje ovu metodu, one su sve tipa MELEE_WEAPON. Metoda se za sad koristi za userData koji se postavi fixturama,
     * kako bi znali u proveri sudara kom tipu objekta pripadaju sudarene fixture.*/
    public abstract ObjectTypes type();

    /**Objekti koje je potrebno update-tovati na svaki frame redefinisu ovu metodu.*/
    @Override
    public void update(float delta) {}

    /*Sledece 4 metode sluze za definisanje ponasanja pri sudaru*/

    /**Klasa {@link PhysicsContactListener} je nas custom listener koji se postavlja na box2d svet. On samo prosledjuje poziv metode beginContact(...)
     * GameObject-u koji je sudaren, a objekat u ovoj metodi definise ponasanje za odredjen poziv i u zavisnosti od toga s kim je sudareno.
     * Iz fixture mozemo izvuci koji je tip objekta koji je sudaren. (Fixture fb - fb.getUserData())
     * @param g predstavlja objekat s kojim se dogodio sudar
     * @param fa je fixtura ovog objekta koja je sudarena
     * @param fb je fixtura objekta s kojim smo sudareni
     * @param contact se prosledjuje iz originalnog poziva u definisanom contact listener-u ({@link com.velidev.uberfury.physics.contact.PhysicsContactListener})
     * @param first  */
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {}

    /**Klasa {@link PhysicsContactListener} je nas custom listener koji se postavlja na box2d svet. On samo prosledjuje poziv metode beginContact(...)
     * GameObject-u koji je sudaren, a objekat u ovoj metodi definise ponasanje za odredjen poziv i u zavisnosti od toga s kim je sudareno.
     * Iz fixture mozemo izvuci koji je tip objekta koji je sudaren. (Fixture fb - fb.getUserData())
     * @param g predstavlja objekat s kojim se dogodio sudar
     * @param fa je fixtura ovog objekta koja je sudarena
     * @param fb je fixtura objekta s kojim smo sudareni
     * @param contact se prosledjuje iz originalnog poziva u definisanom contact listener-u ({@link com.velidev.uberfury.physics.contact.PhysicsContactListener})
     * @param first  */
    public void endContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {}

    /**Klasa {@link PhysicsContactListener} je nas custom listener koji se postavlja na box2d svet. On samo prosledjuje poziv metode beginContact(...)
     * GameObject-u koji je sudaren, a objekat u ovoj metodi definise ponasanje za odredjen poziv i u zavisnosti od toga s kim je sudareno.
     * Iz fixture mozemo izvuci koji je tip objekta koji je sudaren. (Fixture fb - fb.getUserData())
     * @param g predstavlja objekat s kojim se dogodio sudar
     * @param fa je fixtura ovog objekta koja je sudarena
     * @param fb je fixtura objekta s kojim smo sudareni
     * @param contact se prosledjuje iz originalnog poziva u definisanom contact listener-u ({@link PhysicsContactListener})
     * @param oldManifold se prosledjuje iz originalnog poziva*/
    public void preSolve(GameObject g, Fixture fa, Fixture fb, Contact contact, Manifold oldManifold) {}

    /**Klasa {@link PhysicsContactListener} je nas custom listener koji se postavlja na box2d svet. On samo prosledjuje poziv metode beginContact(...)
     * GameObject-u koji je sudaren, a objekat u ovoj metodi definise ponasanje za odredjen poziv i u zavisnosti od toga s kim je sudareno.
     * Iz fixture mozemo izvuci koji je tip objekta koji je sudaren. (Fixture fb - fb.getUserData())
     * @param g predstavlja objekat s kojim se dogodio sudar
     * @param fa je fixtura ovog objekta koja je sudarena
     * @param fb je fixtura objekta s kojim smo sudareni
     * @param contact se prosledjuje iz originalnog poziva u definisanom contact listener-u ({@link PhysicsContactListener})
     * @param impulse se proledjuje iz originalnog poziva*/
    public void postSolve(GameObject g, Fixture fa, Fixture fb, Contact contact, ContactImpulse impulse) {}

    /**Poziva se kada neki objekat primi udarac, od melee ili ranged oruzja. U njoj definisemo sta ce se desiti sa objektom,
     * da li ce mu se npr skinuti energija ili umreti ako je Player, ili samo pustiti zvuk i varnicu ako je staticki zid.
     * @param hit sluzi da se proslede neke informacije o udarcu, npr. jacina (impulse)...*/
    public void takeAHit(Hit hit) {}

    public Body getBody() {
        return body;
    }

    public void setBody(Body b) {
        body = b;
    }

}
