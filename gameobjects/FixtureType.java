package com.velidev.uberfury.gameobjects;

/**
 * Fixtura sadrzi podatke o tome kojem tipu objekta pripada i tip same fixture ({@link FixtureUserData}). Ovo je potrebno
 * zato sto objekat moze sadrzati vise fixtura, i ovde definisemo tip same fixture radi razlikovanja kod jednog objekta.
 * Tip objekta kojem pripada fixtura definisemo u {@link ObjectTypes}
 */
public enum FixtureType {
    PLAYER_BODY, LEFT_JUMP_SENSOR, DOWN_JUMP_SENSOR, RIGHT_JUMP_SENSOR, CENTER_MELEE_RANGE_SENSOR, LEFT_MELEE_RANGE_SENSOR,
    RIGHT_MELEE_RANGE_SENSOR, UP_MELEE_RANGE_SENSOR, DOWN_MELEE_RANGE_SENSOR;
}
