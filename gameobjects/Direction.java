package com.velidev.uberfury.gameobjects;

/**
 * Razliciti smerovi prema kojima moze biti okrenut igrac ili nesto drugo.
 */
public enum Direction {
    UP, DOWN, LEFT, RIGHT;
}
