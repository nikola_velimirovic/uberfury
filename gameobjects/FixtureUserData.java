package com.velidev.uberfury.gameobjects;

/**
 * Sadrzi podatke o tipu objekta kojem pripada fixtura i tipu fixture. Postavlja se kao userData kod fixture, i izvlaci se
 * pri sudaru i proveri sudarenih objekata (fixtura).
 */
public class FixtureUserData {
    private FixtureType myType;
    private ObjectTypes ownerType;
    public int entityId;

	public FixtureUserData() {
		myType = FixtureType.DOWN_JUMP_SENSOR;
		ownerType = ObjectTypes.AMMO;
		entityId = 0;
	}

    public FixtureUserData(FixtureType fixType, ObjectTypes ownerType, int entityId) {
        this.myType = fixType;
        this.ownerType = ownerType;
        this.entityId = entityId;
    }

    public FixtureType getType() {return myType;}

    public ObjectTypes getOwnerType() {return ownerType;}

    public int getEntityId() {
        return entityId;
    }
}
