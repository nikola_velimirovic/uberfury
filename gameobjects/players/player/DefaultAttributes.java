package com.velidev.uberfury.gameobjects.players.player;

public class DefaultAttributes {
    /*Velicina tela*/
    public static final float BODY_WIDTH = 1.5f;
    public static final float BODY_HEIGHT = 1.7f;
    /*Fixture*/
    public static final float BODY_MASS = 30f;
    public static final float BODY_FIX_DENSITY = BODY_MASS / (BODY_WIDTH * BODY_HEIGHT);
    public static final float BODY_FIX_RESTITUTION = 0f;
    public static final float BODY_FIX_FRICTION = 0f;
    /*Velicine senzora*/
    /*Skok*/
    public static final float JUMP_SENSOR_WIDTH = BODY_WIDTH * 0.8f;
    public static final float JUMP_SENSOR_HEIGHT = BODY_HEIGHT / 4;
    /*Brzine*/
    public static final float MOVE_SPEED = 12f;
    public static final float JUMP_SPEED = 25f;
    /*Klizanje*/
    public static final long SLIDING_TIME = 100;
    /*Ponovno radjanje*/
    public static final long RESPAWN_TIME = 2000;
    /*Koristi se kod odbijanja od vertikalnog zida, kako bi igrac ovo vreme bio onemogucen da se krece i
     * slobodno leti. Da nema ovoga igrac se odmah zalepi za zid cim se odbije.*/
    public static final long MIN_BOUNCE_DURATION = 130;
}
