package com.velidev.uberfury.gameobjects.players.player.corpse;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.contact.ContactCategories;

public class Head extends GameObject {
    private SeveredPart severedHead;

    protected static BodyDef headBodyDef = new BodyDef();
    static {
        headBodyDef.type = BodyDef.BodyType.DynamicBody;
        headBodyDef.fixedRotation = false;
    }
    protected static CircleShape headShape = new CircleShape();
    protected static FixtureDef headFixDef = new FixtureDef();
    static {
        headFixDef.shape = headShape;
        headFixDef.density = 10f;
        headFixDef.filter.categoryBits = ContactCategories.CORPSE;
        headFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.MELEE_WEAPON;
        headFixDef.friction = 0.2f;
        headFixDef.restitution = 0.5f;
    }

    Player owner;

    public Head(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
        severedHead = new SeveredPart(b2World);
    }

    public void summon() {
        PlayerAttributes a = owner.attributes;

        if(body != null) {
            body.setUserData(Corpse.dummy);
        }
        float h = a.bodyHeight;
        float x = owner.getBody().getPosition().x;
        float y = owner.getBody().getPosition().y;
        headBodyDef.position.set(x, y + h / 2);
        body = b2World.createBody(headBodyDef);
        body.setUserData(severedHead);
        severedHead.setBody(body);

        headShape.setRadius(h * 1 / 10);
        headFixDef.shape = headShape;
        body.createFixture(headFixDef);
        body.setAngularDamping(10f);
        Corpse.activeBodyParts.add(body);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.CORPSE;
    }

    @Override
    public void takeAHit(Hit hit) {
        body.applyLinearImpulse(hit.getImpulse(), body.getWorldCenter(), true);
        hit.getMeleeWeapon().removeFromRange(severedHead);
    }
}
