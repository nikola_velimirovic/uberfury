package com.velidev.uberfury.gameobjects.players.player.corpse;

public enum LimbType {
    LEFT_ARM, RIGHT_ARM, LEFT_LEG, RIGHT_LEG;
}
