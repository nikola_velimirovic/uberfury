package com.velidev.uberfury.gameobjects.players.player.corpse;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.Updateable;
import com.velidev.uberfury.physics.contact.ContactCategories;

public class Corpse extends GameObject implements Updateable {
    protected static Array<Body> activeBodyParts = new Array<Body>();
    public static Dummy dummy;
    protected static BodyDef corpseBodyDef = new BodyDef();
    static {
        corpseBodyDef.type = BodyDef.BodyType.DynamicBody;
        corpseBodyDef.fixedRotation = false;
    }
    protected static PolygonShape torsoShape = new PolygonShape();
    protected static FixtureDef corpseFixDef = new FixtureDef();
    static {
        corpseFixDef.density = 10f;
        corpseFixDef.filter.categoryBits = ContactCategories.CORPSE;
        corpseFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLAYER;
        corpseFixDef.friction = 0.2f;
        corpseFixDef.restitution = 0.5f;
    }
    Head head;
    Limb leftArm;
    Limb rightArm;
    Limb leftLeg;
    Limb rightLeg;

    short bodyPart;

    Player owner;

    public Corpse(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
        head = new Head(b2World, owner);
        leftArm = new Limb(b2World, owner, LimbType.LEFT_ARM);
        rightArm = new Limb(b2World, owner, LimbType.RIGHT_ARM);
        leftLeg = new Limb(b2World, owner, LimbType.LEFT_LEG);
        rightLeg = new Limb(b2World, owner, LimbType.RIGHT_LEG);
        dummy = new Dummy(b2World);
    }

    private void summonTorso(float x, float y) {
        PlayerAttributes a = owner.attributes;

        float w = a.bodyWidth;
        float h = a.bodyHeight;
        corpseBodyDef.position.set(x, y);
        body = b2World.createBody(corpseBodyDef);
        body.setUserData(dummy);

        torsoShape.setAsBox(w / 10, h / 3);
        corpseFixDef.shape = torsoShape;
        body.createFixture(corpseFixDef);
        activeBodyParts.add(body);
    }

    public void summon(float x, float y) {
        bodyPart = (short) (Math.random() * 6);
        switch (bodyPart) {
            case 0:
                head.summon();
                break;
            case 1:
                leftArm.summon();
                break;
            case 2:
                rightArm.summon();
                break;
            case 3:
                leftLeg.summon();
                break;
            case 4:
                rightLeg.summon();
                break;
            case 5:
                break;
        }
        //summonTorso(x, y);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.CORPSE;
    }

    @Override
    public void update(float delta) {
        for (Body activeBodyPart : activeBodyParts) {
            Vector2 vel = activeBodyPart.getLinearVelocity();
            if(vel.x > -0.2f && vel.x < 0.2f && 0 == vel.y) {
                activeBodyPart.setUserData(Corpse.dummy);
                activeBodyPart.setActive(false);
                activeBodyParts.removeValue(activeBodyPart, true);
            }
        }
    }

    @Override
    public void takeAHit(Hit hit) {
        //trup
        body.applyLinearImpulse(hit.getImpulse(), body.getWorldCenter(), true);
        body.applyAngularImpulse((float)(Math.random() * 0.1f) - 0.001f, true);
        switch (hit.getHitDirection()) {
            case UP: case DOWN:
                float impulse = hit.getImpulse().y;
                short impulseDirection = (short)(Math.random() * 2);
                if(impulseDirection == 0) {
                    hit.setImpulse(impulse, impulse / 2);
                } else {
                    hit.setImpulse(-impulse, impulse / 2);
                }
                break;
            case LEFT: case RIGHT:
                impulse = hit.getImpulse().x;
                hit.setImpulse(impulse, hit.getImpulseSpeed() / 2);
                break;
        }
        //ostali delovi
        switch (bodyPart) {
            case 0:
                head.takeAHit(hit);
                break;
            case 1:
                leftArm.takeAHit(hit);
                break;
            case 2:
                rightArm.takeAHit(hit);
                break;
            case 3:
                leftLeg.takeAHit(hit);
                break;
            case 4:
                rightLeg.takeAHit(hit);
                break;
            case 5:
                break;
        }
    }

}
