package com.velidev.uberfury.gameobjects;

/**
 * Generalni tipovi objekata. Podklase ovih tipova se ne predstavljaju kao posebni tipovi, vec samo kao njihova nadklasa ciji je
 * tip ovde definisan. Koristi se kod sudara, da se proveri tip sudarenih objekata, jer je proveranje klase (getClass) previse specificno.
 */
public enum ObjectTypes {
    ARENA, ONE_WAY_PLATFORM, PLAYER, CORPSE, LEGS, MELEE_WEAPON, RANGED_WEAPON, AMMO
}
