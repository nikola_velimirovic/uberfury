package com.velidev.uberfury.gameobjects;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.states.ObjectState;
import com.velidev.uberfury.rendering.RenderWorld;

public abstract class RenderableObject extends GameObject {
    /**Referenca na ObjectView, koja predstavlja prikaz tela (sprite u sustini). Svako telo javlja svoje stanje view-u, ili lokalno
     * ili preko mreze, kako bi view znao da se renderuje*/
    protected RenderWorld renderWorld;

    /**Stanje koje GameObject prosledjuje ObjectView-u.*/
    protected ObjectState state;

    /**
     * Ovo je default konstruktor za sad, svaki objekat zna koji je njegov "svet" u box2d-u
     *
     * @param b2World
     */
    public RenderableObject(World b2World, RenderWorld renderWorld) {
        super(b2World);
        this.renderWorld = renderWorld;
        state = createState();
    }

    protected abstract ObjectState createState();

}
