package com.velidev.uberfury.gameobjects.weapons.ranged.ammo;

import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.contact.ContactCategories;

public class Bullet extends Ammo {
    public static Array<Bullet> toDelete = new Array<Bullet>();

    private static CircleShape shape = new CircleShape();
    static {
        shape.setRadius(0.2f);
    }
    private static FixtureDef bulletFixDef = new FixtureDef();
    static {
        bulletFixDef.density = 10;
        bulletFixDef.restitution = 0;
        bulletFixDef.shape = shape;
        bulletFixDef.filter.categoryBits = ContactCategories.AMMO;
        bulletFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLAYER;
    }

    private boolean deleted = false;
    private boolean sudar = false;

    private Hit hit;

    public Bullet(World b2World) {
        super(b2World);
    }

    public void delete() {
        b2World.destroyBody(body);
        toDelete.removeValue(this, true);
    }

    @Override
    public void fire(float x, float y, Hit hit) {
        ammoBodyDef.position.set(x, y);
        this.hit = hit;

        body = b2World.createBody(ammoBodyDef);
        body.setUserData(this);
        body.createFixture(bulletFixDef);
        body.applyLinearImpulse(hit.getImpulse(), body.getWorldCenter(), true);
        body.setGravityScale(0);
    }

    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        if(!deleted) {
            if(g.type() != ObjectTypes.AMMO) {
                toDelete.add(this);
                deleted = true;
                g.takeAHit(hit);
                return;
            }
            if(!sudar) {
                sudar = true;
            }
        }
    }
}
