package com.velidev.uberfury.gameobjects.weapons.ranged.ammo;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.weapons.Hit;

public abstract class Ammo extends GameObject {
    protected static BodyDef ammoBodyDef = new BodyDef();
    static {
        ammoBodyDef.type = BodyDef.BodyType.DynamicBody;
        ammoBodyDef.bullet = true;
        ammoBodyDef.fixedRotation = false;
    }

    public Ammo(World b2World) {
        super(b2World);
    }

    public abstract void fire(float x, float y, Hit hit);

    @Override
    public ObjectTypes type() {
        return ObjectTypes.AMMO;
    }
}
