package com.velidev.uberfury.gameobjects.weapons.ranged;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Ammo;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Bullet;

public class Gun extends RangedWeapon {
    public Gun(World b2World, Player owner) {
        super(b2World, owner);
    }

    @Override
    public Ammo loadAmmo() {
        return new Bullet(b2World);
    }
}
