package com.velidev.uberfury.gameobjects.weapons;

public enum MeleeSwingDirection {
    HORIZONTAL, VERTICAL;
}
