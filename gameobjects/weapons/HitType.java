package com.velidev.uberfury.gameobjects.weapons;

public enum HitType {
    MELEE, RANGED;
}
