package com.velidev.uberfury.gameobjects.weapons.melee;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.*;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.Updateable;
import com.velidev.uberfury.physics.contact.ContactCategories;

public abstract class MeleeWeapon extends GameObject implements Updateable {
    protected static PolygonShape weaponShape = new PolygonShape();
    protected static Vector2 weaponCenter = new Vector2();
    protected static FixtureDef weaponFix = new FixtureDef();
    static {
        weaponFix.shape = weaponShape;
        weaponFix.isSensor = true;
        weaponFix.density = 0f;
        weaponFix.filter.categoryBits = ContactCategories.MELEE_WEAPON;
        weaponFix.filter.maskBits = ContactCategories.PLAYER | ContactCategories.AMMO | ContactCategories.MELEE_WEAPON | ContactCategories.CORPSE;
    }

    protected FixtureUserData centerRangeData = new FixtureUserData(FixtureType.CENTER_MELEE_RANGE_SENSOR, type(), 0);
    protected FixtureUserData leftRangeData = new FixtureUserData(FixtureType.LEFT_MELEE_RANGE_SENSOR, type(), 0);
    protected FixtureUserData rightRangeData = new FixtureUserData(FixtureType.RIGHT_MELEE_RANGE_SENSOR, type(), 0);
    protected FixtureUserData upRangeData = new FixtureUserData(FixtureType.UP_MELEE_RANGE_SENSOR, type(), 0);
    protected FixtureUserData downRangeData = new FixtureUserData(FixtureType.DOWN_MELEE_RANGE_SENSOR, type(), 0);

    protected Array<GameObject> inRangeUp = new Array<GameObject>();
    protected Array<GameObject> inRangeDown = new Array<GameObject>();
    protected Array<GameObject> inRangeLeft = new Array<GameObject>();
    protected Array<GameObject> inRangeRight = new Array<GameObject>();
    protected Array<GameObject> inCenter = new Array<GameObject>();

    private long lastSlashTime;
    private short numOfComboSlashes;
    private boolean slashed;
    private Direction slashDirection;
    protected Hit hit = new Hit();

    protected Player owner;
    protected MeleeAttributes attributes;

    public MeleeWeapon(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
        attributes = createAttributes();
        hit.setMeleeWeapon(this);
        hit.setImpulseSpeed(attributes.hitImpulseSpeed);
    }

    protected abstract MeleeAttributes createAttributes();

    public void summon() {
        body = owner.getBody();
        PlayerAttributes pa = owner.attributes;

        float rangeLength = attributes.rangeLength;
        float rangeWidth = attributes.rangeWidth;
        float w = pa.bodyWidth;
        float h = pa.bodyHeight;
        //x i y - koordinate centara senzora u lokalnim vrednostima
        float x = (w + rangeLength) / 2;
        float y = (h + rangeLength) / 2;
        //centar
        weaponCenter.set(0, 0);
        weaponShape.setAsBox(w / 2, h / 2);
        body.createFixture(weaponFix).setUserData(centerRangeData);
        //desno
        weaponCenter.set(x, 0);
        weaponShape.setAsBox(rangeLength / 2, rangeWidth / 2, weaponCenter, 0);
        body.createFixture(weaponFix).setUserData(rightRangeData);
        //gore
        weaponCenter.set(0, y);
        weaponShape.setAsBox(rangeLength / 2, rangeWidth / 2, weaponCenter, 90 * MathUtils.degreesToRadians);
        body.createFixture(weaponFix).setUserData(upRangeData);
        //levo
        weaponCenter.set(-x, 0);
        weaponShape.setAsBox(rangeLength / 2, rangeWidth / 2, weaponCenter, 0);
        body.createFixture(weaponFix).setUserData(leftRangeData);
        //dole
        weaponCenter.set(0, -y);
        weaponShape.setAsBox(rangeLength / 2, rangeWidth / 2, weaponCenter, -90 * MathUtils.degreesToRadians);
        body.createFixture(weaponFix).setUserData(downRangeData);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.MELEE_WEAPON;
    }

    /*Ovde tek protivnik primi udarac zato sto je potrebno neko vreme (timeToReachTheTarget) da stigne udarac do mete*/
    @Override
    public void update(float delta) {
        float hitImpulseSpeed = attributes.hitImpulseSpeed;
        if(slashed) {
            if(System.currentTimeMillis() - lastSlashTime > attributes.timeToReachTheTarget) {
                slashed = false;
                hit.setHitDirection(slashDirection);
                switch (slashDirection) {
                    case UP:
                        hit.setImpulse(0, hitImpulseSpeed);
                        for (GameObject gameObject : inRangeUp) {
                            gameObject.takeAHit(hit);
                        }
                        break;
                    case DOWN:
                        hit.setImpulse(0, -hitImpulseSpeed);
                        for (GameObject gameObject : inRangeDown) {
                            gameObject.takeAHit(hit);
                        }
                        break;
                    case LEFT:
                        hit.setImpulse(-hitImpulseSpeed, 0);
                        for (GameObject gameObject : inRangeLeft) {
                            gameObject.takeAHit(hit);
                        }
                        break;
                    case RIGHT:
                        hit.setImpulse(hitImpulseSpeed, 0);
                        for (GameObject gameObject : inRangeRight) {
                            gameObject.takeAHit(hit);
                        }
                        break;
                }
            }
        }
    }

    public void removeFromRange(GameObject g) {
        inCenter.removeValue(g, true);
        inRangeUp.removeValue(g, true);
        inRangeRight.removeValue(g, true);
        inRangeDown.removeValue(g, true);
        inRangeLeft.removeValue(g, true);
    }

    public void slash(Direction direction) {
        float hitImpulseSpeed = attributes.hitImpulseSpeed;

        if(System.currentTimeMillis() - lastSlashTime < attributes.slashPeriod) return;
        slashed = true;
        slashDirection = direction;
        switch (slashDirection) {
                case UP:
                    hit.setImpulse(0, hitImpulseSpeed / 2);
                    break;
                case DOWN:
                    hit.setImpulse(0, -hitImpulseSpeed / 2);
                    break;
                case LEFT:
                    hit.setImpulse(-hitImpulseSpeed / 2, 0);
                    break;
                case RIGHT:
                    hit.setImpulse(hitImpulseSpeed / 2, 0);
                    break;
        }
        for (GameObject gameObject : inCenter) {
            gameObject.takeAHit(hit);
        }
        long previousSlashTime = lastSlashTime;
        lastSlashTime = System.currentTimeMillis();
        checkRecoil(previousSlashTime, direction);
    }

    /*Provera da li je poslednji udarac u kombu i da li treba igrac da dozivi neki trzaj*/
    private void checkRecoil(long previousSlashTime, Direction direction) {
        if(lastSlashTime - previousSlashTime < attributes.inComboTime) {
            numOfComboSlashes++;
        } else {
            numOfComboSlashes = 1;
        }
        if(numOfComboSlashes == attributes.maxNumOfComboSlashes) {
            owner.takeAPush(attributes.recoilImpulseSpeed, direction);
            numOfComboSlashes = 0;
        }
    }

    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        if(fb.isSensor()) return;
        FixtureType ftype = ((FixtureUserData) fa.getUserData()).getType();
        switch (ftype) {
            case LEFT_MELEE_RANGE_SENSOR:
                inRangeLeft.add(g);
                break;
            case RIGHT_MELEE_RANGE_SENSOR:
                inRangeRight.add(g);
                break;
            case UP_MELEE_RANGE_SENSOR:
                inRangeUp.add(g);
                break;
            case DOWN_MELEE_RANGE_SENSOR:
                inRangeDown.add(g);
                break;
            case CENTER_MELEE_RANGE_SENSOR:
                inCenter.add(g);
                break;
        }
    }

    @Override
    public void endContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        if(fb.isSensor()) return;
        FixtureType ftype = ((FixtureUserData) fa.getUserData()).getType();
        switch (ftype) {
            case LEFT_MELEE_RANGE_SENSOR:
                inRangeLeft.removeValue(g, true);
                break;
            case RIGHT_MELEE_RANGE_SENSOR:
                inRangeRight.removeValue(g, true);
                break;
            case UP_MELEE_RANGE_SENSOR:
                inRangeUp.removeValue(g, true);
                break;
            case DOWN_MELEE_RANGE_SENSOR:
                inRangeDown.removeValue(g, true);
                break;
            case CENTER_MELEE_RANGE_SENSOR:
                inCenter.removeValue(g, true);
                break;
        }
    }
}
