package com.velidev.uberfury.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.velidev.uberfury.game.Uberfury;

public class LoadingScreen extends ScreenAdapter {
	private Uberfury game;

	public LoadingScreen(Uberfury game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		GL20 gl = Gdx.graphics.getGL20();
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gl.glClearColor(0.3f, 0.498f, 1f, 1f);

		if(Uberfury.assets.manager.update()) {
			game.setScreen(Uberfury.gameScreen);
			System.out.println("updating");
		}
	}
}
