package com.velidev.uberfury.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.screens.camera.ParallaxCamera;

public class GameScreen extends ScreenAdapter {
    private SpriteBatch batch;
    public static ParallaxCamera camera;
    public static OrthographicCamera camera2;

    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    private ExtendViewport viewport;
    private ExtendViewport viewport2;

    private Player player;

	private float timeF = 0;

    private long nextTick, skipTicks = 15;
    float x = 0, y = 0;

    public GameScreen() {
        batch = Uberfury.batch;
        initCamera();
    }

    private void initCamera() {
        camera = new ParallaxCamera();
        camera2 = new OrthographicCamera();
        float size = 34f;
        float worldWidth = Uberfury.worldWidth = size;
        float worldHeight = Uberfury.worldHeight = size*(screenHeight/screenWidth);
        Uberfury.maxDistance = (float)Math.sqrt((double)(worldWidth*worldWidth + worldHeight*worldHeight));
        viewport = new ExtendViewport(worldWidth, worldHeight, camera);
        viewport2 = new ExtendViewport(worldWidth, worldHeight, camera2);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.graphics.getGL20();
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0f, 0.498f, 1f, 1f);

        handleInput(delta);

        long time = System.currentTimeMillis();

        if(nextTick == 0) {
            nextTick = time;
        }
	    camera.update();
        camera2.update();

        //gl.glViewport(0, (int)(screenHeight/2) + 20, (int)screenWidth, (int)(screenHeight/2) - 10);
        batch.setProjectionMatrix(camera.combined);
	    batch.begin();
//        while(nextTick <= time) {
//            nextTick += skipTicks;
//	        UberFury.world.setDelta(skipTicks / 1000.0f);
//			UberFury.world.process();
//        }
	    Uberfury.world.setDelta(delta);
	    Uberfury.world.process();
        Uberfury.b2World.step(delta, 8, 3);

	    batch.end();

//        gl.glViewport(0, 0, (int)screenWidth, (int)(screenHeight/2) - 10);
//        batch.setProjectionMatrix(camera2.combined);
//        batch.begin();
//        Uberfury.world.setDelta(delta);
//        Uberfury.world.process();
//
//        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        viewport2.update(width, height);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }


    private void handleInput(float delta) {
        float camMoveSpeed = 5 * delta;
        float camAccelerationFactor = 5;
        if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camMoveSpeed *= camAccelerationFactor;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (camera.position.x > 0)
                camera.translate(-camMoveSpeed, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (camera.position.x < 1024)
                camera.translate(camMoveSpeed, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            if (camera.position.y > 0)
                camera.translate(0, -camMoveSpeed, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            if (camera.position.y < 1024)
                camera.translate(0, camMoveSpeed, 0);
        }

        float camZoomSpeed = 1*delta;
        float camZoomAccelerationFactor = 5;
        if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camZoomSpeed *= camZoomAccelerationFactor;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            camera.zoom += camZoomSpeed;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.X)) {
            camera.zoom -= camZoomSpeed;
        }
    }

}
