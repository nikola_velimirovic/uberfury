package com.velidev.uberfury.screens.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.players.player.Player;

public class CameraHandler {
    private OrthographicCamera camera;
    private Player target;
    float leftBorder, rightBorder, upBorder, downBorder;
    private Vector2 windowPos = new Vector2(), targetPos, cameraPos = new Vector2();
    private float horizontalRadius, verticalRadius;
    private float leftR, upR, downR, rightR;
    private float camSpeed = 16;
    private float horizontalCamOffset, verticalCamOffset;

    private Arena platform;

    public CameraHandler(OrthographicCamera camera, float leftBorder, float rightBorder, float upBorder, float downBorder) {
        this.camera = camera;
        this.leftBorder = leftBorder;
        this.rightBorder = rightBorder;
        this.upBorder = upBorder;
        this.downBorder = downBorder;
    }

    public void setBorders(float left, float right, float up, float down) {
        leftBorder = left;
        rightBorder = right;
        upBorder = up;
        downBorder = down;
    }

    public void attachTo(Player player) {
        target = player;

        targetPos = target.getBody().getPosition();
        windowPos.set(targetPos.x, targetPos.y);

        float width = camera.viewportWidth;
        float height = camera.viewportHeight;

        horizontalRadius = leftR = rightR = width / 16;
        verticalRadius = downR = height / 4;
        upR = height / 3;

        if(windowPos.x < leftBorder + leftR) {
            windowPos.x = leftBorder + leftR;
        }

        if(windowPos.x > rightBorder - rightR) {
            windowPos.x = rightBorder - rightR;
        }

        if(windowPos.y < downBorder + downR) {
            windowPos.y = downBorder + downR;
        }

        if(windowPos.y > upBorder - upR) {
            windowPos.y = upBorder - upR;
        }

        camera.position.x = windowPos.x;
        camera.position.y = windowPos.y;
        cameraPos.set(camera.position.x, camera.position.y);

        if(cameraPos.x < leftBorder + width / 3) {
            camera.position.x = leftBorder + width / 3;
        }

        if(cameraPos.x > rightBorder - width / 3) {
            camera.position.x = rightBorder - width / 3;
        }

        if(cameraPos.y < downBorder + height / 3) {
            camera.position.y = downBorder + height / 3;
        }

        if(cameraPos.y > upBorder - height / 3) {
            camera.position.y = upBorder - height / 3;
        }

        cameraPos.set(camera.position.x, camera.position.y);

        verticalCamOffset = height / 12;

    }



    public void attachToPlatform(Arena platform) {
        float surfaceHeight = platform.surfaceHeight();
        if(surfaceHeight > windowPos.y || surfaceHeight <= windowPos.y - downR) {
            windowPos.y = surfaceHeight;
        }
    }



}
