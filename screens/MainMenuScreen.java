package com.velidev.uberfury.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.velidev.uberfury.game.Uberfury;

public class MainMenuScreen extends Stage implements Screen {
    private Uberfury igra;
    private SpriteBatch batch;
    private OrthographicCamera cam;

    public MainMenuScreen(Uberfury igra) {
        this.igra = igra;
        batch = igra.batch;
        cam = new OrthographicCamera();
    }

    @Override
    public void render(float delta) {
        
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.ENTER) {
            igra.setScreen(igra.gameScreen);
        }
        return false;
    }

}
