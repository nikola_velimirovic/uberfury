package com.velidev.uberfury.game;

import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.assets.Assets;
import com.velidev.uberfury.assets.LevelLoader;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.AnimationComponent;
import com.velidev.uberfury.entity.components.Animator;
import com.velidev.uberfury.entity.components.Health;
import com.velidev.uberfury.entity.components.Tile;
import com.velidev.uberfury.entity.components.complex.FixtureDefinition;
import com.velidev.uberfury.entity.components.complex.Shoot;
import com.velidev.uberfury.entity.components.complex.Slash;
import com.velidev.uberfury.entity.components.complex.Summon;
import com.velidev.uberfury.entity.components.complex.audio.AudioListener;
import com.velidev.uberfury.entity.components.complex.references.CameraRef;
import com.velidev.uberfury.entity.components.simple.*;
import com.velidev.uberfury.entity.components.simple.flag.Bind;
import com.velidev.uberfury.entity.components.simple.flag.CameraTarget;
import com.velidev.uberfury.entity.components.simple.flag.entities.Camera;
import com.velidev.uberfury.entity.components.simple.flag.entities.Window;
import com.velidev.uberfury.entity.components.simple.triggers.SlashTrigger;
import com.velidev.uberfury.entity.fsm.AnimatorSystem;
import com.velidev.uberfury.entity.fsm.EntityState;
import com.velidev.uberfury.entity.fsm.TC;
import com.velidev.uberfury.entity.fsm.animation.AnimatorController;
import com.velidev.uberfury.entity.managers.action.ActionFacade;
import com.velidev.uberfury.entity.managers.action.managers.*;
import com.velidev.uberfury.entity.managers.contact.*;
import com.velidev.uberfury.entity.systems.*;
import com.velidev.uberfury.entity.systems.camera.CameraShaker;
import com.velidev.uberfury.entity.systems.camera.CameraSystem;
import com.velidev.uberfury.entity.systems.camera.WindowSystem;
import com.velidev.uberfury.entity.systems.player.SummonerSystem;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.FixtureUserData;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.DefaultAttributes;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.input.KeyboardInputHandler;
import com.velidev.uberfury.input.InputHandler;
import com.velidev.uberfury.input.TouchInputHandler;
import com.velidev.uberfury.physics.contact.ContactCategories;
import com.velidev.uberfury.screens.GameScreen;
import com.velidev.uberfury.screens.LoadingScreen;
import com.velidev.uberfury.screens.MainMenuScreen;

public class Uberfury extends Game {
    public MainMenuScreen mainMenuScreen;
    public static SpriteBatch batch;
    public static GameScreen gameScreen;
	public static LoadingScreen loadingScreen;
    public static Assets assets = new Assets();

    public InputHandler inputHandler;

    public static int ID;
    public static Entity pleya;

	public static World b2World;
	public static com.artemis.World world = new com.artemis.World();
	public static com.artemis.World renderWorld = new com.artemis.World();

	public static float worldWidth;
	public static float worldHeight;
	public static float maxDistance;

	public static AudioListener audioListener;

    @Override
    public void create() {
        batch = new SpriteBatch();

	    assets.manager.load("audio/barreta.mp3", Sound.class);
	    assets.manager.load("audio/jump.mp3", Sound.class);
	    assets.manager.load("audio/fall.mp3", Sound.class);
	    assets.manager.load("audio/slash_empty.mp3", Sound.class);
	    assets.manager.load("audio/footstep_concrete.wav", Sound.class);

	    //assets.manager.finishLoading();

	    MovementManager movementManager = new MovementManager();
	    JumpManager jumpManager = new JumpManager();
	    LookManager lookManager = new LookManager();
	    MeleeManager meleeManager = new MeleeManager();
	    RangedManager rangedManager = new RangedManager();

	    world.setManager(movementManager);
	    world.setManager(jumpManager);
	    world.setManager(lookManager);
	    world.setManager(meleeManager);
	    world.setManager(rangedManager);

	    world.setManager(new BulletContactManager());
	    world.setManager(new JumpSensorsContactManager());
	    world.setManager(new FloorContactManager());
	    world.setManager(new WallContactManager());
	    world.setManager(new ContactManager());
	    world.setManager(new OneWayPlatformContactManager());

	    TagManager tagManager = new TagManager();
	    world.setManager(tagManager);

	    TC.initialize(world);

	    world.setSystem(new TileSystem());
	    world.setSystem(new SummonerSystem());
	    world.setSystem(new MovementSystem());
	    world.setSystem(new WindowBindSystem());
	    world.setSystem(new WindowSystem());
	    world.setSystem(new CameraSystem());
	    world.setSystem(new ShakeCalculator());
	    world.setSystem(new CameraShaker());
	    world.setSystem(new SlashSystem());
	    world.setSystem(new ShootSystem());
	    world.setSystem(new BulletSystem());
	    world.setSystem(new LookSystem());
	    world.setSystem(new HealthDamageSystem());
	    world.setSystem(new HealthSystem());
	    world.setSystem(new DeathSystem());
	    world.setSystem(new DestroyBodySystem());
	    world.setSystem(new MovingPlatformSystem());
	    world.setManager(new MovingPlatformManager());
	    world.setSystem(new OnPlatformSystem());
	    world.setSystem(new AnimatorSystem());
	    world.setSystem(new FloorSystem());
	    world.setSystem(new PhysicalBodyToTextureAlignerSystem());
	    world.setSystem(new AnimationSystem());
	    world.setSystem(new RenderSystem());

	    world.setSystem(new AudioSourceCatcherSystem());
	    world.setSystem(new AudioListenerSystem());

	    world.initialize();

	    createBackground();

	    gameScreen = new GameScreen();

	    LevelLoader ld = new LevelLoader(new InternalFileHandleResolver());
	    ld.load();

	    pleya = createPlayer(-5, 5);

	    Entity player2 = createPlayer(4, 5);

	    player2.edit().create(CameraTarget.class);
	    player2.edit().create(Bind.class);

	    addAnimatorTo(pleya);
	    addAnimatorTo(player2);

	    Entity cam1 = createCam(GameScreen.camera, CameraSystem.CAMERA1_TAG);
	    Entity cam2 = createCam(GameScreen.camera2, CameraSystem.CAMERA2_TAG);

	    tagManager.register(CameraSystem.CAMERA1_TAG, cam1);
	    tagManager.register("player1", pleya);
	    tagManager.register(CameraSystem.CAMERA2_TAG, cam2);
	    tagManager.register("player2", player2);

	    ActionFacade actionFacade = new ActionFacade(pleya, movementManager, meleeManager, rangedManager, jumpManager, lookManager);
	    ActionFacade actionFacade2 = new ActionFacade(player2, movementManager, meleeManager, rangedManager, jumpManager, lookManager);

	    Music wind = Gdx.audio.newMusic(Gdx.files.internal("audio/wind.wav"));
	    wind.play();
	    wind.setLooping(true);

	    Music song = Gdx.audio.newMusic(Gdx.files.internal("audio/thriller.mp3"));
	    song.play();
	    song.setVolume(0.4f);
	    song.setLooping(true);

	    loadingScreen = new LoadingScreen(this);

	    if(Gdx.app.getType() == Application.ApplicationType.Android || Gdx.app.getType() == Application.ApplicationType.iOS) {
            inputHandler = new TouchInputHandler(actionFacade);
        } else {
            inputHandler = new KeyboardInputHandler(actionFacade, actionFacade2);
        }
        Gdx.input.setInputProcessor(inputHandler);
        setScreen(loadingScreen);
    }

	//PRIVREMENE METODE
	private void createBackground() {
		Entity background = world.createEntity();

		Tile tile = new Tile();
		background.edit().add(tile);

		Texture wall = new Texture(Gdx.files.internal("tree.png"));
		tile.textureRegion = new TextureRegion(wall);
	}

	public static Entity createPlayer(float x, float y) {
		Entity player = world.createEntity();

		Summon summon = new Summon();

		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set(x, y);
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.fixedRotation = true;
		summon.bodyDef = bodyDef;

		BodyUserData bodyUserData = new BodyUserData();
		bodyUserData.contactManagerClass = JumpSensorsContactManager.class;
		bodyUserData.entity = player;
		summon.bodyUserData = bodyUserData;

		FixtureDefinition fd1 = new FixtureDefinition();

		PolygonShape bodyShape = new PolygonShape();
		FixtureDef bodyFixDef = new FixtureDef();
		bodyFixDef.shape = bodyShape;
		PlayerAttributes a = new PlayerAttributes();
        /*Ovde verovatno treba malo slozeniji oblik (shape) tela*/
		bodyShape.setAsBox(a.bodyWidth / 2, a.bodyHeight / 2);
		bodyFixDef.density = a.bodyFixDensity;
		bodyFixDef.restitution = a.bodyFixRestitution;
		bodyFixDef.friction = a.bodyFixFriction;
		bodyFixDef.filter.categoryBits = ContactCategories.PLAYER;
		bodyFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLATFORM | ContactCategories.MELEE_WEAPON | ContactCategories.AMMO;
		FixtureUserData bodyFixData = new FixtureUserData(FixtureType.PLAYER_BODY, ObjectTypes.PLAYER, player.getId());

		fd1.fixtureDef = bodyFixDef;
		fd1.fixtureUserData = bodyFixData;

		summon.fixtureDefinitions.add(fd1);

		float sensorWidth = a.bodyWidth * 0.8f;
		float sensorHeight = a.bodyHeight / 4;

		//DONJI SENZOR
		FixtureUserData downSensorData = new FixtureUserData(FixtureType.DOWN_JUMP_SENSOR, ObjectTypes.LEGS, player.getId());
		FixtureDefinition fd2 = new FixtureDefinition();

		FixtureDef sensorFixDef = new FixtureDef();
		PolygonShape sensorShape = new PolygonShape();
		Vector2 sensorCenter = new Vector2();

		sensorFixDef.isSensor = true;
		sensorFixDef.shape = sensorShape;
		sensorFixDef.filter.categoryBits = ContactCategories.SENSOR;

		sensorFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLATFORM;
		sensorCenter.set(0, -a.bodyHeight / 2);
		sensorShape.setAsBox(sensorWidth / 2, sensorHeight / 2, sensorCenter, 0);

		fd2.fixtureDef = sensorFixDef;
		fd2.fixtureUserData = downSensorData;

		summon.fixtureDefinitions.add(fd2);

		//LEVI SENZOR
		FixtureUserData leftSensorData = new FixtureUserData(FixtureType.LEFT_JUMP_SENSOR, ObjectTypes.LEGS, player.getId());
		FixtureDefinition fd3 = new FixtureDefinition();

		FixtureDef sensorFixDef1 = new FixtureDef();
		PolygonShape sensorShape1 = new PolygonShape();
		Vector2 sensorCenter1 = new Vector2();

		sensorFixDef1.isSensor = true;
		sensorFixDef1.shape = sensorShape1;
		sensorFixDef1.filter.categoryBits = ContactCategories.SENSOR;

		sensorFixDef1.filter.maskBits = ContactCategories.ARENA;
		sensorCenter1.set(-a.bodyWidth / 2, -(a.bodyHeight / 2) * 0.2f);
		sensorShape1.setAsBox(sensorWidth / 3, sensorHeight / 2, sensorCenter1, 90 * MathUtils.degreesToRadians);

		fd3.fixtureDef = sensorFixDef1;
		fd3.fixtureUserData = leftSensorData;

		summon.fixtureDefinitions.add(fd3);

		//DESNI SENZOR
		FixtureUserData rightSensorData = new FixtureUserData(FixtureType.RIGHT_JUMP_SENSOR, ObjectTypes.LEGS, player.getId());
		FixtureDefinition fd4 = new FixtureDefinition();

		FixtureDef sensorFixDef2 = new FixtureDef();
		PolygonShape sensorShape2 = new PolygonShape();
		Vector2 sensorCenter2 = new Vector2();

		sensorFixDef2.isSensor = true;
		sensorFixDef2.shape = sensorShape2;
		sensorFixDef2.filter.categoryBits = ContactCategories.SENSOR;

		sensorFixDef2.filter.maskBits = ContactCategories.ARENA;
		sensorCenter2.set(a.bodyWidth / 2, -(a.bodyHeight / 2) * 0.2f);
		sensorShape2.setAsBox(sensorWidth / 3, sensorHeight / 2, sensorCenter2, 90 * MathUtils.degreesToRadians);

		fd4.fixtureDef = sensorFixDef2;
		fd4.fixtureUserData = rightSensorData;

		summon.fixtureDefinitions.add(fd4);

		//PLAYER
		player.edit().add(summon);
		player.edit().add(new JumpConditions());
		player.edit().add(new MoveConditions());
		MoveSpeed playerMoveSpeed = new MoveSpeed(DefaultAttributes.MOVE_SPEED);
		player.edit().add(playerMoveSpeed);
		player.edit().add(new JumpSpeed(DefaultAttributes.JUMP_SPEED));
		Dimensions playerDim = new Dimensions(DefaultAttributes.BODY_WIDTH, DefaultAttributes.BODY_HEIGHT);
		player.edit().add(playerDim);
		player.edit().add(new MoveDirection(Direction.RIGHT));
		player.edit().add(player.edit().create(LookConditions.class));
		LookDirection ld = player.edit().create(LookDirection.class);
		ld.direction = Direction.RIGHT;
		player.edit().add(ld);
		Health health = player.edit().create(Health.class);
		health.value = 200;
		player.edit().add(health);
		player.edit().add(new Transform());
		player.edit().add(new com.velidev.uberfury.entity.components.simple.flag.entities.Player());
		player.edit().create(SlashTrigger.class);
		//player.edit().create(Render.class);
		Slash slash = new Slash();
		slash.duration = 400;
		slash.range = 1f;
		Damage damage = new Damage();
		damage.value = 67;
		slash.effects.add(damage);
		player.edit().add(slash);

		Shoot shoot = new Shoot();
		shoot.duration = 400;
		player.edit().add(shoot);

		player.edit().create(CameraTarget.class);
		player.edit().create(Bind.class);

		return player;
	}

	public static void addAnimatorTo(Entity e) {

		final TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("knight/out/pack.atlas"));
		TextureAtlas.AtlasRegion knight_move_left = atlas.findRegion("knight_move_left");
		TextureAtlas.AtlasRegion knight_move_right = atlas.findRegion("knight_move_right");
		TextureAtlas.AtlasRegion knight_idle_left = atlas.findRegion("knight_idle_left");
		TextureAtlas.AtlasRegion knight_idle_right = atlas.findRegion("knight_idle_right");
		TextureAtlas.AtlasRegion knight_jump_left = atlas.findRegion("knight_jump_left");
		TextureAtlas.AtlasRegion knight_jump_right = atlas.findRegion("knight_jump_right");
		TextureAtlas.AtlasRegion knight_attack_right = atlas.findRegion("knight_attack_right");
		TextureAtlas.AtlasRegion knight_attack_left = atlas.findRegion("knight_attack_left");


		//MOVE LEFT
		Array<TextureRegion> moveLeftRegions = new Array<TextureRegion>();
		for (int i = 0; i < 6; i++) {
			float frameWidth = knight_move_left.getRegionWidth() / 6;
			float frameHeight = knight_move_left.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_move_left, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			moveLeftRegions.add(region);
		}

		float moveAnimationSpeed = 1.0f/((DefaultAttributes.MOVE_SPEED/DefaultAttributes.BODY_WIDTH)*3);
		Animation animationLeft = new Animation(moveAnimationSpeed, moveLeftRegions, Animation.PlayMode.LOOP);
		AnimationComponent animationComponentLeft = new AnimationComponent();
		animationComponentLeft.animation = animationLeft;

		//MOVE RIGHT
		Array<TextureRegion> moveRightRegions = new Array<TextureRegion>();
		for (int i = 0; i < 6; i++) {
			float frameWidth = knight_move_right.getRegionWidth() / 6;
			float frameHeight = knight_move_right.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_move_right, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			moveRightRegions.add(region);
		}

		Animation animationRight = new Animation(moveAnimationSpeed, moveRightRegions, Animation.PlayMode.LOOP);
		AnimationComponent animationComponentRight = new AnimationComponent();
		animationComponentRight.animation = animationRight;

		//IDLE LEFT
		Array<TextureRegion> leftIdleRegions = new Array<TextureRegion>();
		for (int i = 0; i < 8; i++) {
			float frameWidth = knight_idle_left.getRegionWidth() / 8;
			float frameHeight = knight_idle_left.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_idle_left, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			leftIdleRegions.add(region);
		}

		float idleAnimationSpeed = 1.0f/8.0f;
		Animation animationIdleLeft = new Animation(idleAnimationSpeed, leftIdleRegions, Animation.PlayMode.LOOP);
		AnimationComponent animationComponentIdleLeft = new AnimationComponent();
		animationComponentIdleLeft.animation = animationIdleLeft;

		//IDLE RIGHT
		Array<TextureRegion> rightIdleRegions = new Array<TextureRegion>();
		for (int i = 0; i < 8; i++) {
			float frameWidth = knight_idle_right.getRegionWidth() / 8;
			float frameHeight = knight_idle_right.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_idle_right, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			rightIdleRegions.add(region);
		}

		Animation animationIdleRight = new Animation(idleAnimationSpeed, rightIdleRegions, Animation.PlayMode.LOOP);
		AnimationComponent animationComponentIdleRight = new AnimationComponent();
		animationComponentIdleRight.animation = animationIdleRight;

		//ATTACK RIGHT
		Array<TextureRegion> rightAttackRegions = new Array<TextureRegion>();
		for (int i = 0; i < 5; i++) {
			float frameWidth = knight_attack_right.getRegionWidth() / 5;
			float frameHeight = knight_attack_right.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_attack_right, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			rightAttackRegions.add(region);
		}

		Animation animationAttackRight = new Animation(1/25.0f, rightAttackRegions, Animation.PlayMode.NORMAL);
		AnimationComponent animationComponentAttackRight = new AnimationComponent();
		animationComponentAttackRight.animation = animationAttackRight;
		animationComponentAttackRight.isAtomic = true;

		//ATTACK LEFT
		Array<TextureRegion> leftAttackRegions = new Array<TextureRegion>();
		for (int i = 0; i < 5; i++) {
			float frameWidth = knight_attack_left.getRegionWidth() / 5;
			float frameHeight = knight_attack_left.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_attack_left, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			leftAttackRegions.add(region);
		}

		Animation animationAttackLeft = new Animation(1/20.0f, leftAttackRegions, Animation.PlayMode.NORMAL);
		AnimationComponent animationComponentAttackLeft = new AnimationComponent();
		animationComponentAttackLeft.animation = animationAttackLeft;
		animationComponentAttackLeft.isAtomic = true;

		//JUMP LEFT
		Array<TextureRegion> leftJumpRegions = new Array<TextureRegion>();
		for (int i = 0; i < 4; i++) {
			float frameWidth = knight_jump_left.getRegionWidth() / 4;
			float frameHeight = knight_jump_left.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_jump_left, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			leftJumpRegions.add(region);
		}

		Animation animationJumpLeft = new Animation(1/4.0f, leftJumpRegions, Animation.PlayMode.NORMAL);
		AnimationComponent animationComponentJumpLeft = new AnimationComponent();
		animationComponentJumpLeft.animation = animationJumpLeft;

		//JUMP RIGHT
		Array<TextureRegion> rightJumpRegions = new Array<TextureRegion>();
		for (int i = 0; i < 4; i++) {
			float frameWidth = knight_jump_right.getRegionWidth() / 4;
			float frameHeight = knight_jump_right.getRegionHeight();
			TextureRegion region = new TextureRegion(knight_jump_right, (int) (i*frameWidth), 0, (int) frameWidth, (int) frameHeight);
			rightJumpRegions.add(region);
		}

		Animation animationJumpRight = new Animation(idleAnimationSpeed, rightJumpRegions, Animation.PlayMode.NORMAL);
		AnimationComponent animationComponentJumpRight = new AnimationComponent();
		animationComponentJumpRight.animation = animationJumpRight;

		AnimatorController animatorController = new AnimatorController(e);

		EntityState mLeft = animatorController.addAnimationState("movingLeft", animationComponentLeft);
		EntityState mRight = animatorController.addAnimationState("movingRight", animationComponentRight);
		EntityState iLeft = animatorController.addAnimationState("idleLeft", animationComponentIdleLeft);
		EntityState iRight = animatorController.addAnimationState("idleRight", animationComponentIdleRight);
		EntityState jLeft = animatorController.addAnimationState("jumpLeft", animationComponentJumpLeft);
		EntityState jRight = animatorController.addAnimationState("jumpRight", animationComponentJumpRight);
		EntityState aRight = animatorController.addAnimationState("attackRight", animationComponentAttackRight);
		EntityState aLeft = animatorController.addAnimationState("attackLeft", animationComponentAttackLeft);

		iRight.addTransition(animatorController, mRight, TC.IdleRightToMoveRight.getInstance());
		iRight.addTransition(animatorController, mLeft, TC.IdleLeftToMoveLeft.getInstance());
		iRight.addTransition(animatorController, jRight, TC.AnyStateToJump.getInstance());
		iRight.addTransition(animatorController, aRight, TC.AnyStateToAttack.getInstance());

		iLeft.addTransition(animatorController, mLeft, TC.IdleLeftToMoveLeft.getInstance());
		iLeft.addTransition(animatorController, mRight, TC.IdleRightToMoveRight.getInstance());
		iLeft.addTransition(animatorController, jLeft, TC.AnyStateToJump.getInstance());
		iLeft.addTransition(animatorController, aLeft, TC.AnyStateToAttack.getInstance());

		mLeft.addTransition(animatorController, iLeft, TC.MoveLeftToIdleLeft.getInstance());
		mLeft.addTransition(animatorController, jLeft, TC.AnyStateToJump.getInstance());
		mLeft.addTransition(animatorController, aLeft, TC.AnyStateToAttack.getInstance());

		mRight.addTransition(animatorController, iRight, TC.MoveRightToIdleRight.getInstance());
		mRight.addTransition(animatorController, jRight, TC.AnyStateToJump.getInstance());
		mRight.addTransition(animatorController, aRight, TC.AnyStateToAttack.getInstance());

		jLeft.addTransition(animatorController, iLeft, TC.JumpToIdle.getInstance());
		jLeft.addTransition(animatorController, aLeft, TC.AnyStateToAttack.getInstance());
		jLeft.addTransition(animatorController, jRight, TC.LeftJumpToRightJump.getInstance());

		jRight.addTransition(animatorController, iRight, TC.JumpToIdle.getInstance());
		jRight.addTransition(animatorController, aRight, TC.AnyStateToAttack.getInstance());
		jRight.addTransition(animatorController, jLeft, TC.RightJumpToLeftJump.getInstance());

		aLeft.addTransition(animatorController, iLeft, TC.AttackToIdle.getInstance());
		aRight.addTransition(animatorController, iRight, TC.AttackToIdle.getInstance());

		Animator animator = new Animator();
		animator.animatorController = animatorController;
		animatorController.changeStateTo(iRight);
		e.edit().add(animator);
	}

	private Entity createCam(OrthographicCamera camera, String cameraTag) {
		float width = camera.viewportWidth;
		float height = camera.viewportHeight;

		Entity camEntity = world.createEntity();
		camEntity.edit().add(new CameraRef(camera));
		camEntity.edit().add(new Boundaries(-24, 208, 43, 0));
		Transform windowPos = new Transform();
		windowPos.x = 0;
		windowPos.y = camera.position.y / 3;
		camEntity.edit().add(windowPos);
		camEntity.edit().add(new HorizontalOffset(0));
		camEntity.edit().create(VerticalOffset.class);
		camEntity.edit().add(new Span(width/16, width/16, width/3, width/3));
		camEntity.edit().add(new Camera());
		camEntity.edit().add(new Window());
		camEntity.edit().add(new Condition());

		return camEntity;
	}

    @Override
    public void render() {
        super.render();
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            inputHandler.act();
            inputHandler.draw();
        }
    }
}