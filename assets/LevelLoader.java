package com.velidev.uberfury.assets;

import com.artemis.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.gushikustudios.rube.RubeScene;
import com.gushikustudios.rube.loader.RubeSceneLoader;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.EntityContactListener;
import com.velidev.uberfury.entity.components.MovingPlatform;
import com.velidev.uberfury.entity.components.Tile;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.managers.contact.*;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.FixtureUserData;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.physics.contact.ContactCategories;

public class LevelLoader extends AsynchronousAssetLoader<Level, LevelLoader.LevelParameter> {

    protected static RubeScene rubeScene;

    private Vector2 gravity = new Vector2(0, -60f);

	protected static RubeSceneLoader loader = new RubeSceneLoader(new World(new Vector2(), true));
	public LevelLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	public static Entity test;

	public void load() {
        rubeScene = loader.loadScene(Gdx.files.internal("test1.json"));
        World b2World = rubeScene.getWorld();

        b2World.setGravity(gravity);

        Uberfury.b2World = b2World;
		b2World.setContactListener(new EntityContactListener());

	    Texture floorWall = new Texture(Gdx.files.internal("knight/tile_floor_wall.png"));
	    TextureRegion platformReg = new TextureRegion(floorWall, 0, floorWall.getHeight()/2, floorWall.getWidth()/2, floorWall.getHeight()/2);
	    TextureRegion wallReg = new TextureRegion(floorWall, 0, floorWall.getHeight()/2, floorWall.getWidth()/2, floorWall.getHeight()/2);
		TextureRegion crateReg = new TextureRegion(new Texture(Gdx.files.internal("knight/crate.png")));

	    Tile platformTile = new Tile();
	    platformTile.textureRegion = platformReg;
	    Tile wallTile = new Tile();
	    wallTile.textureRegion = wallReg;
	    Tile crateTile = new Tile();
	    crateTile.textureRegion = crateReg;

	    Array<Body> floors = rubeScene.getNamed(Body.class, "floor");
        for (Body body : floors) {
            Entity e = Uberfury.world.createEntity();
            body.setFixedRotation(true);
            for(Fixture fix : body.getFixtureList() ){
                Filter f = fix.getFilterData();
                f.categoryBits = ContactCategories.ARENA;
                f.maskBits = ContactCategories.PLAYER | ContactCategories.SENSOR;
                fix.setFilterData(f);
                fix.setUserData(new FixtureUserData(FixtureType.CENTER_MELEE_RANGE_SENSOR, ObjectTypes.AMMO, e.getId()));
            }

	        PhysicalBody pb = new PhysicalBody(body);
            e.edit().add(pb);

	        Dimensions dim = new Dimensions(52, 1);
            e.edit().add(dim);
	        e.edit().add(platformTile);

            BodyUserData csd = new BodyUserData();
            csd.entity = e;
            csd.contactManagerClass = FloorContactManager.class;
            body.setUserData(csd);
        }

	    Array<Body> walls = rubeScene.getNamed(Body.class, "wall");
	    for (Body body : walls) {
		    Entity e = Uberfury.world.createEntity();
		    test = e;
		    body.setFixedRotation(true);
		    for(Fixture fix : body.getFixtureList() ){
			    Filter f = fix.getFilterData();
			    f.categoryBits = ContactCategories.ARENA;
			    f.maskBits = ContactCategories.PLAYER | ContactCategories.SENSOR;
			    fix.setFilterData(f);
			    fix.setUserData(new FixtureUserData(FixtureType.CENTER_MELEE_RANGE_SENSOR, ObjectTypes.AMMO, e.getId()));
		    }

		    PhysicalBody pb = new PhysicalBody(body);
		    e.edit().add(pb);

		    Dimensions dim = new Dimensions(1, 50);
		    e.edit().add(dim);
		    e.edit().add(platformTile);

		    BodyUserData csd = new BodyUserData();
		    csd.entity = e;
		    csd.contactManagerClass = WallContactManager.class;
		    body.setUserData(csd);
	    }

        Array<Body> platforms = rubeScene.getNamed(Body.class, "platform");
        for (Body b : platforms) {
            Entity e = Uberfury.world.createEntity();
            e.edit().add(new PhysicalBody(b));
            e.edit().add(new Dimensions(4.8f, 1));
	        e.edit().add(platformTile);

            BodyUserData csd = new BodyUserData();
            csd.contactManagerClass = OneWayPlatformContactManager.class;
            csd.entity = e;
            b.setUserData(csd);

            for(Fixture fix : b.getFixtureList() ){
	            Filter f = fix.getFilterData();
	            f.categoryBits = ContactCategories.PLATFORM;
	            f.maskBits = ContactCategories.PLAYER | ContactCategories.SENSOR;
	            fix.setFilterData(f);
                fix.setUserData(new FixtureUserData(FixtureType.CENTER_MELEE_RANGE_SENSOR, ObjectTypes.AMMO, e.getId()));
            }
        }

	    Array<Body> moving = rubeScene.getNamed(Body.class, "moving");
	    for (Body b : moving) {
		    Entity e = Uberfury.world.createEntity();
		    e.edit().add(new PhysicalBody(b));
		    e.edit().add(new Dimensions(4.8f, 1));
		    e.edit().add(platformTile);

		    MovingPlatform mp = new MovingPlatform();

		    mp.velocity = 4;
		    mp.range = 8;
		    mp.oldPosition = b.getPosition().x - mp.range;
		    e.edit().add(mp);

		    BodyUserData csd = new BodyUserData();
		    csd.contactManagerClass = MovingPlatformManager.class;
		    csd.entity = e;
		    b.setUserData(csd);

		    for(Fixture fix : b.getFixtureList() ){
			    fix.setUserData(new FixtureUserData(FixtureType.CENTER_MELEE_RANGE_SENSOR, ObjectTypes.AMMO, e.getId()));
		    }
	    }

	    Array<Body> crate = rubeScene.getNamed(Body.class, "crate");
	    for (Body b : crate) {
		    Entity e = Uberfury.world.createEntity();
		    e.edit().add(new PhysicalBody(b));
		    e.edit().add(new Dimensions(1, 1));
		    e.edit().add(crateTile);

		    BodyUserData csd = new BodyUserData();
		    csd.contactManagerClass = ContactManager.class;
		    csd.entity = e;
		    b.setUserData(csd);
		    b.setLinearDamping(0f);
		    b.setFixedRotation(false);

		    for(Fixture fix : b.getFixtureList() ){
			    Filter fil = fix.getFilterData();
			    fil.categoryBits = ContactCategories.PLAYER;
			    fil.maskBits = ContactCategories.ARENA | ContactCategories.PLATFORM | ContactCategories.MELEE_WEAPON;
			    fix.setFilterData(fil);
			    fix.setUserData(new FixtureUserData(FixtureType.UP_MELEE_RANGE_SENSOR, ObjectTypes.AMMO, e.getId()));
			    fix.setRestitution(0);
		    }
	    }
    }

	@Override
	public void loadAsync(AssetManager assetManager, String s, FileHandle fileHandle, LevelParameter levelParameter) {

	}

	@Override
	public Level loadSync(AssetManager assetManager, String s, FileHandle fileHandle, LevelParameter levelParameter) {
		return null;
	}

	@Override
	public Array<AssetDescriptor> getDependencies(String s, FileHandle fileHandle, LevelParameter levelParameter) {
		return null;
	}

	public static class LevelParameter extends AssetLoaderParameters<Level> {
	}
}
