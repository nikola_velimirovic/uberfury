package com.velidev.uberfury.entity;

import com.badlogic.gdx.physics.box2d.*;
import com.velidev.uberfury.entity.managers.contact.ContactManager;
import com.velidev.uberfury.game.Uberfury;

public class EntityContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        BodyUserData sa = (BodyUserData) fa.getBody().getUserData();
        BodyUserData sb = (BodyUserData) fb.getBody().getUserData();

        ContactManager esa = Uberfury.world.getManager(sa.contactManagerClass);
        ContactManager esb = Uberfury.world.getManager(sb.contactManagerClass);

        esa.beginContact(fa, fb, contact);
        esb.beginContact(fb, fa, contact);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        BodyUserData sa = (BodyUserData) fa.getBody().getUserData();
        BodyUserData sb = (BodyUserData) fb.getBody().getUserData();

        ContactManager esa = Uberfury.world.getManager(sa.contactManagerClass);
        ContactManager esb = Uberfury.world.getManager(sb.contactManagerClass);

        esa.endContact(fa, fb, contact);
        esb.endContact(fb, fa, contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        BodyUserData sa = (BodyUserData) fa.getBody().getUserData();
        BodyUserData sb = (BodyUserData) fb.getBody().getUserData();

        ContactManager esa = Uberfury.world.getManager(sa.contactManagerClass);
        ContactManager esb = Uberfury.world.getManager(sb.contactManagerClass);

        esa.preSolve(fa, fb, contact, oldManifold);
        esb.preSolve(fb, fa, contact, oldManifold);
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();

        BodyUserData sa = (BodyUserData) fa.getBody().getUserData();
        BodyUserData sb = (BodyUserData) fb.getBody().getUserData();

        ContactManager esa = Uberfury.world.getManager(sa.contactManagerClass);
        ContactManager esb = Uberfury.world.getManager(sb.contactManagerClass);

        esa.postSolve(fa, fb, contact, impulse);
        esb.postSolve(fb, fa, contact, impulse);
    }
}
