package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.velidev.uberfury.entity.components.Tile;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class TileSystem extends EntityProcessingSystem {
	private ComponentMapper<Tile> mTile;
	public TileSystem() {
		super(Aspect.getAspectForAll(Tile.class).exclude(Dimensions.class));
	}

	@Override
	protected void process(Entity e) {
		Tile tile = mTile.get(e);
		TextureRegion reg = tile.textureRegion;

		SpriteBatch batch = Uberfury.batch;

		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();

		for(int i=-50; i<60; i++) {
			for(int j=-10; j<60; j++) {
				float x = i;
				float y = j;
				batch.draw(reg, x, y, 2, 2);
			}
		}
	}
}
