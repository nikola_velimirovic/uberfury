package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.complex.Render;
import com.velidev.uberfury.entity.components.complex.Shoot;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Bullet;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.LookDirection;
import com.velidev.uberfury.entity.components.simple.Transform;
import com.velidev.uberfury.entity.managers.contact.BulletContactManager;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.contact.ContactCategories;

@Wire
public class ShootSystem extends EntityProcessingSystem {
	private ComponentMapper<Shoot> mShoot;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<LookDirection> mLookDirection;
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	private ComponentMapper<AudioSource> mAudioSource;

	public static final float GUN_DISTANCE_FROM_BODY = 0.1f;
	public static final float DEFAULT_BULLET_SPEED = 200f;
	public static final float BULLET_SIZE = 0.3f;

	protected Hit hit = new Hit();
	protected float hitImpulseSpeed = DEFAULT_BULLET_SPEED;

	TextureRegion region = new TextureRegion(new Texture(Gdx.files.internal("bullet2.png")));

	public ShootSystem() {
		super(Aspect.getAspectForAll(Shoot.class, Dimensions.class, LookDirection.class, PhysicalBody.class));
	}

	@Override
	protected void process(Entity e) {
		Shoot shoot = mShoot.get(e);

		if(!shoot.isShooting) {
			return;
		}
		if(System.currentTimeMillis() < shoot.time + shoot.duration) {
			return;
		}

		shoot.time = System.currentTimeMillis();

		Dimensions dimensions = mDimensions.get(e);
		LookDirection lookDirection = mLookDirection.get(e);
		Body shooterBody = mPhysicalBody.get(e).body;

		final BodyDef ammoBodyDef = new BodyDef();
		ammoBodyDef.type = BodyDef.BodyType.DynamicBody;
		ammoBodyDef.bullet = true;
		ammoBodyDef.fixedRotation = false;

		final CircleShape shape = new CircleShape();
		shape.setRadius(BULLET_SIZE);

		final FixtureDef bulletFixDef = new FixtureDef();

		bulletFixDef.density = 10;
		bulletFixDef.restitution = 0;
		bulletFixDef.shape = shape;
		bulletFixDef.filter.categoryBits = ContactCategories.AMMO;
		bulletFixDef.filter.maskBits = ContactCategories.PLAYER;
		bulletFixDef.isSensor = true;

		float x = 0f;
		float y = 0f;
		float width = dimensions.width;
		float height = dimensions.height;

		Transform transform = new Transform();
		switch (lookDirection.direction) {
			case UP:
				transform.rotation = 270;
				x = shooterBody.getPosition().x;
				y = shooterBody.getPosition().y + height / 2 + (GUN_DISTANCE_FROM_BODY + BULLET_SIZE);
				hit.setImpulse(0, hitImpulseSpeed);
				break;
			case DOWN:
				transform.rotation = 90;
				x = shooterBody.getPosition().x;
				y = shooterBody.getPosition().y - height / 2 - (GUN_DISTANCE_FROM_BODY + BULLET_SIZE);
				hit.setImpulse(0, -hitImpulseSpeed);
				break;
			case LEFT:
				transform.rotation = 0;
				x = shooterBody.getPosition().x - width / 2 - (GUN_DISTANCE_FROM_BODY + BULLET_SIZE);
				y = shooterBody.getPosition().y;
				hit.setImpulse(-hitImpulseSpeed, 0);
				break;
			case RIGHT:
				transform.rotation = 180;
				x = shooterBody.getPosition().x + width / 2 + (GUN_DISTANCE_FROM_BODY + BULLET_SIZE);
				y = shooterBody.getPosition().y;
				hit.setImpulse(hitImpulseSpeed, 0);
				break;
		}

		ammoBodyDef.position.set(x, y);

		Entity bul = Uberfury.world.createEntity();

		Body bullet = Uberfury.b2World.createBody(ammoBodyDef);
		BodyUserData bodyUserData = new BodyUserData();
		bodyUserData.entity = bul;
		bodyUserData.contactManagerClass = BulletContactManager.class;
		bullet.setUserData(bodyUserData);

		bullet.createFixture(bulletFixDef);
		bullet.applyLinearImpulse(hit.getImpulse(), bullet.getWorldCenter(), true);
		bullet.setGravityScale(0);

		PhysicalBody pBullet = new PhysicalBody();
		pBullet.body = bullet;
		bul.edit().add(pBullet);

		Dimensions dim = new Dimensions();
		dim.height = 0.4f;
		dim.width = 0.4f;
		bul.edit().add(dim);


		transform.x = bullet.getPosition().x;
		transform.y = bullet.getPosition().y;
		bul.edit().add(transform);

		Render render = new Render();
		render.region = region;
		bul.edit().add(render);

		Bullet b = new Bullet();
		b.range = 10;
		b.xStart = bullet.getPosition().x;
		b.yStart = bullet.getPosition().y;
		bul.edit().add(b);

		AudioSource audioSource = mAudioSource.get(e);
		for(SoundComponent soundComponent : audioSource.soundComponents) {
			if(soundComponent.tag == AudioTag.SHOOT) {
				soundComponent.soundId = soundComponent.sound.play(0);
				break;
			}
		}
	}
}
