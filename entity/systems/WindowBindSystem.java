package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.simple.flag.Bind;
import com.velidev.uberfury.entity.components.simple.flag.CameraTarget;
import com.velidev.uberfury.entity.systems.camera.CameraSystem;
import com.velidev.uberfury.entity.systems.camera.WindowSystem;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class WindowBindSystem extends EntityProcessingSystem {
	private ComponentMapper<CameraTarget> targetMapper;

    public WindowBindSystem() {
        super(Aspect.getAspectForAll(CameraTarget.class, Bind.class));
    }

    @Override
    public void process(Entity target) {
        Entity cam = null;
        TagManager tagManager = Uberfury.world.getManager(TagManager.class);
        if(target.getId() == tagManager.getEntity("player1").getId()) {
            cam = tagManager.getEntity(CameraSystem.CAMERA1_TAG);
        } else {
            cam = tagManager.getEntity(CameraSystem.CAMERA2_TAG);
        }

        if(cam == null) return;

	    Uberfury.world.getSystem(WindowSystem.class).bind(cam, target);

        target.edit().remove(Bind.class);
    }
}
