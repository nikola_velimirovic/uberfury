package com.velidev.uberfury.entity.systems;

import com.artemis.*;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.complex.Slash;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.LookDirection;
import com.velidev.uberfury.entity.components.simple.triggers.SlashTrigger;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.FixtureUserData;

@Wire
public class SlashSystem extends EntityProcessingSystem {
	private ComponentMapper<Slash> mSlash;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<LookDirection> mDirection;
	private ComponentMapper<SlashTrigger> mSlashTrigger;

	private ComponentMapper<AudioSource> mAudioSource;

	float impulseX;

	private Slash slash;
//	Sound hurtSound = Uberfury.assets.manager.get("audio/fall.mp3", Sound.class);

	public SlashSystem() {
		super(Aspect.getAspectForAll(PhysicalBody.class, Dimensions.class, LookDirection.class, Slash.class, SlashTrigger.class));
	}

	@Override
	public void process(final Entity e) {
		slash = mSlash.get(e);
		SlashTrigger slashTrigger = mSlashTrigger.get(e);

		if(slash.isSlashing == false) {
			return;
		}
		if(System.currentTimeMillis() < slash.time + slash.duration) {
			return;
		}

		slashTrigger.slashed = true;
		slash.time = System.currentTimeMillis();

		World b2World = Uberfury.b2World;
		Vector2 bodyPosition = mPhysicalBody.get(e).body.getPosition();
		Dimensions playerDim = mDimensions.get(e);
		Direction lookDirection = mDirection.get(e).direction;
		float bx = bodyPosition.x;
		float by = bodyPosition.y;

		float lowerX = bx, upperX = bx;
		float lowerY = by, upperY = by;

		impulseX = 0;

		switch (lookDirection) {
			case UP:
				upperY = by + playerDim.height/2 + slash.range;
				lowerX = bx - playerDim.width/2;
				upperX = bx + playerDim.width/2;
				break;
			case DOWN:
				lowerY = by - playerDim.height/2 - slash.range;
				lowerX = bx - playerDim.width/2;
				upperX = bx + playerDim.width/2;
				break;
			case LEFT:
				lowerX = bx - playerDim.width/2 - slash.range;
				lowerY = by - playerDim.height/2;
				upperY = by + playerDim.height/2;
				impulseX = -10;
				break;
			case RIGHT:
				upperX = bx + playerDim.width/2 + slash.range;
				lowerY = by - playerDim.height/2;
				upperY = by + playerDim.height/2;
				impulseX = 10;
				break;
		}

		b2World.QueryAABB(new QueryCallback() {
			@Override
			public boolean reportFixture(Fixture fixture) {
				Entity target = ((BodyUserData) fixture.getBody().getUserData()).entity;
				if (target.getId() == e.getId()) {
					return true;
				}
				if(fixture.getUserData() == null) return true;
				boolean changed = false;
				EntityEdit edit = target.edit();
				for (Component c : slash.effects) {
					edit.add(c);
					changed = true;
				}
				if (((FixtureUserData) fixture.getUserData()).getType() == FixtureType.UP_MELEE_RANGE_SENSOR) {
					Body body = target.getComponent(PhysicalBody.class).body;
					body.applyLinearImpulse(SlashSystem.this.impulseX, 12, body.getWorldCenter().x, body.getWorldCenter().y, true);
					body.applyAngularImpulse(20f, true);
				} else if (((FixtureUserData) fixture.getUserData()).getType() == FixtureType.PLAYER_BODY) {
					//hurtSound.play();
				}
				return true;
			}
		}, lowerX, lowerY, upperX, upperY);

		AudioSource audioSource = mAudioSource.get(e);
		for(SoundComponent soundComponent : audioSource.soundComponents) {
			if(soundComponent.tag == AudioTag.SLASH) {
				soundComponent.soundId = soundComponent.sound.play(0);
				break;
			}
		}
	}
}
