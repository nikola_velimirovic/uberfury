package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.velidev.uberfury.entity.components.simple.Shake;

@Wire
public class ShakeCalculator extends EntityProcessingSystem {
	private ComponentMapper<Shake> shakeMapper;

	public ShakeCalculator() {
		super(Aspect.getAspectForAll(Shake.class));
	}

	@Override
	public void process(Entity e) {
		Shake shake = shakeMapper.get(e);

		if(shake.radius == 0) {
			shake.radius = shake.maxRadius;
		}

		if(shake.angle < 0) {
			shake.angle = (float) Math.random()*180;
		} else {
			shake.angle = (float) Math.random()*180*(-1);
		}

		shake.dx = shake.radius* MathUtils.cosDeg(shake.angle);
		shake.dy = shake.radius* MathUtils.sinDeg(shake.angle);

		shake.radius *= 0.9f;
		if(shake.radius < 0.01f) {
			shake.radius = 0;
			e.removeComponent(Shake.class);
			e.changedInWorld();
		}
	}
}
