package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Bullet;
import com.velidev.uberfury.entity.components.simple.flag.DestroyBody;

@Wire
public class BulletSystem extends EntityProcessingSystem {
	private ComponentMapper<Bullet> mBullet;
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	public BulletSystem() {
		super(Aspect.getAspectForAll(Bullet.class, PhysicalBody.class));
	}

	@Override
	protected void process(Entity e) {
		Bullet bullet = mBullet.get(e);
		Vector2 curPos = mPhysicalBody.get(e).body.getPosition();

		if(bullet.xStart + bullet.range < curPos.x) e.edit().create(DestroyBody.class);
		else
		if(bullet.xStart - bullet.range > curPos.x) e.edit().create(DestroyBody.class);
		else
		if(bullet.yStart + bullet.range < curPos.y) e.edit().create(DestroyBody.class);
		else
		if(bullet.yStart - bullet.range > curPos.y) e.edit().create(DestroyBody.class);
	}
}
