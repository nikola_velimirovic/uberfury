package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.flag.Dead;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.input.KeyboardInputHandler;

@Wire
public class DeathSystem extends EntityProcessingSystem {
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	public DeathSystem() {
		super(Aspect.getAspectForAll(Dead.class, PhysicalBody.class));
	}

	@Override
	public void process(Entity e) {
		Body body = mPhysicalBody.get(e).body;
		float x = body.getPosition().x;
		float y = body.getPosition().y;
		Uberfury.b2World.destroyBody(body);

		TagManager tagManager = Uberfury.world.getManager(TagManager.class);

		if(tagManager.getEntity("player1").getId() == e.getId()) {
			Entity player = Uberfury.createPlayer(-5, 5);
			tagManager.register("player1", player);
			Uberfury.addAnimatorTo(player);
			KeyboardInputHandler.getF1().setPlayer(player);
		} else {
			int xx = MathUtils.random(-5, 5);
			Entity player = Uberfury.createPlayer(xx, 10);
			tagManager.register("player2", player);
			Uberfury.addAnimatorTo(player);
			//DesktopInputHandler.getF2().setPlayer(player);
		}
		e.edit().deleteEntity();
	}
}
