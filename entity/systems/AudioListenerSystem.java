package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.complex.audio.AudioListener;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;

@Wire
public class AudioListenerSystem extends EntityProcessingSystem {
	private ComponentMapper<AudioListener> mAudioListener;
	
	public AudioListenerSystem() {
		super(Aspect.getAspectForAll(AudioListener.class));
	}

	@Override
	protected void process(Entity e) {
		AudioListener audioListener = mAudioListener.get(e);

		for(SoundComponent soundComponent : audioListener.caughtSounds) {
			soundComponent.sound.setVolume(soundComponent.soundId, soundComponent.volume);
		}
	}
}
