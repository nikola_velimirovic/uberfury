package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.Tile;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class FloorSystem extends EntityProcessingSystem {
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<Tile> mTile;

	public FloorSystem() {
		super(Aspect.getAspectForAll(PhysicalBody.class, Dimensions.class, Tile.class));
	}

	@Override
	protected void process(Entity e) {
		Body body = mPhysicalBody.get(e).body;
		Dimensions dim = mDimensions.get(e);
		TextureRegion reg = mTile.get(e).textureRegion;

		SpriteBatch batch = Uberfury.batch;

		if(dim.height == 50) {
			batch.draw(reg, body.getPosition().x - dim.width, body.getPosition().y - dim.height / 8, dim.width, dim.height);
		} else if(dim.width > 1) {
			batch.draw(reg, body.getPosition().x - dim.width / 2, body.getPosition().y - dim.height, dim.width, dim.height);
		} else {
			batch.draw(reg, body.getPosition().x - dim.width / 2, body.getPosition().y - dim.height / 2, dim.width, dim.height);
		}

	}
}
