package com.velidev.uberfury.entity.systems.camera;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.velidev.uberfury.entity.components.complex.references.CameraRef;
import com.velidev.uberfury.entity.components.simple.Shake;
import com.velidev.uberfury.entity.components.simple.flag.entities.Camera;

@Wire
public class CameraShaker extends EntityProcessingSystem {
	private ComponentMapper<CameraRef> cameraMapper;
	private ComponentMapper<Shake> shakeMapper;
	
	public CameraShaker() {
		super(Aspect.getAspectForAll(Camera.class, Shake.class));
	}

	@Override
	public void process(Entity e) {
		OrthographicCamera camera = cameraMapper.get(e).camera;
		Shake shake = shakeMapper.get(e);

		camera.translate(shake.dx, shake.dy);
	}
}
