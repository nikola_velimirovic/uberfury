package com.velidev.uberfury.entity.systems.camera;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.complex.references.CameraRef;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.*;
import com.velidev.uberfury.entity.components.simple.flag.CameraTarget;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class CameraSystem extends EntityProcessingSystem {
	public static final String CAMERA1_TAG = "CAMERA1";
	public static final String CAMERA2_TAG = "CAMERA2";

	private ComponentMapper<Transform> mPosition;
	private ComponentMapper<Span> mSpan;
	private ComponentMapper<Condition> mCondition;
	private ComponentMapper<CenterWindow> mCenterWindow;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<CameraRef> mCameraRef;
	private ComponentMapper<HorizontalOffset> mHorizontalOffset;
	private ComponentMapper<VerticalOffset> mVerticalOffset;
	private ComponentMapper<MoveSpeed> mMoveSpeed;
	private ComponentMapper<Boundaries> mBoundaries;

	public CameraSystem() {
		super(Aspect.getAspectForAll(CameraTarget.class));
	}

	@Override
	public void process(Entity target) {
		float deltaTime = world.delta;

		//target
		Body targetBody = mPhysicalBody.get(target).body;
		Vector2 targetPos = targetBody.getPosition();
		Dimensions targetDim = mDimensions.get(target);
		MoveSpeed moveSpeed = mMoveSpeed.get(target);

		//camera
		Entity cam = null;
		TagManager tagManager = Uberfury.world.getManager(TagManager.class);
		if(target.getId() == tagManager.getEntity("player1").getId()) {
			cam = tagManager.getEntity(CAMERA1_TAG);
		} else {
			cam = tagManager.getEntity(CAMERA2_TAG);
		}


		OrthographicCamera camera = mCameraRef.get(cam).camera;
		float width = camera.viewportWidth;
		float height = camera.viewportHeight;
		HorizontalOffset horOff = mHorizontalOffset.get(cam);
		VerticalOffset verOff = mVerticalOffset.get(cam);
		float camSpeed = moveSpeed.value*2f;

		//world boundaries
		Boundaries boundaries = mBoundaries.get(cam);

		//window
		Transform windowPos = mPosition.get(cam);
		Span windowSpan = mSpan.get(cam);
		Condition toMoveUp = mCondition.get(cam);

		float dx = (windowPos.x + horOff.value) - camera.position.x;
		float velX = targetBody.getLinearVelocity().x;
		if (dx != 0) {
			if ((targetPos.x - targetDim.width/2 <= windowPos.x - windowSpan.left) && Math.abs(velX) > camSpeed) {
				camSpeed = -2*velX;
			} else if ((targetPos.x + targetDim.width/2 >= windowPos.x + windowSpan.right) && Math.abs(velX) > camSpeed) {
				camSpeed = 2*velX;
			}
			if (horOff.value != 0) {
				if (horOff.value > 0) {
					if (velX <= 0.1f && velX >= 0 && !((targetPos.x + targetDim.width/2) - (windowPos.x + windowSpan.right) < -0.2f)) {
						camSpeed = moveSpeed.value;
					} else if (velX <= -0.1f || velX == 0) {
						camSpeed = 0;
					}
					if (dx > camSpeed*deltaTime) {
						dx = camSpeed*deltaTime;
					}
				} else {
					if (velX >= -0.1f && velX <= 0 && !((targetPos.x - targetDim.width/2) - (windowPos.x - windowSpan.left) > 0.2f)) {
						camSpeed = moveSpeed.value;
					} else if (velX >= 0.1f || velX == 0) {
						camSpeed = 0;
					}
					if (dx < -camSpeed*deltaTime) {
						dx = -camSpeed*deltaTime;
					}
				}
				boolean leftBorderCheck = camera.position.x + dx > boundaries.left + width/3;
				boolean rightBorderCheck = camera.position.x + dx < boundaries.right - width/3;
				if (!leftBorderCheck) {
					dx = boundaries.left + width/3 - camera.position.x;
					horOff.value = 0;
				} else if (!rightBorderCheck) {
					dx = boundaries.right - width/3 - camera.position.x;
					horOff.value = 0;
				}
			} else {
				dx = 0;
			}
		}

		camSpeed = moveSpeed.value;
		float dy = (windowPos.y + verOff.value) - camera.position.y;
		float velY = targetBody.getLinearVelocity().y;
		if (velY < 0 && dy > 0) {
			dy = 0;
			toMoveUp.value = false;
		} else if (velY == 0 && dy > 0 && toMoveUp.value == false && verOff.value == 0) {
			dy = 0;
		}
		if (dy != 0) {
			if ((dy > 0 && velY > 0) || (dy>0 && verOff.value != 0)) {
				toMoveUp.value = true;
			}
			if (targetPos.y - targetDim.height/2 <= windowPos.y - windowSpan.down) {
				camSpeed = -2*velY;
			} else if (targetPos.y + targetDim.height/2 >= windowPos.y + windowSpan.up) {
				camSpeed = 2*velY;
			}
			if (dy > camSpeed*deltaTime) dy = camSpeed*deltaTime;
			if (dy < -camSpeed*deltaTime) dy = -camSpeed*deltaTime;
			boolean downBorderCheck = camera.position.y + dy >= boundaries.down + height/3;
			boolean upBorderCheck = camera.position.y + dy <= boundaries.up - height/3;
			if (!downBorderCheck) {
				dy = boundaries.down + height/3 - camera.position.y;
			} else if (!upBorderCheck) {
				dy = boundaries.up - height/3 - camera.position.y;
			}
		}

		if (dx != 0 || dy != 0) {
			camera.translate(dx, dy);
		}
	}
}
