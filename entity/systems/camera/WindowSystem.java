package com.velidev.uberfury.entity.systems.camera;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.complex.references.CameraRef;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.*;
import com.velidev.uberfury.entity.components.simple.flag.CameraTarget;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.Direction;

@Wire
public class WindowSystem extends EntityProcessingSystem {
	private ComponentMapper<Transform> mPosition;
	private ComponentMapper<Span> mSpan;
	private ComponentMapper<Condition> mCondition;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<CameraRef> mCameraRef;
	private ComponentMapper<HorizontalOffset> mHorizontalOffset;
	private ComponentMapper<VerticalOffset> mVerticalOffset;
	private ComponentMapper<LookDirection> mLookDirection;

	public WindowSystem() {
		super(Aspect.getAspectForAll(CameraTarget.class));
	}

	public void bind(Entity cam, Entity target) {
		//world boundaries
		Boundaries boundaries = cam.getComponent(Boundaries.class);

		//camera
		OrthographicCamera camera = cam.getComponent(CameraRef.class).camera;
		float width = camera.viewportWidth;
		float height = camera.viewportHeight;

		//target
		Body targetBody = target.getComponent(PhysicalBody.class).body;
		Vector2 targetPos = targetBody.getPosition();

		//window
		Transform windowPos = cam.getComponent(Transform.class);
		Span windowSpan = cam.getComponent(Span.class);

		windowPos.x = targetPos.x;
		windowPos.y = targetPos.y;

		windowSpan.left = windowSpan.right = width/16;
		windowSpan.down = height/4;
		windowSpan.up = height/3;

		if (windowPos.x < boundaries.left + windowSpan.left) {
			windowPos.x = boundaries.left + windowSpan.left;
		}

		if (windowPos.x > boundaries.right - windowSpan.right) {
			windowPos.x = boundaries.right - windowSpan.right;
		}

		if (windowPos.y < boundaries.down + windowSpan.down) {
			windowPos.y = boundaries.down + windowSpan.down;
		}

		if (windowPos.y > boundaries.up - windowSpan.up) {
			windowPos.y = boundaries.up - windowSpan.up;
		}

		camera.position.x = windowPos.x;
		camera.position.y = windowPos.y;

		if (camera.position.x < boundaries.left + width/3) {
			camera.position.x = boundaries.left + width/3;
		}

		if (camera.position.x > boundaries.right - width/3) {
			camera.position.x = boundaries.right - width/3;
		}

		if (camera.position.y < boundaries.down + height/3) {
			camera.position.y = boundaries.down + height/3;
		}

		if (camera.position.y > boundaries.up - height/3) {
			camera.position.y = boundaries.up - height/3;
		}
	}

	public void centerOn(Entity player, float height) {
		Entity cam = null;
		TagManager tagManager = Uberfury.world.getManager(TagManager.class);
		if(player.getId() == tagManager.getEntity("player1").getId()) {
			cam = tagManager.getEntity(CameraSystem.CAMERA1_TAG);
		} else {
			cam = tagManager.getEntity(CameraSystem.CAMERA2_TAG);
		}

		Transform windowPos = mPosition.get(cam);
		Condition toMoveUp = mCondition.get(cam);

		if (windowPos.y != height) {
			toMoveUp.value = false;
		}
		windowPos.y = height;
		cam.edit().remove(CenterWindow.class);
	}

	@Override
	public void process(Entity target) {
		Entity cam = null;
		TagManager tagManager = Uberfury.world.getManager(TagManager.class);
		if(target.getId() == tagManager.getEntity("player1").getId()) {
			cam = tagManager.getEntity(CameraSystem.CAMERA1_TAG);
		} else {
			cam = tagManager.getEntity(CameraSystem.CAMERA2_TAG);
		}

		//window
		Transform windowPos = mPosition.get(cam);
		Span windowSpan = mSpan.get(cam);

		//target
		Body targetBody = mPhysicalBody.get(target).body;
		Vector2 targetPos = targetBody.getPosition();
		Dimensions targetDim = mDimensions.get(target);
		LookDirection lookDirection = mLookDirection.get(target);

		//camera
		OrthographicCamera camera = mCameraRef.get(cam).camera;
		HorizontalOffset horOff = mHorizontalOffset.get(cam);
		VerticalOffset verOff = mVerticalOffset.get(cam);

		float playerRightSide = targetPos.x + targetDim.width/2;
		float windowRightSide = windowPos.x + windowSpan.right;
		if (playerRightSide > windowRightSide) {
			horOff.value = camera.viewportWidth/12;
			windowPos.x += playerRightSide - windowRightSide;
		}

		float playerLeftSide = targetPos.x - targetDim.width/2;
		float windowLeftSide = windowPos.x - windowSpan.left;
		if (playerLeftSide < windowLeftSide) {
			horOff.value = -camera.viewportWidth/12;
			windowPos.x += playerLeftSide - windowLeftSide;
		}

		float playerUpSide = targetPos.y + targetDim.height/2;
		float windowUpSide = windowPos.y + windowSpan.up;
		if (playerUpSide > windowUpSide) {
			float dy = playerUpSide - windowUpSide;
			windowPos.y += dy;
		}

		float playerDownSide = targetPos.y - targetDim.height/2;
		float windowDownSide = windowPos.y - windowSpan.down;
		if (playerDownSide < windowDownSide) {
			float dy = playerDownSide - windowDownSide;
			windowPos.y += dy;
		}

		verOff.value = 0;
		if(lookDirection.direction == Direction.UP) {
			verOff.value = camera.viewportHeight / 6;
		} else if(lookDirection.direction == Direction.DOWN) {
			verOff.value = -camera.viewportHeight / 6;
		}
	}
}
