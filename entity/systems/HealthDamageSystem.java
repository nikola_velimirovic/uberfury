package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.Health;
import com.velidev.uberfury.entity.components.simple.Damage;

/**
 * Created by Luka on 8/5/2014.
 */

@Wire
public class HealthDamageSystem extends EntityProcessingSystem {
	private ComponentMapper<Damage> damageMapper;
	private ComponentMapper<Health> healthMapper;

	public HealthDamageSystem() {
		super(Aspect.getAspectForAll(Health.class, Damage.class));
	}

	@Override
	public void process(Entity e) {
		Damage damage = damageMapper.get(e);
		Health health = healthMapper.get(e);

		health.value -= damage.value;

		System.out.println(e);
		System.out.println(health.value);

		e.edit().remove(Damage.class);
	}
}
