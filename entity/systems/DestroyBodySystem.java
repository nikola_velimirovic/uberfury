package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.flag.DestroyBody;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class DestroyBodySystem extends EntityProcessingSystem {
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	public DestroyBodySystem() {
		super(Aspect.getAspectForAll(DestroyBody.class, PhysicalBody.class));
	}

	@Override
	protected void process(Entity e) {
		Uberfury.b2World.destroyBody(mPhysicalBody.get(e).body);
		e.edit().remove(DestroyBody.class);
		e.deleteFromWorld();
	}
}
