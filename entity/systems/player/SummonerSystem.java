package com.velidev.uberfury.entity.systems.player;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.entity.components.complex.FixtureDefinition;
import com.velidev.uberfury.entity.components.complex.Summon;
import com.velidev.uberfury.entity.components.complex.audio.AudioListener;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.game.Uberfury;

public class SummonerSystem extends EntityProcessingSystem {
	private ComponentMapper<Summon> mSummon;

    public SummonerSystem() {
        super(Aspect.getAspectForAll(Summon.class));
    }

    @Override
    public void process(Entity entity) {
        Summon summon = entity.getComponent(Summon.class);
        World b2World = Uberfury.b2World;

        Body body = b2World.createBody(summon.bodyDef);
        for(FixtureDefinition fd : summon.fixtureDefinitions) {
            Fixture f = body.createFixture(fd.fixtureDef);
            f.setUserData(fd.fixtureUserData);
        }
        body.setUserData(summon.bodyUserData);

        PhysicalBody pb = entity.edit().create(PhysicalBody.class);
        pb.body = body;

        if(entity.getId() == Uberfury.pleya.getId()) {
            AudioSource audioSource = new AudioSource();

            SoundComponent soundComponent = new SoundComponent();
            soundComponent.position = Uberfury.pleya.getComponent(PhysicalBody.class).body.getPosition();
            soundComponent.sound = Uberfury.assets.manager.get("audio/barreta.mp3", Sound.class);
            soundComponent.radius = Uberfury.worldWidth;
            soundComponent.tag = AudioTag.SHOOT;
            soundComponent.minVolume = 0.01f;
            soundComponent.maxVolume = 0.4f;

            audioSource.soundComponents.add(soundComponent);

            soundComponent = new SoundComponent();
            soundComponent.position = Uberfury.pleya.getComponent(PhysicalBody.class).body.getPosition();
            soundComponent.sound = Uberfury.assets.manager.get("audio/footstep_concrete.wav", Sound.class);
            soundComponent.radius = Uberfury.worldWidth * 2;
            soundComponent.tag = AudioTag.FOOTSTEP;
            soundComponent.minVolume = 0.3f;
            soundComponent.maxVolume = 1f;

            audioSource.soundComponents.add(soundComponent);

            soundComponent = new SoundComponent();
            soundComponent.position = Uberfury.pleya.getComponent(PhysicalBody.class).body.getPosition();
            soundComponent.sound = Uberfury.assets.manager.get("audio/jump.mp3", Sound.class);
            soundComponent.radius = Uberfury.worldWidth * 2;
            soundComponent.tag = AudioTag.JUMP;
            soundComponent.minVolume = 0.3f;
            soundComponent.maxVolume = 1f;

            audioSource.soundComponents.add(soundComponent);

            soundComponent = new SoundComponent();
            soundComponent.position = Uberfury.pleya.getComponent(PhysicalBody.class).body.getPosition();
            soundComponent.sound = Uberfury.assets.manager.get("audio/slash_empty.mp3", Sound.class);
            soundComponent.radius = Uberfury.worldWidth * 2;
            soundComponent.tag = AudioTag.SLASH;
            soundComponent.minVolume = 0.3f;
            soundComponent.maxVolume = 1f;

            audioSource.soundComponents.add(soundComponent);

            Uberfury.pleya.edit().add(audioSource);
        } else {
            Uberfury.audioListener = new AudioListener();
            Uberfury.audioListener.position = entity.getComponent(PhysicalBody.class).body.getPosition();
            entity.edit().add(Uberfury.audioListener);
        }
        entity.edit().remove(Summon.class);
    }
}
