package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.entity.components.simple.MoveConditions;
import com.velidev.uberfury.entity.components.simple.MoveDirection;
import com.velidev.uberfury.entity.components.simple.MoveSpeed;
import com.velidev.uberfury.physics.MovementHandler;

@Wire
public class MovementSystem extends EntityProcessingSystem {
	private ComponentMapper<MoveConditions> mMoveConditions;
	private ComponentMapper<JumpConditions> mJumpConditions;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<MoveSpeed> mMoveSpeed;
	private ComponentMapper<MoveDirection> mMoveDirection;

	private ComponentMapper<AudioSource> mAudioSource;

	int timer = 0;

    public MovementSystem() {
        super(Aspect.getAspectForAll(MoveConditions.class, JumpConditions.class, PhysicalBody.class, MoveSpeed.class, MoveDirection.class, AudioSource.class));
    }

    @Override
    public void process(Entity e) {
	    MoveConditions mc = mMoveConditions.get(e);
	    JumpConditions jc = mJumpConditions.get(e);
	    PhysicalBody pb = mPhysicalBody.get(e);
	    MoveSpeed ms = mMoveSpeed.get(e);

	    timer++;

	    SoundComponent footstep = null;

	    AudioSource audioSource = mAudioSource.get(e);
	    for(SoundComponent soundComponent : audioSource.soundComponents) {
		    if(soundComponent.tag == AudioTag.FOOTSTEP) {
			    footstep = soundComponent;
			    break;
		    }
	    }

	    if (mc.movingLeft) {
			MovementHandler.moveLeft(pb.body, ms.value);
		    checkFriction(jc, pb);
		    if(jc.downPlatforms > 0 && timer > 10) {
			    footstep.soundId = footstep.sound.play(0);
			    timer = 0;
		    }
		    return;
	    }

	    if (mc.movingRight) {
			MovementHandler.moveRight(pb.body, ms.value);
		    checkFriction(jc, pb);
		    if(jc.downPlatforms > 0 && timer > 10) {
			    footstep.soundId = footstep.sound.play(0);
			    timer = 0;
		    }
		    return;
	    }

	    if (jc.downPlatforms > 0 && !jc.inAir) {
		    pb.body.setAwake(false);
	    }
    }

    private void checkFriction(JumpConditions jc, PhysicalBody pb) {
        if((jc.leftPlatforms > 0 || jc.rightPlatforms > 0) && 0 == jc.downPlatforms) {
            if(pb.body.getLinearVelocity().y <= 0) {
                MovementHandler.moveDown(pb.body, 1.5f);//napravi konstantu ovde
            }
        }
    }

}
