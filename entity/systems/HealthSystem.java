package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.Health;
import com.velidev.uberfury.entity.components.simple.flag.Dead;

/**
 * Created by Luka on 8/6/2014.
 */

@Wire
public class HealthSystem extends EntityProcessingSystem {
	private ComponentMapper<Health> healthMapper;
	public HealthSystem() {
		super(Aspect.getAspectForAll(Health.class).exclude(Dead.class));
	}

	@Override
	public void process(Entity e) {
		Health health = healthMapper.get(e);

		if(health.value <= 0) {
			e.edit().create(Dead.class);
		}
	}
}
