package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.simple.LookConditions;
import com.velidev.uberfury.entity.components.simple.LookDirection;
import com.velidev.uberfury.entity.components.simple.MoveDirection;
import com.velidev.uberfury.gameobjects.Direction;

@Wire
public class LookSystem extends EntityProcessingSystem {
	private ComponentMapper<LookConditions> lookConditionsMapper;
	private ComponentMapper<LookDirection> lookDirectionMapper;
	private ComponentMapper<MoveDirection> moveDirectionMapper;

	public LookSystem() {
		super(Aspect.getAspectForAll(LookConditions.class, LookDirection.class, MoveDirection.class));
	}

	@Override
	public void process(Entity e) {
		LookConditions lc = lookConditionsMapper.get(e);
		LookDirection ld = lookDirectionMapper.get(e);
		MoveDirection md = moveDirectionMapper.get(e);

		if(lc.startLookingUp && !lc.lookingDown) {
			ld.direction = Direction.UP;
			lc.lookingUp = true;
			lc.startLookingUp = false;
		} else if(lc.startLookingDown && !lc.lookingUp) {
			ld.direction = Direction.DOWN;
			lc.lookingDown = true;
			lc.startLookingDown = false;
		}

		if(!lc.lookingUp && !lc.lookingDown) {
			ld.direction = md.moveDirection;
		}
	}
}
