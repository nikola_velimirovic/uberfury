package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.entity.components.complex.audio.AudioListener;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class AudioSourceCatcherSystem extends EntityProcessingSystem {
	private ComponentMapper<AudioSource> mAudioSource;

	public AudioSourceCatcherSystem() {
		super(Aspect.getAspectForAll(AudioSource.class));
	}

	@Override
	protected void process(Entity e) {
		AudioSource audioSource = mAudioSource.get(e);
		AudioListener audioListener = Uberfury.audioListener;

		for(SoundComponent soundComponent : audioSource.soundComponents) {
			float volume = calculateVolume(audioListener, soundComponent);
			if(volume > soundComponent.minVolume) {
				if(!audioListener.caughtSounds.contains(soundComponent, true)) {
					audioListener.caughtSounds.add(soundComponent);
				}
				soundComponent.volume = volume;
			} else {
				audioListener.caughtSounds.removeValue(soundComponent, true);
				soundComponent.volume = soundComponent.minVolume;
			}
			if(soundComponent.tag == AudioTag.FOOTSTEP)
				System.out.println("volume: " + soundComponent.volume);
		}
	}

	private float calculateVolume(AudioListener audioListener, SoundComponent soundComponent) {
		Vector2 subVector = audioListener.position.sub(soundComponent.position);
		float x = subVector.x / soundComponent.radius;

		soundComponent.pan = -x;
		if(soundComponent.pan > 1) soundComponent.pan = 1;
		else if(soundComponent.pan < -1) soundComponent.pan = -1;

		float y = subVector.y / soundComponent.radius;
		float distance = (float)Math.sqrt((double)(x*x + y*y));
		float volume = (1 - distance) * soundComponent.maxVolume;
		return volume;
	}
}
