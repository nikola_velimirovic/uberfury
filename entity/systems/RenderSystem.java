package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.complex.Render;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.Transform;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class RenderSystem extends EntityProcessingSystem {
	private ComponentMapper<Transform> mTransform;
    private ComponentMapper<Render> mRender;
    private ComponentMapper<Dimensions> mDimensions;

    public RenderSystem() {
        super(Aspect.getAspectForAll(Transform.class, Render.class, Dimensions.class));
    }

    @Override
    public void process(Entity e) {
        Transform transform = mTransform.get(e);
        Render render = mRender.get(e);
        Dimensions dimensions = mDimensions.get(e);

        Uberfury.batch.draw(render.region, transform.x, transform.y, dimensions.width/2,
                            dimensions.height/2, dimensions.width, dimensions.height,
                            transform.scaleX, transform.scaleY, transform.rotation);
    }
}
