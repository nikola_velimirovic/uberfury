package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.velidev.uberfury.entity.components.AnimationComponent;
import com.velidev.uberfury.entity.components.complex.Render;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class AnimationSystem extends EntityProcessingSystem {
	private ComponentMapper<AnimationComponent> mAnimation;
	private ComponentMapper<Render> mRender;

	public AnimationSystem() {
		super(Aspect.getAspectForAll(AnimationComponent.class));
	}

	@Override
	protected void process(Entity e) {
		AnimationComponent animationComponent = mAnimation.get(e);

		animationComponent.stateTime += Uberfury.world.delta;
		TextureRegion currentFrame = animationComponent.animation.getKeyFrame(animationComponent.stateTime, true);
		if(animationComponent.isAtomic && animationComponent.animation.isAnimationFinished(animationComponent.stateTime)) {
			animationComponent.currentState.setCanBeChanged(true);
		}

		if(mRender.get(e) == null) {
			Render render = new Render();
			render.region = currentFrame;
			e.edit().add(render);
		} else {
			mRender.get(e).region = currentFrame;
		}
	}
}
