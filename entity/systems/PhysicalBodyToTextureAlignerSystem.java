package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.Transform;
import com.velidev.uberfury.entity.components.simple.Bullet;
import com.velidev.uberfury.entity.components.simple.flag.entities.Player;

@Wire
public class PhysicalBodyToTextureAlignerSystem extends EntityProcessingSystem {
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Transform> mPosition;
	private ComponentMapper<Dimensions> mDimensions;

	public PhysicalBodyToTextureAlignerSystem() {
		super(Aspect.getAspectForAll(PhysicalBody.class, Transform.class, Dimensions.class).one(Player.class, Bullet.class));
	}

	@Override
	protected void process(Entity e) {
		Dimensions dimensions = mDimensions.get(e);
		Vector2 bodyPosition = mPhysicalBody.get(e).body.getPosition();
		Transform textureTransform = mPosition.get(e);

		textureTransform.x = bodyPosition.x - dimensions.width / 2;
		textureTransform.y = bodyPosition.y - dimensions.height / 2;
	}
}
