package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.MovingPlatform;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.flag.entities.Player;
import com.velidev.uberfury.physics.MovementHandler;

@Wire
public class OnPlatformSystem extends EntityProcessingSystem {
	private ComponentMapper<MovingPlatform> mOnPlatform;
	private ComponentMapper<PhysicalBody> mPhysicalBody;

    public OnPlatformSystem() {
        super(Aspect.getAspectForAll(MovingPlatform.class, PhysicalBody.class, Player.class));
    }

    @Override
    public void process(Entity e) {
		float onVelocity = mOnPlatform.get(e).velocity;
	    Body body = mPhysicalBody.get(e).body;

	    float velX = body.getLinearVelocity().x;
	    float velY = body.getLinearVelocity().y;
	    if(((velX < 0.1f && velX > -0.1f) && velY - 0.2f < 0)) {
		    if(onVelocity > 0) {
			    MovementHandler.moveRight(body, Math.abs(onVelocity));
		    } else {
			    MovementHandler.moveLeft(body, Math.abs(onVelocity));
		    }
	    }
    }

}
