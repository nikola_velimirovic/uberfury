package com.velidev.uberfury.entity.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.physics.box2d.Body;
import com.velidev.uberfury.entity.components.MovingPlatform;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.flag.entities.Player;

@Wire
public class MovingPlatformSystem extends EntityProcessingSystem {
	private ComponentMapper<MovingPlatform> mMovingPlatform;
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	public MovingPlatformSystem() {
		super(Aspect.getAspectForAll(MovingPlatform.class, PhysicalBody.class).exclude(Player.class));
	}

	@Override
	protected void process(Entity e) {
		Body body = mPhysicalBody.get(e).body;
		MovingPlatform mp = mMovingPlatform.get(e);

		if(Math.abs(mp.oldPosition - body.getPosition().x) >= mp.range) {
			mp.velocity *= -1;
			body.setLinearVelocity(mp.velocity, 0);
			mp.oldPosition = body.getPosition().x;
		}

	}
}
