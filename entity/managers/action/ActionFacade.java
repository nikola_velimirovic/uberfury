package com.velidev.uberfury.entity.managers.action;

import com.artemis.Entity;
import com.velidev.uberfury.entity.managers.action.managers.*;

public class ActionFacade {
	private Entity player;

	private MovementManager movementManager;
	private MeleeManager meleeManager;
	private RangedManager rangedManager;
	private JumpManager jumpManager;
	private LookManager lookManager;

	public ActionFacade(Entity player, MovementManager movementManager, MeleeManager meleeManager, RangedManager rangedManager, JumpManager jumpManager, LookManager lookManager) {
		this.player = player;
		this.movementManager = movementManager;
		this.meleeManager = meleeManager;
		this.rangedManager = rangedManager;
		this.jumpManager = jumpManager;
		this.lookManager = lookManager;
	}

	public void setPlayer(Entity player) {
		this.player = player;
	}

	public void moveLeft() {
		movementManager.moveLeft(player);
	}

	public void stopMovingLeft() {
		movementManager.stopMovingLeft(player);
	}

	public void moveRight() {
		movementManager.moveRight(player);
	}

	public void stopMovingRight() {
		movementManager.stopMovingRight(player);
	}

	public void lookUp() {
		lookManager.lookUp(player);
	}

	public void stopLookingUp() {
		lookManager.stopLookingUp(player);
	}

	public void lookDown() {
		lookManager.lookDown(player);
	}

	public void stopLookingDown() {
		lookManager.stopLookingDown(player);
	}

	public void jump() {
		jumpManager.jump(player);
	}

	public void startMelee() {
		meleeManager.startMelee(player);
	}

	public void stopMelee() {
		meleeManager.stopMelee(player);
	}

	public void startRanged() {
		rangedManager.startRanged(player);
	}

	public void stopRanged() {
		rangedManager.stopRanged(player);
	}
}
