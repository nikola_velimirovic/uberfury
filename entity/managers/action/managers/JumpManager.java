package com.velidev.uberfury.entity.managers.action.managers;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.managers.action.ActionManager;
import com.velidev.uberfury.entity.components.complex.audio.AudioSource;
import com.velidev.uberfury.entity.components.complex.audio.AudioTag;
import com.velidev.uberfury.entity.components.complex.audio.SoundComponent;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.entity.components.simple.JumpSpeed;
import com.velidev.uberfury.entity.components.simple.MoveDirection;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.physics.MovementHandler;

@Wire
public class JumpManager extends ActionManager {
	private ComponentMapper<JumpConditions> mJumpConditions;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<JumpSpeed> mJumpSpeed;
	private ComponentMapper<MoveDirection> mMoveDirection;

	private ComponentMapper<AudioSource> mAudioSource;

	public void jump(Entity player) {
		SoundComponent jumpSound = null;
		AudioSource audioSource = mAudioSource.get(player);
		for(SoundComponent soundComponent : audioSource.soundComponents) {
			if(soundComponent.tag == AudioTag.JUMP) {
				jumpSound = soundComponent;
				break;
			}
		}
		jump(mJumpConditions.get(player), mPhysicalBody.get(player), mJumpSpeed.get(player), mMoveDirection.get(player), jumpSound);
	}

	private void jump(JumpConditions jc, PhysicalBody pb, JumpSpeed js, MoveDirection md, SoundComponent jumpSound) {

		if(jc.downPlatforms > 0) {
			MovementHandler.jumpUp(pb.body, js.value);
			jc.inAir = true;
			jumpSound.soundId = jumpSound.sound.play(0);
			return;
		}

		if(jc.leftPlatforms > 0) {
			MovementHandler.jumpRight(pb.body, js.value);
			jc.inAir = true;
			jumpSound.soundId = jumpSound.sound.play(0);
			md.moveDirection = Direction.RIGHT;
			return;
		}

		if(jc.rightPlatforms > 0) {
			MovementHandler.jumpLeft(pb.body, js.value);
			jc.inAir = true;
			jumpSound.soundId = jumpSound.sound.play(0);
			md.moveDirection = Direction.LEFT;
			return;
		}
	}
}
