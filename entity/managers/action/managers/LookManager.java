package com.velidev.uberfury.entity.managers.action.managers;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.managers.action.ActionManager;
import com.velidev.uberfury.entity.components.simple.LookConditions;

@Wire
public class LookManager extends ActionManager {
	private ComponentMapper<LookConditions> mLookConditions;

	public void lookUp(Entity player) {
		mLookConditions.get(player).startLookingUp = true;
	}

	public void stopLookingUp(Entity player) {
		mLookConditions.get(player).lookingUp = false;
	}

	public void lookDown(Entity player) {
		mLookConditions.get(player).startLookingDown = true;
	}

	public void stopLookingDown(Entity player) {
		mLookConditions.get(player).lookingDown = false;
	}
}
