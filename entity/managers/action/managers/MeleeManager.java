package com.velidev.uberfury.entity.managers.action.managers;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.managers.action.ActionManager;
import com.velidev.uberfury.entity.components.complex.Slash;

@Wire
public class MeleeManager extends ActionManager {
	private ComponentMapper<Slash> mSlash;

	public void startMelee(Entity player) {
		mSlash.get(player).isSlashing = true;
	}

	public void stopMelee(Entity player) {
		mSlash.get(player).isSlashing = false;
	}
}
