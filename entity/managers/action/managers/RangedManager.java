package com.velidev.uberfury.entity.managers.action.managers;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.managers.action.ActionManager;
import com.velidev.uberfury.entity.components.complex.Shoot;

@Wire
public class RangedManager extends ActionManager {
	private ComponentMapper<Shoot> mShoot;

	public void startRanged(Entity player) {
		mShoot.get(player).isShooting = true;
	}

	public void stopRanged(Entity player) {
		mShoot.get(player).isShooting = false;
	}
}
