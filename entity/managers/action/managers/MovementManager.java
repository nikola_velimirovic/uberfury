package com.velidev.uberfury.entity.managers.action.managers;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.managers.action.ActionManager;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.MoveConditions;
import com.velidev.uberfury.entity.components.simple.MoveDirection;
import com.velidev.uberfury.entity.components.simple.MoveSpeed;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.physics.MovementHandler;

@Wire
public class MovementManager extends ActionManager {
	private ComponentMapper<MoveConditions> mMoveConditions;
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<MoveSpeed> mMoveSpeed;
	private ComponentMapper<MoveDirection> mMoveDirection;

    /*Kretanje*/
    public void moveLeft(Entity player) {
        moveLeft(mMoveConditions.get(player), mMoveDirection.get(player));
    }

    public void moveRight(Entity player) {
	    moveRight(mMoveConditions.get(player), mMoveDirection.get(player));
    }

    public void stopMovingLeft(Entity player) {
        stopMovingLeft(mMoveConditions.get(player), mMoveDirection.get(player), mPhysicalBody.get(player));
    }

    public void stopMovingRight(Entity player) {
        stopMovingRight(mMoveConditions.get(player), mMoveDirection.get(player), mPhysicalBody.get(player));
    }

    private void moveLeft(MoveConditions mc, MoveDirection md) {
        if(!mc.movingRight) {
            mc.movingLeft = true;
            md.moveDirection = Direction.LEFT;
            mc.requestedToMoveLeft = false;
        } else {
            mc.requestedToMoveLeft = true;
        }
    }

    private void moveRight(MoveConditions mc, MoveDirection md) {
        if(!mc.movingLeft) {
            mc.movingRight = true;
            md.moveDirection = Direction.RIGHT;
            mc.requestedToMoveRight = false;
        } else {
            mc.requestedToMoveRight = true;
        }
    }

    private void stopMovingLeft(MoveConditions mc, MoveDirection md, PhysicalBody pb) {
        mc.movingLeft = false;
        mc.requestedToMoveLeft = false;
        MovementHandler.stopXMovement(pb.body);
        if(mc.requestedToMoveRight) {
            moveRight(mc, md);
        }
    }

    private void stopMovingRight(MoveConditions mc, MoveDirection md, PhysicalBody pb) {
        mc.movingRight = false;
        mc.requestedToMoveRight = false;
        MovementHandler.stopXMovement(pb.body);
        if(mc.requestedToMoveLeft) {
            moveLeft(mc, md);
        }
    }
}
