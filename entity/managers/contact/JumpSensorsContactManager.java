package com.velidev.uberfury.entity.managers.contact;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.FixtureUserData;

@Wire
public class JumpSensorsContactManager extends ContactManager {
	private ComponentMapper<JumpConditions> mJumpConditions;

    @Override
    public void beginContact(Fixture fa, Fixture fb, Contact contact) {
        Entity player = ((BodyUserData) fa.getBody().getUserData()).entity;

        JumpConditions jc = mJumpConditions.get(player);

        FixtureType ftype = ((FixtureUserData) fa.getUserData()).getType();
        switch (ftype) {
            case DOWN_JUMP_SENSOR:
                jc.downPlatforms++;
                break;
            case LEFT_JUMP_SENSOR:
                jc.leftPlatforms++;
                break;
            case RIGHT_JUMP_SENSOR:
                jc.rightPlatforms++;
                break;
        }
    }

    @Override
    public void endContact(Fixture fa, Fixture fb, Contact contact) {
        Entity player = ((BodyUserData) fa.getBody().getUserData()).entity;

	    JumpConditions jc = mJumpConditions.get(player);

        FixtureType ftype = ((FixtureUserData) fa.getUserData()).getType();
        switch (ftype) {
            case DOWN_JUMP_SENSOR:
                jc.downPlatforms--;
                break;
            case LEFT_JUMP_SENSOR:
                jc.leftPlatforms--;
                break;
            case RIGHT_JUMP_SENSOR:
                jc.rightPlatforms--;
                break;
        }
    }

}
