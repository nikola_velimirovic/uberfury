package com.velidev.uberfury.entity.managers.contact;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.entity.components.simple.flag.entities.Player;
import com.velidev.uberfury.entity.systems.camera.WindowSystem;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.FixtureUserData;

@Wire
public class OneWayPlatformContactManager extends ContactManager {
    public static Array<Contact> contacts = new Array<Contact>();

	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<Dimensions> mDimensions;
	private ComponentMapper<JumpConditions> mJumpConditions;

    @Override
    public void endContact(Fixture fa, Fixture fb, Contact contact) {
        contacts.removeValue(contact, false);
    }

    @Override
    public void beginContact(Fixture fa, Fixture fb, Contact contact) {
        Entity platform = ((BodyUserData) fa.getBody().getUserData()).entity;
        Entity player = ((BodyUserData) fb.getBody().getUserData()).entity;

	    if(player.getComponent(Player.class) == null) {
		    return;
	    }

        FixtureUserData fixtureUserData = (FixtureUserData) fb.getUserData();
        if(fixtureUserData.getType() != FixtureType.PLAYER_BODY) {
            return;
        }

        Body platformBody = mPhysicalBody.get(platform).body;
        Dimensions platformDim = mDimensions.get(platform);

        Body playerBody = mPhysicalBody.get(player).body;
        JumpConditions jc = mJumpConditions.get(player);

        for(Vector2 point : contact.getWorldManifold().getPoints()) {
            Vector2 platformVel = platformBody.getLinearVelocityFromWorldPoint(point);
            Vector2 playerVel = playerBody.getLinearVelocityFromWorldPoint(point);

            Vector2 relative = platformBody.getLocalVector(playerVel.sub(platformVel));
            if(relative.y < 0.1f) {
                WindowSystem wh = Uberfury.world.getSystem(WindowSystem.class);
	            float centerHeight = platformBody.getPosition().y + platformDim.height / 2;
                wh.centerOn(player, platformBody.getPosition().y + platformDim.height/2);

                jc.inAir = false;
                if(fb.getBody().getLinearVelocity().y < 0.1f && fb.getBody().getLinearVelocity().y > -0.1f) {
                    fb.getBody().setAwake(false);
                }
                return;
            }
        }

        contacts.add(contact);
    }

    @Override
    public void preSolve(Fixture fa, Fixture fb, Contact contact, Manifold oldManifold) {
        for(Contact c : OneWayPlatformContactManager.contacts) {
            c.setEnabled(false);
        }
    }
}
