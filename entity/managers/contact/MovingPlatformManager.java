package com.velidev.uberfury.entity.managers.contact;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.MovingPlatform;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;

@Wire(injectInherited = true)
public class MovingPlatformManager extends OneWayPlatformContactManager {
	private ComponentMapper<PhysicalBody> mPhysicalBody;
	private ComponentMapper<MovingPlatform> mMovingPlatform;
	@Override
	public void beginContact(Fixture fa, Fixture fb, Contact contact) {
		super.beginContact(fa, fb, contact);
		Entity platform = ((BodyUserData) fa.getBody().getUserData()).entity;
		Entity player = ((BodyUserData) fb.getBody().getUserData()).entity;

		MovingPlatform mp = mMovingPlatform.get(platform);

		player.edit().add(mp);
	}

	@Override
	public void endContact(Fixture fa, Fixture fb, Contact contact) {
		super.endContact(fa, fb, contact);
		Entity player = ((BodyUserData) fb.getBody().getUserData()).entity;
		player.edit().remove(MovingPlatform.class);
	}
}
