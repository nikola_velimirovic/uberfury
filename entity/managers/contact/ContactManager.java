package com.velidev.uberfury.entity.managers.contact;

import com.artemis.Manager;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ContactManager extends Manager {
    public void beginContact(Fixture fa, Fixture fb, Contact contact) {}

    public void endContact(Fixture fa, Fixture fb, Contact contact) {}

    public void preSolve(Fixture fa, Fixture fb, Contact contact, Manifold oldManifold) {}

    public void postSolve(Fixture fa, Fixture fb, Contact contact, ContactImpulse impulse) {}

}
