package com.velidev.uberfury.entity.managers.contact;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Damage;
import com.velidev.uberfury.entity.components.simple.flag.DestroyBody;

@Wire
public class BulletContactManager extends ContactManager {
	private ComponentMapper<PhysicalBody> mPhysicalBody;

	Damage damage = new Damage();
	{
		damage.value = 40;
	}
	//Sound hurtSound = Uberfury.assets.manager.get("audio/fall.mp3", Sound.class);

	@Override
	public void beginContact(Fixture fa, Fixture fb, Contact contact) {
		Entity bullet = ((BodyUserData) fa.getBody().getUserData()).entity;
		Entity player = ((BodyUserData) fb.getBody().getUserData()).entity;

		player.edit().add(damage);

	//	hurtSound.play();
		bullet.edit().create(DestroyBody.class);
	}
}
