package com.velidev.uberfury.entity.managers.contact;

import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.velidev.uberfury.entity.BodyUserData;
import com.velidev.uberfury.entity.components.complex.references.box2d.PhysicalBody;
import com.velidev.uberfury.entity.components.simple.Dimensions;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.entity.components.simple.flag.entities.Player;
import com.velidev.uberfury.entity.systems.camera.WindowSystem;
import com.velidev.uberfury.game.Uberfury;

@Wire
public class FloorContactManager extends ContactManager {

	@Override
    public void beginContact(Fixture fa, Fixture fb, Contact contact) {
        Entity platform = ((BodyUserData) fa.getBody().getUserData()).entity;
        Entity player = ((BodyUserData) fb.getBody().getUserData()).entity;

		if(player.getComponent(Player.class) == null) {
			return;
		}

        Body platformBody = platform.getComponent(PhysicalBody.class).body;
        Dimensions platformDim = platform.getComponent(Dimensions.class);

        float centerHeight = platformBody.getPosition().y + platformDim.height / 2;
		Uberfury.world.getSystem(WindowSystem.class).centerOn(player, centerHeight);

        JumpConditions jc = player.getComponent(JumpConditions.class);
        jc.inAir = false;
        if(fb.getBody().getLinearVelocity().y < 0.1f && fb.getBody().getLinearVelocity().y > -0.1f) {
            fb.getBody().setAwake(false);
        }
    }
}
