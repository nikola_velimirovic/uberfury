package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class Span extends Component {
    public float left, right, up, down;

    public Span(float left, float right, float up, float down) {
        this.left = left; this.right = right; this.up = up; this.down = down;
    }

    public Span() {
    }
}
