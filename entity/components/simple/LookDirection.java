package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;
import com.velidev.uberfury.gameobjects.Direction;

public class LookDirection extends Component {
	public Direction direction;
}
