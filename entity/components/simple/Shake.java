package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class Shake extends Component {
	public float radius, maxRadius;
	public float angle;
	public float dx, dy;
}
