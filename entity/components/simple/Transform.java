package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class Transform extends Component {
    public float x, y;
    public float rotation;
    public float scaleX = 1;
    public float scaleY = 1;
}
