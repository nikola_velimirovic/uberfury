package com.velidev.uberfury.entity.components.simple;


import com.artemis.Component;

/**
 * Created by Luka on 7/24/2014.
 */
public class Boundaries extends Component {
    public float left, right, up, down;

    public Boundaries(float left, float right, float up, float down) {
        this.left = left; this.right = right; this.up = up; this.down = down;
    }

    public Boundaries() {}
}
