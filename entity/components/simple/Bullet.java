package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class Bullet extends Component {
	public float range;
	public float xStart;
	public float yStart;
}
