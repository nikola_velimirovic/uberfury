package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class JumpConditions extends Component {
    public short downPlatforms, leftPlatforms, rightPlatforms;
    public boolean inAir;
}
