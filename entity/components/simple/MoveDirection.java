package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;
import com.velidev.uberfury.gameobjects.Direction;

public class MoveDirection extends Component {
    public Direction moveDirection;

    public MoveDirection(Direction moveDirection) {
        this.moveDirection = moveDirection;
    }

    public MoveDirection() {
        moveDirection = Direction.RIGHT;
    }
}
