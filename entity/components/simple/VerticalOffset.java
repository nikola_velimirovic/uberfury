package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class VerticalOffset extends Component {
    public float value;
}
