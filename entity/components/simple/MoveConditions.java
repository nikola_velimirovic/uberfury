package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class MoveConditions extends Component {
    public boolean movingLeft, movingRight, requestedToMoveLeft, requestedToMoveRight;
}
