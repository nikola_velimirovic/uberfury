package com.velidev.uberfury.entity.components.simple;


import com.artemis.Component;

/**
 * Created by Luka on 7/24/2014.
 */
public class Dimensions extends Component {
    public float width, height;

    public Dimensions(float width, float height) {
        this.width = width;
        this.height = height;
    }

    public Dimensions() {
    }
}
