package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class MoveSpeed extends Component {
    public float value;

    public MoveSpeed(float value) {
        this.value = value;
    }

    public MoveSpeed() {
    }
}
