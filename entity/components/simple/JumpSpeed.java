package com.velidev.uberfury.entity.components.simple;

import com.artemis.Component;

public class JumpSpeed extends Component {
    public float value;

    public JumpSpeed(float value) {
        this.value = value;
    }

    public JumpSpeed() {
    }
}
