package com.velidev.uberfury.entity.components.simple;


import com.artemis.Component;

/**
 * Created by Luka on 7/24/2014.
 */
public class HorizontalOffset extends Component {
    public float value;

    public HorizontalOffset(float value) {
        this.value = value;
    }

    public HorizontalOffset() {
    }
}
