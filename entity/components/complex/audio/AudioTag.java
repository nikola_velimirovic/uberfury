package com.velidev.uberfury.entity.components.complex.audio;

public enum AudioTag {
	SHOOT, FOOTSTEP, JUMP, SLASH
}
