package com.velidev.uberfury.entity.components.complex.audio;

import com.artemis.Component;
import com.badlogic.gdx.utils.Array;

public class AudioSource extends Component {
	public Array<SoundComponent> soundComponents = new Array<SoundComponent>();
}
