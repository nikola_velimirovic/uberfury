package com.velidev.uberfury.entity.components.complex.audio;

import com.artemis.Component;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;

public class SoundComponent extends Component {
	public AudioTag tag;
	public Sound sound;
	public Vector2 position;
	public long soundId;
	public float volume;
	public float pan;
	public float radius;
	public float minVolume;
	public float maxVolume;
}
