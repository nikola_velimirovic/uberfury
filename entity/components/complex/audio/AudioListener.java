package com.velidev.uberfury.entity.components.complex.audio;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class AudioListener extends Component {
	public Vector2 position = new Vector2();
	public Array<SoundComponent> caughtSounds = new Array<SoundComponent>();

}
