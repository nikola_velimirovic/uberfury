package com.velidev.uberfury.entity.components.complex;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Texture;

public class Display extends Component {
    public Texture texture;

    public Display(String name) {
        texture = new Texture(name);
    }
}

