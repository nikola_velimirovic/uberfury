package com.velidev.uberfury.entity.components.complex;

import com.artemis.Component;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.velidev.uberfury.gameobjects.FixtureUserData;

public class FixtureDefinition extends Component {
    public FixtureDef fixtureDef;
    public FixtureUserData fixtureUserData;
}
