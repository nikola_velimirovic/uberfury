package com.velidev.uberfury.entity.components.complex.references.box2d;

import com.artemis.Component;
import com.badlogic.gdx.physics.box2d.World;

public class PhysicalWorld extends Component {
    public World b2World;
}
