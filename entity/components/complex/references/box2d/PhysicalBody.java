package com.velidev.uberfury.entity.components.complex.references.box2d;

import com.artemis.Component;
import com.badlogic.gdx.physics.box2d.Body;

public class PhysicalBody extends Component {
    public Body body;

    public PhysicalBody(Body body) {
        this.body = body;
    }

    public PhysicalBody() {
    }
}
