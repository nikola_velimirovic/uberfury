package com.velidev.uberfury.entity.components.complex.references;

import com.artemis.Component;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class CameraRef extends Component {
    public OrthographicCamera camera;

    public CameraRef(OrthographicCamera camera) {
        this.camera = camera;
    }
}
