package com.velidev.uberfury.entity.components.complex;

import com.artemis.Component;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.entity.BodyUserData;

public class Summon extends Component {
    public BodyUserData bodyUserData;
    public BodyDef bodyDef;
    public Array<FixtureDefinition> fixtureDefinitions = new Array<FixtureDefinition>();
}
