package com.velidev.uberfury.entity.components.complex;

import com.artemis.Component;
import com.badlogic.gdx.utils.Array;

public class Shoot extends Component {
	public long time;
	public long duration;
	public boolean isShooting;
	public Array<Component> effects = new Array<Component>();
}
