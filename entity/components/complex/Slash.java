package com.velidev.uberfury.entity.components.complex;

import com.artemis.Component;
import com.badlogic.gdx.utils.Array;

public class Slash extends Component {
	public long time;
	public long duration;
	public float range;
	public boolean isSlashing;
	public Array<Component> effects = new Array<Component>();
}
