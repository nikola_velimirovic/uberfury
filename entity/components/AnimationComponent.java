package com.velidev.uberfury.entity.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.velidev.uberfury.entity.fsm.EntityState;

public class AnimationComponent extends Component {
	public EntityState currentState;
	public Animation animation;
	public float stateTime;
	public boolean isAtomic;
}
