package com.velidev.uberfury.entity.components;

import com.artemis.Component;

public class MovingPlatform extends Component {
	public float velocity;
	public float oldPosition;
	public float range;
}
