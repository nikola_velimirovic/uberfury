package com.velidev.uberfury.entity;

import com.artemis.Entity;
import com.velidev.uberfury.entity.managers.contact.ContactManager;

public class BodyUserData {
    public Class<? extends ContactManager> contactManagerClass;
    public Entity entity;
}
