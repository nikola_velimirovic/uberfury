package com.velidev.uberfury.entity;

public enum FieldType {
    FULL, DELTA
}
