package com.velidev.uberfury.entity.fsm;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.utils.Bag;
import com.badlogic.gdx.utils.ObjectMap;

public class EntityStateMachine {
	public static Bag<EntityStateMachine> machines = new Bag<EntityStateMachine>();

	private ObjectMap<String, EntityState> states = new ObjectMap<String, EntityState>();
	private Entity e;
	protected EntityState currentState;

	public EntityStateMachine(Entity e) {
		this.e = e;
		machines.add(this);
	}

	public EntityState createState(String key) {
		EntityState state = new EntityState(this);
		states.put(key, state);
		return state;
	}

	public EntityState getCurrentState() {
		return currentState;
	}

	public Entity getEntity() {
		return e;
	}

	public void changeStateTo(String key) {
		EntityState newState = states.get(key);
		changeStateTo(newState);
	}

	public void changeStateTo(EntityState newState) {
		if(null == newState) {
			throw new Error("Ne postoji STATE!");
		}
		if(newState == currentState) {
			return;
		}

		if(currentState != null) {
			for (Component component : currentState.getComponents()) {
				e.edit().remove(component.getClass());
			}
		}

		for(Component component : newState.getComponents()) {
			e.edit().add(component);
		}

		currentState = newState;
	}
}
