package com.velidev.uberfury.entity.fsm;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.velidev.uberfury.entity.components.Animator;
import com.velidev.uberfury.entity.fsm.animation.AnimatorController;

@Wire
public class AnimatorSystem extends EntityProcessingSystem {
	private ComponentMapper<Animator> mAnimator;

	public AnimatorSystem() {
		super(Aspect.getAspectForAll(Animator.class));
	}

	@Override
	protected void process(Entity e) {
		AnimatorController animatorController = mAnimator.get(e).animatorController;
		processAnimatorController(animatorController);
	}

	protected void processAnimatorController(AnimatorController animatorController) {
		EntityState currentState = animatorController.getCurrentState();
		if(currentState.canBeChanged()) {
			processCurrentState(currentState);
		}
	}

	protected void processCurrentState(EntityState currentState) {
		for(Transition transition : currentState.getTransitions()) {
			boolean changedState = processTransition(transition);
			if(changedState) break;
		}
	}

	private boolean processTransition(Transition transition) {
		return transition.evaluate();
	}

}
