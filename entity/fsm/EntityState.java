package com.velidev.uberfury.entity.fsm;

import com.artemis.Component;
import com.artemis.utils.Bag;
import com.velidev.uberfury.entity.fsm.animation.AnimatorController;
import com.velidev.uberfury.entity.fsm.animation.TransitionCondition;

public class EntityState {
	private Bag<Component> components = new Bag<Component>();
	private Bag<Transition> transitions = new Bag<Transition>();
	private boolean canBeChanged = true;

	private EntityStateMachine machine;

	public EntityState(EntityStateMachine machine) {
		this.machine = machine;
	}

	public boolean canBeChanged() {
		return canBeChanged;
	}

	public void setCanBeChanged(boolean canBeChanged) {
		this.canBeChanged = canBeChanged;
	}

	public EntityStateMachine getMachine() {
		return machine;
	}

	public EntityState addComponent(Component component) {
		components.add(component);
		return this;
	}

	public Bag<Component> getComponents() {
		return components;
	}

	public EntityState addTransition(AnimatorController animatorController, EntityState outState, TransitionCondition transitionCondition) {
		return addTransition(new Transition(animatorController, outState, transitionCondition));
	}

	public EntityState addTransition(Transition transition) {
		transitions.add(transition);
		return this;
	}

	public Bag<Transition> getTransitions() {
		return transitions;
	}
}
