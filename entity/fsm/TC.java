package com.velidev.uberfury.entity.fsm;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.Manager;
import com.artemis.World;
import com.artemis.annotations.Wire;
import com.velidev.uberfury.entity.components.simple.JumpConditions;
import com.velidev.uberfury.entity.components.simple.MoveConditions;
import com.velidev.uberfury.entity.components.simple.MoveDirection;
import com.velidev.uberfury.entity.components.simple.triggers.SlashTrigger;
import com.velidev.uberfury.entity.fsm.animation.TransitionCondition;
import com.velidev.uberfury.gameobjects.Direction;

/**Transition Conditions*/
public class TC {
	public static void initialize(World world) {
		world.setManager((Manager) AnyStateToJump.getInstance());
		world.setManager((Manager) JumpToIdle.getInstance());
		world.setManager((Manager) MoveRightToIdleRight.getInstance());
		world.setManager((Manager) IdleRightToMoveRight.getInstance());
		world.setManager((Manager) AnyStateToAttack.getInstance());
		world.setManager((Manager) MoveLeftToIdleLeft.getInstance());
		world.setManager((Manager) IdleLeftToMoveLeft.getInstance());
		world.setManager((Manager) AttackToIdle.getInstance());
		world.setManager((Manager) LeftJumpToRightJump.getInstance());
		world.setManager((Manager) RightJumpToLeftJump.getInstance());
	}

	@Wire
	public static class IdleLeftToMoveLeft extends Manager implements TransitionCondition {
		private ComponentMapper<MoveConditions> mMoveConditions;
		private static IdleLeftToMoveLeft idleLeftToMoveLeft = new IdleLeftToMoveLeft();

		@Override
		public boolean evaluate(Entity e) {
			MoveConditions mc = mMoveConditions.get(e);
			return mc.movingLeft;
		}

		public static TransitionCondition getInstance() {
			return idleLeftToMoveLeft;
		}
	}

	@Wire
	public static class MoveLeftToIdleLeft extends Manager implements TransitionCondition {
		private ComponentMapper<MoveConditions> mMoveConditions;
		private static MoveLeftToIdleLeft moveLeftToIdleLeft = new MoveLeftToIdleLeft();

		@Override
		public boolean evaluate(Entity e) {
			MoveConditions mc = mMoveConditions.get(e);
			return !mc.movingLeft;
		}

		public static TransitionCondition getInstance() {
			return moveLeftToIdleLeft;
		}
	}

	@Wire
	public static class IdleRightToMoveRight extends Manager implements TransitionCondition {
		private ComponentMapper<MoveConditions> mMoveConditions;
		private static IdleRightToMoveRight idleRightToMoveRight = new IdleRightToMoveRight();

		@Override
		public boolean evaluate(Entity e) {
			MoveConditions mc = mMoveConditions.get(e);
			return mc.movingRight;
		}

		public static TransitionCondition getInstance() {
			return idleRightToMoveRight;
		}
	}

	@Wire
	public static class MoveRightToIdleRight extends Manager implements TransitionCondition {
		private ComponentMapper<MoveConditions> mMoveConditions;
		private static MoveRightToIdleRight moveRightToIdleRight = new MoveRightToIdleRight();

		@Override
		public boolean evaluate(Entity e) {
			MoveConditions mc = mMoveConditions.get(e);
			return !mc.movingRight;
		}

		public static TransitionCondition getInstance() {
			return moveRightToIdleRight;
		}
	}

	@Wire
	public static class AnyStateToJump extends Manager implements TransitionCondition {
		private ComponentMapper<JumpConditions> mJumpConditions;
		private static AnyStateToJump anyStateToJump = new AnyStateToJump();

		@Override
		public boolean evaluate(Entity e) {
			JumpConditions jc = mJumpConditions.get(e);
			return jc.inAir;
		}

		public static TransitionCondition getInstance() {
			return anyStateToJump;
		}
	}

	@Wire
	public static class JumpToIdle extends Manager implements TransitionCondition {
		private ComponentMapper<JumpConditions> mJumpConditions;
		private static JumpToIdle jumpToIdle = new JumpToIdle();

		@Override
		public boolean evaluate(Entity e) {
			JumpConditions jc = mJumpConditions.get(e);
			return !jc.inAir;
		}

		public static TransitionCondition getInstance() {
			return jumpToIdle;
		}
	}

	@Wire
	public static class LeftJumpToRightJump extends Manager implements TransitionCondition {
		private ComponentMapper<MoveDirection> mMoveDirection;
		private static LeftJumpToRightJump leftJumpToRightJump = new LeftJumpToRightJump();

		@Override
		public boolean evaluate(Entity e) {
			MoveDirection md = mMoveDirection.get(e);
			return md.moveDirection == Direction.RIGHT;
		}

		public static TransitionCondition getInstance() {
			return leftJumpToRightJump;
		}
	}

	@Wire
	public static class RightJumpToLeftJump extends Manager implements TransitionCondition {
		private ComponentMapper<MoveDirection> mMoveDirection;
		private static RightJumpToLeftJump fromRightJumpToLeftJump = new RightJumpToLeftJump();

		@Override
		public boolean evaluate(Entity e) {
			MoveDirection md = mMoveDirection.get(e);
			return md.moveDirection == Direction.LEFT;
		}

		public static TransitionCondition getInstance() {
			return fromRightJumpToLeftJump;
		}
	}

	@Wire
	public static class AnyStateToAttack extends Manager implements TransitionCondition {
		private ComponentMapper<SlashTrigger> mSlashTrigger;
		private static AnyStateToAttack anyStateToAttack = new AnyStateToAttack();

		@Override
		public boolean evaluate(Entity e) {
			SlashTrigger trigger = mSlashTrigger.get(e);
			boolean slashed = trigger.slashed;
			trigger.slashed = false;
			return slashed;
		}

		public static TransitionCondition getInstance() {
			return anyStateToAttack;
		}
	}

	@Wire
	public static class AttackToIdle extends Manager implements TransitionCondition {
		private ComponentMapper<SlashTrigger> mSlashTrigger;
		private static AttackToIdle attackToIdle = new AttackToIdle();

		@Override
		public boolean evaluate(Entity e) {
			SlashTrigger trigger = mSlashTrigger.get(e);
			boolean slashed = trigger.slashed;
			trigger.slashed = false;
			return !slashed;
		}

		public static TransitionCondition getInstance() {
			return attackToIdle;
		}
	}
}
