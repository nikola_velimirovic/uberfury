package com.velidev.uberfury.entity.fsm.animation;

import com.artemis.Entity;

public interface TransitionCondition {
	public boolean evaluate(Entity e);
}
