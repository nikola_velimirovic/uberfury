package com.velidev.uberfury.entity.fsm.animation;

import com.artemis.Entity;
import com.velidev.uberfury.entity.components.AnimationComponent;
import com.velidev.uberfury.entity.fsm.EntityState;
import com.velidev.uberfury.entity.fsm.EntityStateMachine;

public class AnimatorController extends EntityStateMachine {
	public AnimatorController(Entity e) {
		super(e);
	}

	public EntityState addAnimationState(String key, AnimationComponent animationComponent) {
		EntityState entityState = createState(key);
		animationComponent.currentState = entityState;
		entityState.addComponent(animationComponent);
		return entityState;
	}

	@Override
	public void changeStateTo(EntityState newState) {
		if(newState != null) {
			refreshComponentOf(newState);
		}
		super.changeStateTo(newState);
	}

	private void refreshComponentOf(EntityState newState) {
		AnimationComponent animationComponent = (AnimationComponent) newState.getComponents().get(0);
		animationComponent.stateTime = 0;
		if(animationComponent.isAtomic) {
			newState.setCanBeChanged(false);
		}
	}
}
