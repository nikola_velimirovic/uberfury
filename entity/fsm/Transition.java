package com.velidev.uberfury.entity.fsm;

import com.velidev.uberfury.entity.fsm.animation.TransitionCondition;

public class Transition {
	private EntityStateMachine machine;
	private EntityState outState;
	private TransitionCondition transitionCondition;

	public Transition(EntityStateMachine machine, EntityState outState, TransitionCondition transitionCondition) {
		this.machine = machine;
		this.outState = outState;
		this.transitionCondition = transitionCondition;
	}

	public boolean evaluate() {
		boolean evaluation = transitionCondition.evaluate(machine.getEntity());
		if(evaluation == true) {
			machine.changeStateTo(outState);
		}
		return evaluation;
	}
}
