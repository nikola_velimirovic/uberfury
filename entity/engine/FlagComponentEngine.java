package com.velidev.uberfury.entity.engine;


import com.artemis.Component;
import com.artemis.World;

import java.util.HashMap;

public class FlagComponentEngine {
	private World world;
	private HashMap<Class<? extends Component>, Component> components;

	public FlagComponentEngine(World world) {
		this.world = world;
		components = new HashMap<Class<? extends Component>, Component>();
	}

	public <T extends Component> T getComponent(Class<T> componentType) {
		T component = (T)components.get(componentType);
		if(component == null) {
			components.put(componentType, component);
		}
		return component;
	}
}
