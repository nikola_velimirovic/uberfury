package com.velidev.uberfury.physics;

public interface Updateable {
    public void update(float delta);
}
