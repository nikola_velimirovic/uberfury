package com.velidev.uberfury.physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.assets.Level;
import com.velidev.uberfury.entity.EntityContactListener;
import com.velidev.uberfury.game.Uberfury;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Bullet;

public class LocalWorld extends PhysicsWorld {
    private static final int MAX_PLAYERS = 4;//ovo ce da se izmeni

    private Level level;
    public World b2World; //vrati na private ili protected
    private float width, height, ratio;
    public Array<Player> players = new Array<Player>(MAX_PLAYERS);//VRATI NA PRIVATE

   //TRENUTNE GLUPOSTI
    private Uberfury igra;
    private Vector2 gravity = new Vector2(0, -35f);

    private Arena arena = new Arena(b2World);
    public static Player player1;
    public  Player player2;
    private Body svet;
    private Box2DDebugRenderer b2Rend = new Box2DDebugRenderer();

    public void initWorld() {
	    b2Rend.setDrawBodies(true);
	    b2Rend.setDrawInactiveBodies(false);
        b2World.setContactListener(new EntityContactListener());
        ratio = (float)Gdx.graphics.getHeight() / (float)Gdx.graphics.getWidth();
    }


    public LocalWorld(float worldWidth, float worldHeight) {
        width = worldWidth; height = worldHeight;
    }

    public void setB2World(World b2World) {
        this.b2World = b2World;
    }

    @Override
    public void update(float delta) {
        for (Player player : players) {
            //player.update(delta);
        }
        for (Bullet bullet : Bullet.toDelete) {
            bullet.delete();
        }
        for(Player player : Player.playersToDie) {
            player.die();
        }

        b2World.step(delta, 1, 1);
    }

    public void render(Matrix4 matrix4) {
        b2Rend.render(b2World, matrix4);
    }

    public Player getPlayer() {
        return player1;
    }
}
