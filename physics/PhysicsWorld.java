package com.velidev.uberfury.physics;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;

public abstract class PhysicsWorld implements Updateable {

    public abstract void render(Matrix4 matrix4);

    public abstract void setB2World(World b2World);
}
