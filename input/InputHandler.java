package com.velidev.uberfury.input;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.velidev.uberfury.entity.managers.action.ActionFacade;

public abstract class InputHandler extends Stage {
    protected ActionFacade actionFacade;

    public InputHandler(ActionFacade actionFacade) {
        this.actionFacade = actionFacade;
    }

    public void setActionFacade(ActionFacade actionFacade) {
        this.actionFacade = actionFacade;
    }
}
