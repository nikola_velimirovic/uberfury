package com.velidev.uberfury.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.velidev.uberfury.entity.managers.action.ActionFacade;

public class TouchInputHandler extends InputHandler {

    private class MovementListener extends InputListener {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(x > Gdx.graphics.getWidth() / 2 && y < Gdx.graphics.getHeight()/3) {
	            actionFacade.startMelee();
	            return true;
            }
	        if(x > Gdx.graphics.getWidth() / 2 && y > Gdx.graphics.getHeight()/3 && y < 2*(Gdx.graphics.getHeight()/3)) {
		        actionFacade.jump();
		        return true;
	        }
            if(x > Gdx.graphics.getWidth() / 2 && y > 2*(Gdx.graphics.getHeight()/3)) {
                actionFacade.startRanged();
                return true;
            }
            if(x < Gdx.graphics.getWidth()/2 && x > Gdx.graphics.getWidth()/4) {
                actionFacade.moveRight();
                return true;
            }
            if(x < Gdx.graphics.getWidth() / 4) {
                actionFacade.moveLeft();
                return true;
            }
            return true;
        }

        @Override
        public void touchDragged(InputEvent event, float x, float y, int pointer) {
//            if(touchpad.getKnobPercentX() > 0) {
//                actionFacade.stopMovingLeft();
//                actionFacade.moveRight();
//           } else if(touchpad.getKnobPercentX() < 0) {
//                actionFacade.stopMovingRight();
//                actionFacade.moveLeft();
//            }
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            if(x > Gdx.graphics.getWidth() / 2 && y < Gdx.graphics.getHeight()/3) {
                actionFacade.stopMelee();
                return;
            }
            if(x > Gdx.graphics.getWidth() / 2 && y > Gdx.graphics.getHeight()/3 && y < 2*(Gdx.graphics.getHeight()/3)) {
                return;
            }
            if(x > Gdx.graphics.getWidth() / 2 && y > 2*(Gdx.graphics.getHeight()/3)) {
                actionFacade.stopRanged();
                return;
            }
            if(x < Gdx.graphics.getWidth()/2 && x > Gdx.graphics.getWidth()/4) {
                actionFacade.stopMovingRight();
                return;
            }
            if(x < Gdx.graphics.getWidth() / 4) {
                actionFacade.stopMovingLeft();
                return;
            }
        }
    }

    private Skin touchpadSkin = new Skin();
    private Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle();
    private Drawable touchBackground, touchKnob;
    public Touchpad touchpad;

    public Button jumpButton;

    public TouchInputHandler(ActionFacade actionFacade) {
        super(actionFacade);
        MovementListener movementListener = new MovementListener();
//        initTouchpad(movementListener);
        addListener(movementListener);
    }

    public void initTouchpad(MovementListener movementListener) {
        touchpadSkin.add("touchBackground", new Texture(Gdx.files.internal("data/touchBackground.png")));
        touchpadSkin.add("touchKnob", new Texture(Gdx.files.internal("data/touchKnob.png")));

        touchBackground = touchpadSkin.getDrawable("touchBackground");
        touchKnob = touchpadSkin.getDrawable("touchKnob");

        touchpadStyle.background = touchBackground;
        touchpadStyle.knob = touchKnob;

        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();

        touchKnob.setMinWidth(screenHeight / 7);
        touchKnob.setMinHeight(screenHeight / 7);

        touchpad = new Touchpad(screenHeight / 12, touchpadStyle);
        touchpad.setBounds(screenWidth / 30, screenHeight / 20, screenHeight / 3, screenHeight / 3);

        touchBackground.setMinHeight(screenHeight / 5);
        touchBackground.setMinWidth(screenHeight / 5);

        addActor(touchpad);

        touchpad.addListener(movementListener);
    }

    public void initJumpButton() {
        jumpButton = new Button(touchKnob);
        jumpButton.setPosition(Gdx.graphics.getWidth() * 4 / 5, Gdx.graphics.getHeight() * 1 / 5);
    }
}
