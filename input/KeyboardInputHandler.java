package com.velidev.uberfury.input;

import com.badlogic.gdx.Input;
import com.velidev.uberfury.entity.managers.action.ActionFacade;

public class KeyboardInputHandler extends InputHandler {
    protected ActionFacade actionFacade2;

    public KeyboardInputHandler(ActionFacade actionFacade1, ActionFacade actionFacade2) {
        super(actionFacade1);
        this.actionFacade2 = actionFacade2;
        instance = this;
    }

    public static KeyboardInputHandler instance;

    public static ActionFacade getF1() {
        return instance.actionFacade;
    }

    public static ActionFacade getF2() {
        return instance.actionFacade2;
    }

    @Override
    public synchronized boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.A:
                actionFacade.moveLeft();
                break;
            case Input.Keys.W:
                actionFacade.lookUp();
                break;
            case Input.Keys.S:
                actionFacade.lookDown();
                break;
            case Input.Keys.D:
                actionFacade.moveRight();
                break;
        }
        if (keycode == Input.Keys.V) {
            actionFacade.startMelee();
        }
        if (keycode == Input.Keys.B) {
            actionFacade.jump();
        }
        if (keycode == Input.Keys.N) {
            actionFacade.startRanged();
        }
	    if (keycode == Input.Keys.J) {
//		    Shake shake = UberFury.cam.createComponent(Shake.class);
//		    shake.maxRadius = 0.5f;
//			UberFury.cam.addComponent(shake);
//		    UberFury.cam.changedInWorld();
	    }

        switch (keycode) {
            case Input.Keys.NUMPAD_4:
                actionFacade2.moveLeft();
                break;
            case Input.Keys.NUMPAD_8:
                actionFacade2.lookUp();
                break;
            case Input.Keys.NUMPAD_5:
                actionFacade2.lookDown();
                break;
            case Input.Keys.NUMPAD_6:
                actionFacade2.moveRight();
                break;
        }
        if (keycode == Input.Keys.I) {
            actionFacade2.startMelee();
        }
        if (keycode == Input.Keys.O) {
            actionFacade2.jump();
        }
        if (keycode == Input.Keys.P) {
            actionFacade2.startRanged();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode) {
            case Input.Keys.A:
                actionFacade.stopMovingLeft();
                break;
            case Input.Keys.D:
                actionFacade.stopMovingRight();
                break;
            case Input.Keys.W:
                actionFacade.stopLookingUp();
                break;
            case Input.Keys.S:
                actionFacade.stopLookingDown();
                break;
        }

        switch(keycode) {
            case Input.Keys.NUMPAD_4:
                actionFacade2.stopMovingLeft();
                break;
            case Input.Keys.NUMPAD_6:
                actionFacade2.stopMovingRight();
                break;
            case Input.Keys.NUMPAD_8:
                actionFacade2.stopLookingUp();
                break;
            case Input.Keys.NUMPAD_5:
                actionFacade2.stopLookingDown();
                break;
        }

        if(keycode == Input.Keys.V) {
            actionFacade.stopMelee();
        }
        if (keycode == Input.Keys.N) {
            actionFacade.stopRanged();
        }

        if(keycode == Input.Keys.I) {
            actionFacade2.stopMelee();
        }
        if (keycode == Input.Keys.P) {
            actionFacade2.stopRanged();
        }
        return false;
    }
}
