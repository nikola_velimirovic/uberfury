package com.velidev.uberfury.rendering;

public interface Renderable {
    public void render();
}
