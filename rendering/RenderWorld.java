package com.velidev.uberfury.rendering;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.velidev.uberfury.game.Uberfury;

public class RenderWorld implements Renderable {

    private OrthographicCamera camera;

    public RenderWorld(OrthographicCamera camera) {
        this.camera = camera;
    }

    @Override
    public void render() {
        SpriteBatch batch = Uberfury.batch;
        batch.setProjectionMatrix(camera.combined);
        batch.begin();


        batch.end();
    }

}
