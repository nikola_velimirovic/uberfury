package com.velidev.uberfury.physics;

/**
 * Created by Luka on 1/23/14.
 */
public interface Updateable {
    public void update(float delta);
}
