package com.velidev.uberfury.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Luka on 1/22/14.
 */
public class MovementHandler {
    private static Vector2 impulse = new Vector2(0, 0);

    public static void moveUp(Body body, float speed) {
        float velocityChange = speed - body.getLinearVelocity().y;
        float impulseY = body.getMass() * velocityChange;
        impulse.set(0, impulseY);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void moveDown(Body body, float speed) {
        float velocityChange = speed + body.getLinearVelocity().y;
        float impulseY = body.getMass() * velocityChange;
        impulse.set(0, -impulseY);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void moveLeft(Body body, float speed) {
        float velocityChange = speed + body.getLinearVelocity().x;
        float impulseX = body.getMass() * velocityChange;
        impulse.set(-impulseX, 0);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void moveRight(Body body, float speed) {
        float velocityChange = speed - body.getLinearVelocity().x;
        float impulseX = body.getMass() * velocityChange;
        impulse.set(impulseX, 0);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void stopXMovement(Body body) {
        float impulseX = body.getMass() * body.getLinearVelocity().x;
        impulse.set(-impulseX, 0);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void stopYMovement(Body body) {
        float impulseY = body.getMass() * body.getLinearVelocity().y;
        impulse.set(0, -impulseY);
        body.applyLinearImpulse(impulse, body.getWorldCenter(), true);
    }

    public static void stopLinearMovement(Body body) {
        body.setLinearVelocity(0, 0);
    }

    public static void stopRotation(Body body) {
        float rotationImpulse = body.getAngularVelocity();
        body.applyAngularImpulse(-rotationImpulse, true);
    }

    public static void pushLeft(Body body, float speed) {
        float velocityChange = speed + body.getLinearVelocity().x;
        float impulseX = body.getMass() * velocityChange;
        impulse.set(-impulseX, 0);
        body.applyForce(impulse, body.getWorldCenter(), true);
    }

    public static void jumpUp(Body body, float speed) {
        moveUp(body, speed);
    }

    public static void jumpLeft(Body body, float speed) {
        moveUp(body, speed);
        moveLeft(body, speed);
    }

    public static void jumpRight(Body body, float speed) {
        moveUp(body, speed);
        moveRight(body, speed);
    }
}
