package com.velidev.uberfury.physics;

/**
 * Created by Luka on 1/22/14.
 */
public interface ActionHandler {

    /*Metode za pomeranje igraca*/
    public void moveLeft(int playerID);

    public void moveRight(int playerID);

    public void lookUp(int playerID);

    public void lookDown(int playerID);

    public void stopMovingLeft(int playerID);

    public void stopMovingRight(int playerID);

    public void stopLookingDown(int playerID);

    public void stopLookingUp(int playerID);

    public void jump(int playerID);

    /*Metode za ubivanje*/
    public void slash(int playerID);

    public void shoot(int playerID);

}
