package com.velidev.uberfury.physics;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import java.io.IOException;

/**
 * Created by Luka on 1/23/14.
 */
public class PhysicsWorldProxy extends PhysicsWorld {
    ActionHandler actionHandler;

    public PhysicsWorldProxy() {

        Client client = new Client();
        try {

            client.start();

            Kryo kryo = client.getKryo();
            kryo.register(ActionHandler.class);

            final ObjectSpace clientSpace = new ObjectSpace();

            clientSpace.registerClasses(kryo);

            client.addListener(new Listener(){
                @Override
                public void connected(Connection connection) {
                    connection.setTimeout(0);
                    actionHandler = clientSpace.getRemoteObject(connection, 42, ActionHandler.class);

                }
            });

            client.connect(5000, "192.168.2.4", 54000, 55000);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void render(Matrix4 matrix4) {

    }

    @Override
    public void setB2World(World b2World) {

    }

    @Override
    public void moveLeft(int playerID) {
        actionHandler.moveLeft(playerID);
    }

    @Override
    public void moveRight(int playerID) {
        actionHandler.moveRight(playerID);
    }

    @Override
    public void lookUp(int playerID) {
        actionHandler.lookUp(playerID);
    }

    @Override
    public void lookDown(int playerID) {
        actionHandler.lookDown(playerID);
    }

    @Override
    public void stopMovingLeft(int playerID) {
        actionHandler.stopMovingLeft(playerID);
    }

    @Override
    public void stopMovingRight(int playerID) {
        actionHandler.stopMovingRight(playerID);
    }

    @Override
    public void stopLookingDown(int playerID) {
        actionHandler.stopLookingDown(playerID);
    }

    @Override
    public void stopLookingUp(int playerID) {
        actionHandler.stopLookingUp(playerID);
    }

    @Override
    public void jump(int playerID) {
        actionHandler.jump(playerID);
    }

    @Override
    public void slash(int playerID) {
        actionHandler.slash(playerID);
    }

    @Override
    public void shoot(int playerID) {
        actionHandler.shoot(playerID);
    }

    @Override
    public void update(float delta) {

    }
}
