package com.velidev.uberfury.physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Bullet;
import com.velidev.uberfury.levels.Level;
import com.velidev.uberfury.physics.contact.PhysicsContactListener;

/**
 * Created by Luka on 1/23/14.
 */
public class LocalWorld extends PhysicsWorld {
    private static final int MAX_PLAYERS = 4;//ovo ce da se izmeni

    private Level level;
    public World b2World; //vrati na private ili protected
    private float width, height, ratio;
    public Array<Player> players = new Array<Player>(MAX_PLAYERS);//VRATI NA PRIVATE

   //TRENUTNE GLUPOSTI
    private Box2DDebugRenderer b2rend;
    private UberFury igra;
    private Vector2 gravity = new Vector2(0, -35f);

    private Arena arena = new Arena(b2World);
    public Player player1;
    public  Player player2;
    private Body svet;
    private Box2DDebugRenderer b2Rend = new Box2DDebugRenderer();;

    public void initWorld() {
        b2World.setContactListener(new PhysicsContactListener());
        ratio = (float)Gdx.graphics.getHeight() / (float)Gdx.graphics.getWidth();
    }


    public LocalWorld(UberFury igra, float worldWidth, float worldHeight) {
        width = worldWidth; height = worldHeight;
        //treba da vidim ovo
        this.igra = igra;
    }

    public void setB2World(World b2World) {
        this.b2World = b2World;
    }

    @Override
    public void update(float delta) {
        for (Player player : players) {
            player.update(delta);
        }
        for (Bullet bullet : Bullet.toDelete) {
            bullet.delete();
        }
        for(Player player : Player.playersToDie) {
            player.die();
        }

        b2World.step(delta, 8, 3);
    }

    public void render(Matrix4 matrix4) {
        b2Rend.render(b2World, matrix4);
    }

    public Player getPlayer() {
        return player1;
    }

    @Override
    public void moveLeft(int playerID) {
        Player player = players.get(playerID);
        player.moveLeft();
    }

    @Override
    public void moveRight(int playerID) {
        Player player = players.get(playerID);
        player.moveRight();
    }

    @Override
    public void lookUp(int playerID) {
        players.get(playerID).lookUp();
    }

    @Override
    public void lookDown(int playerID) {
        players.get(playerID).lookDown();
    }

    @Override
    public void stopMovingLeft(int playerID) {
        Player player = players.get(playerID);
        player.stopMovingLeft();
    }

    @Override
    public void stopMovingRight(int playerID) {
        Player player = players.get(playerID);
        player.stopMovingRight();
    }

    @Override
    public void stopLookingDown(int playerID) {
        players.get(playerID).stopLookingDown();
    }

    @Override
    public void stopLookingUp(int playerID) {
        players.get(playerID).stopLookingUp();
    }

    @Override
    public void jump(int playerID) {
        Player player = players.get(playerID);
        player.jump();
    }

    @Override
    public void slash(int playerID) {
        players.get(playerID).slash();
    }

    @Override
    public void shoot(int playerID) {
        Player player = players.get(playerID);
        player.shoot();
    }
}
