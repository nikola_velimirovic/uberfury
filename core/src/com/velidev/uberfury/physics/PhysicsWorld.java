package com.velidev.uberfury.physics;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Luka on 1/23/14.
 */
public abstract class PhysicsWorld implements ActionHandler, Updateable {

    public abstract void render(Matrix4 matrix4);

    public abstract void setB2World(World b2World);
}
