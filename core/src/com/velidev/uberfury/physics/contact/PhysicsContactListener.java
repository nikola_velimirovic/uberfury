package com.velidev.uberfury.physics.contact;

import com.badlogic.gdx.physics.box2d.*;
import com.velidev.uberfury.gameobjects.GameObject;


/**
 * Created by Luka on 1/25/14.
 */
public class PhysicsContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        if(null == fa) {
            return;
        }
        Fixture fb = contact.getFixtureB();
        if(null == fb) {
            return;
        }
        GameObject a = (GameObject) fa.getBody().getUserData();
        GameObject b = (GameObject) fb.getBody().getUserData();
        a.beginContact(b, fa, fb, contact, true);
        b.beginContact(a, fb, fa, contact, false);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        if(null == fa) {
            return;
        }
        Fixture fb = contact.getFixtureB();
        if(null == fb) {
            return;
        }
        GameObject a = (GameObject) fa.getBody().getUserData();
        GameObject b = (GameObject) fb.getBody().getUserData();
        a.endContact(b, fa, fb, contact, true);
        b.endContact(a, fb, fa, contact, false);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Fixture fa = contact.getFixtureA();
        if(null == fa) {
            return;
        }
        Fixture fb = contact.getFixtureB();
        if(null == fb) {
            return;
        }
        GameObject a = (GameObject) fa.getBody().getUserData();
        GameObject b = (GameObject) fb.getBody().getUserData();
        a.preSolve(b, fa, fb, contact, oldManifold);
        b.preSolve(a, fb, fa, contact, oldManifold);
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Fixture fa = contact.getFixtureA();
        if(null == fa) {
            return;
        }
        Fixture fb = contact.getFixtureB();
        if(null == fb) {
            return;
        }
        GameObject a = (GameObject) fa.getBody().getUserData();
        GameObject b = (GameObject) fb.getBody().getUserData();
        a.postSolve(b, fa, fb, contact, impulse);
        b.postSolve(a, fb, fa, contact, impulse);
    }
}
