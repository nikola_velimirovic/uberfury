package com.velidev.uberfury.physics.contact;

/**
 * Created by Luka on 1/26/14.
 * Sluzi za definisanje kategorija za proveru sudara dve fixture(da li treba da se sudaraju ili ne).
 * Fixturi se podese category bits, sto oznacava koje je ona kategorije i podese se maskbits,
 * sto oznacava s kojim se kategorijama ona sudara. Kategorije su 1(...0001), 2(...0010), 4(...0100)...
 * ukupno ih ima 16, za po jedan bit u 16-bitnoj reci. Npr. zelimo da se PLAYER sudara sa ARENA i NESTO,
 * ali ne i sa PLAYER kategorijom, onda podesimo:
 *      fixture.categorybits = PLAYER;
 *      fixture.maskbits = ARENA | NESTO;
 * da bi se sudarao i sa PLAYER kategorijom samo dodamo
 *      fixture.maskbits = ARENA | NESTO | PLAYER;
 */

public class ContactCategories {
    public static final short ARENA = 0x0001;
    public static final short PLAYER = 0x0002;
    public static final short AMMO = 0x0004;
    public static final short SENSOR = 0x0008;
    public static final short MELEE_WEAPON = 0x0010;
    public static final short CORPSE = 0x0020;
    public static final short PLATFORM = 0x0040;
}
