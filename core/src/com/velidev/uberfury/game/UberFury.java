package com.velidev.uberfury.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.velidev.uberfury.gameobjects.players.Samurai;
import com.velidev.uberfury.inputhandling.DesktopInputHandler;
import com.velidev.uberfury.inputhandling.InputHandler;
import com.velidev.uberfury.inputhandling.MobileInputHandler;
import com.velidev.uberfury.levels.LevelLoader;
import com.velidev.uberfury.physics.LocalWorld;
import com.velidev.uberfury.rendering.RenderWorld;
import com.velidev.uberfury.screens.GameScreen;
import com.velidev.uberfury.screens.MainMenuScreen;

public class UberFury extends Game {
    public MainMenuScreen mainMenuScreen;
    public static SpriteBatch batch;
    public GameScreen gameScreen;
    public static AssetManager assetManager;

    public InputHandler inputHandler;

    @Override
    public void create() {
        batch = new SpriteBatch();

        AssetManager am = new AssetManager();
        //am.load("data/out/pack.atlas", TextureAtlas.class);

        //am.finishLoading();

        //TextureAtlas ta = am.get("data/out/pack.atlas");


        //POSTAVI SE SPLASH SCREEN

        //UCITA SE MAIN MENU SCREEN
        mainMenuScreen = new MainMenuScreen(this);

        float width = 24;

        RenderWorld rw = new RenderWorld(new OrthographicCamera());


        if(Gdx.app.getType() == Application.ApplicationType.Android) {



            LocalWorld lw = new LocalWorld(this, width, width * ((float)Gdx.graphics.getHeight()/Gdx.graphics.getWidth()));

            LevelLoader ld = new LevelLoader();
            ld.load(lw, rw);

            lw.initWorld();
            lw.player1 = new Samurai(lw.b2World, rw);
            lw.player1.summon(0f, 1f);
            lw.players.add(lw.player1);

            inputHandler = new MobileInputHandler(this, lw);
            gameScreen = new GameScreen(this, lw, rw);

        } else {
            LocalWorld lw = new LocalWorld(this, width, width * ((float)Gdx.graphics.getHeight()/Gdx.graphics.getWidth()));

            LevelLoader ld = new LevelLoader();
            ld.load(lw, rw);

            lw.initWorld();
            lw.player1 = new Samurai(lw.b2World, rw);
            lw.player1.summon(0f, 1f);
            lw.players.add(lw.player1);

            inputHandler = new DesktopInputHandler(this, lw);
            gameScreen = new GameScreen(this, lw, rw);
            /*
            if(Gdx.app.getType() != Application.ApplicationType.WebGL) {
                try {
                    Server server = new Server();
                    server.start();
                    server.bind(54000, 55000);

                    Kryo kryo = server.getKryo();
                    kryo.register(ActionHandler.class);

                    final ObjectSpace serverSpace = new ObjectSpace();

                    serverSpace.registerClasses(kryo);

                    serverSpace.register(42, lw);
                    server.addListener(new Listener() {
                        @Override
                        public void connected(Connection connection) {
                            connection.setTimeout(0);
                            serverSpace.addConnection(connection);
                            Gdx.app.error("LUKAAAAAAAAA", "connected");
                        }

                        @Override
                        public void received(Connection connection, Object o) {
                            Gdx.app.error("LUKA", "received");
                            super.received(connection, o);
                        }
                    });
                } catch (Exception e) {
                    Gdx.app.error("Lukaveli", "exception", e);
                }
            }*/

        }
        Gdx.input.setInputProcessor(inputHandler);
        setScreen(gameScreen);
    }

    @Override
    public void render() {
        super.render();
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            inputHandler.act();
            inputHandler.draw();
        }
    }
}
