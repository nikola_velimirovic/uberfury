package com.velidev.uberfury.rendering;

/**
 * Created by Luka on 1/23/14.
 */
public interface Renderable {
    public void render();
}
