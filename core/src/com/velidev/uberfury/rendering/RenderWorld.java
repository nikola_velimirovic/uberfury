package com.velidev.uberfury.rendering;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.objectviews.ObjectView;

/**
 * Created by Luka on 1/23/14.
 */
public class RenderWorld implements Renderable {

    private OrthographicCamera camera;

    private Array<ObjectView> views = new Array<ObjectView>();

    public RenderWorld(OrthographicCamera camera) {
        this.camera = camera;
    }

    @Override
    public void render() {
        SpriteBatch batch = UberFury.batch;
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        //ovde ide rendering svih objekata u igri
        for (ObjectView view : views) {
            view.render();
        }

        batch.end();
    }

    public void addView(ObjectView view) {
        views.add(view);
    }
}
