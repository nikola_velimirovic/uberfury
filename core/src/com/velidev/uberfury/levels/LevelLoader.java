package com.velidev.uberfury.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.gushikustudios.rube.RubeScene;
import com.gushikustudios.rube.loader.RubeSceneLoader;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.arena.OneWayPlatform;
import com.velidev.uberfury.physics.PhysicsWorld;
import com.velidev.uberfury.physics.contact.ContactCategories;
import com.velidev.uberfury.rendering.RenderWorld;


/**
 * Created by Luka on 4/5/2014.
 */
public class LevelLoader {
    protected static RubeSceneLoader loader = new RubeSceneLoader();
    protected static RubeScene rubeScene;

    private Vector2 gravity = new Vector2(0, -70f);

    public void load(PhysicsWorld physicsWorld, RenderWorld renderWorld) {
        rubeScene = loader.loadScene(Gdx.files.internal("test1.json"));
        World b2World = rubeScene.getWorld();

        b2World.setGravity(gravity);

        physicsWorld.setB2World(b2World);

        Array<Body> bodies = rubeScene.getBodies();



        for (Body body : bodies) {
            body.setFixedRotation(true);
            for(Fixture fix : body.getFixtureList() ){
                Filter f = fix.getFilterData();
                f.categoryBits = ContactCategories.ARENA;
                f.maskBits = ContactCategories.PLAYER | ContactCategories.SENSOR | ContactCategories.ARENA;
                fix.setFilterData(f);
            }
            Arena arena = new Arena(b2World);
            body.setUserData(arena);
            arena.setBody(body);
        }


        Array<Body> a = rubeScene.getNamed(Body.class, "platform");
        for (Body b : a) {
            OneWayPlatform p = new OneWayPlatform(b2World);
            b.setUserData(p);
            p.setBody(b);

        }
    }
}
