package com.velidev.uberfury.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.physics.LocalWorld;
import com.velidev.uberfury.physics.PhysicsWorld;
import com.velidev.uberfury.rendering.RenderWorld;
import com.velidev.uberfury.screens.camera.CameraHandler;


/**
 * Created by Luka on 1/16/14.
 */
public class GameScreen extends ScreenAdapter {
    private UberFury uberFury;
    private PhysicsWorld physicsWorld;
    private RenderWorld renderWorld;

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private CameraHandler cameraHandler;
    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    private ExtendViewport viewport;

    private Player player;

    private long nextTick, skipTicks = 15;
    float x = 0, y = 0;

    public GameScreen(UberFury uberFury, PhysicsWorld physicsWorld, RenderWorld renderWorld) {
        this.uberFury = uberFury;
        batch = uberFury.batch;
        this.physicsWorld = physicsWorld;
        this.renderWorld = renderWorld;
        initCamera();
    }

    private void initCamera() {
        camera = new OrthographicCamera();
        viewport = new ExtendViewport(24f, 24f * (screenWidth / screenHeight), camera);

        cameraHandler = new CameraHandler(camera, -20, 20, 200, 0);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.graphics.getGL20();
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0f, 0.1f, 0.3f, 0f);

        handleInput(delta);

        batch.setProjectionMatrix(camera.combined);

        long time = System.currentTimeMillis();

        if(nextTick == 0) {
            nextTick = time;
        }

        while(nextTick <= time) {
            nextTick += skipTicks;
            physicsWorld.update(skipTicks / 1000.0f);
        }

        cameraHandler.update(delta);
        camera.update();
        physicsWorld.render(camera.combined); //cisto u svrhe testiranja

        renderWorld.render();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cameraHandler.attachTo(((LocalWorld)physicsWorld).player1);
        ((LocalWorld)physicsWorld).player1.setCameraHandler(cameraHandler);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }


    private void handleInput(float delta) {
        float camMoveSpeed = 5 * delta;
        float camAccelerationFactor = 5;
        if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camMoveSpeed *= camAccelerationFactor;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (camera.position.x > 0)
                camera.translate(-camMoveSpeed, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (camera.position.x < 1024)
                camera.translate(camMoveSpeed, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            if (camera.position.y > 0)
                camera.translate(0, -camMoveSpeed, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            if (camera.position.y < 1024)
                camera.translate(0, camMoveSpeed, 0);
        }

        float camZoomSpeed = 1*delta;
        float camZoomAccelerationFactor = 5;
        if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            camZoomSpeed *= camZoomAccelerationFactor;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            camera.zoom += camZoomSpeed;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.X)) {
            camera.zoom -= camZoomSpeed;
        }
    }

}
