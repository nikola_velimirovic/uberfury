package com.velidev.uberfury.screens.camera;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.physics.Updateable;


/**
 * Created by Luka on 2/10/14.
 */
public class CameraHandler implements Updateable {
    private OrthographicCamera camera;
    private Player target;
    float leftBorder, rightBorder, upBorder, downBorder;
    private Vector2 windowPos = new Vector2(), targetPos, cameraPos = new Vector2();
    private float horizontalRadius, verticalRadius;
    private float leftR, upR, downR, rightR;
    private float camSpeed = 16;
    private float horizontalCamOffset, verticalCamOffset;

    private Arena platform;

    Mesh quad;

    public CameraHandler(OrthographicCamera camera, float leftBorder, float rightBorder, float upBorder, float downBorder) {
        this.camera = camera;
        this.leftBorder = leftBorder;
        this.rightBorder = rightBorder;
        this.upBorder = upBorder;
        this.downBorder = downBorder;
        horizontalRadius = leftR = rightR = camera.viewportWidth / 10;
        verticalRadius = upR = downR = camera.viewportHeight / 4;
    }

    public void setBorders(float left, float right, float up, float down) {
        leftBorder = left;
        rightBorder = right;
        upBorder = up;
        downBorder = down;
    }

    public void attachTo(Player player) {
        target = player;

        targetPos = target.getBody().getPosition();
        windowPos.set(targetPos.x, targetPos.y);

        float width = camera.viewportWidth;
        float height = camera.viewportHeight;

        horizontalRadius = leftR = rightR = width / 16;
        verticalRadius = downR = height / 4;
        upR = height / 3;

        if(windowPos.x < leftBorder + leftR) {
            windowPos.x = leftBorder + leftR;
        }

        if(windowPos.x > rightBorder - rightR) {
            windowPos.x = rightBorder - rightR;
        }

        if(windowPos.y < downBorder + downR) {
            windowPos.y = downBorder + downR;
        }

        if(windowPos.y > upBorder - upR) {
            windowPos.y = upBorder - upR;
        }

        camera.position.x = windowPos.x;
        camera.position.y = windowPos.y;
        cameraPos.set(camera.position.x, camera.position.y);

        if(cameraPos.x < leftBorder + width / 3) {
            camera.position.x = leftBorder + width / 3;
        }

        if(cameraPos.x > rightBorder - width / 3) {
            camera.position.x = rightBorder - width / 3;
        }

        if(cameraPos.y < downBorder + height / 3) {
            camera.position.y = downBorder + height / 3;
        }

        if(cameraPos.y > upBorder - height / 3) {
            camera.position.y = upBorder - height / 3;
        }

        cameraPos.set(camera.position.x, camera.position.y);

        verticalCamOffset = height / 12;

    }

    private void checkWindow() {
        PlayerAttributes a = target.getAttributes();

        float playerRightSide = targetPos.x + a.bodyWidth / 2;
        float windowRightSide =  windowPos.x + rightR;
        if(playerRightSide > windowRightSide) {
            horizontalCamOffset = camera.viewportWidth / 12;
            windowPos.x += playerRightSide - windowRightSide;
            //System.out.println("RIGHT: " + (windowPos.x + rightR));
        }

        float playerLeftSide = targetPos.x - a.bodyWidth / 2;
        float windowLeftSide = windowPos.x - leftR;
        if(playerLeftSide < windowLeftSide) {
            horizontalCamOffset = -camera.viewportWidth / 12;
            windowPos.x += playerLeftSide - windowLeftSide;
            //System.out.println("LEFT: " + (windowPos.x + -leftR));
        }

        float playerUpSide = targetPos.y + a.bodyHeight / 2;
        float windowUpSide = windowPos.y + upR;
        if(playerUpSide > windowUpSide) {
            windowPos.y += playerUpSide - windowUpSide;
            //System.out.println("UP: " + (windowPos.y + upR));
        }

        float playerDownSide = targetPos.y - a.bodyHeight / 2;
        float windowDownSide = windowPos.y - downR;
        if(playerDownSide < windowDownSide) {
            windowPos.y += playerDownSide - windowDownSide;
            //System.out.println("DOWN: " + (windowPos.y - downR));
        }
    }

    private void checkCamera(float delta) {
        float width = camera.viewportWidth;
        float height = camera.viewportHeight;

        float camSpeed = this.camSpeed;
        float velX = target.getBody().getLinearVelocity().x;

        float dx = (windowPos.x + horizontalCamOffset) - cameraPos.x;
        if(horizontalCamOffset > 0) {
            if(velX <= 0.1f && velX >=0 && !((targetPos.x + target.getAttributes().bodyWidth / 2) - (windowPos.x + rightR) < -0.1f)) {
                camSpeed = this.camSpeed / 8;
            } else if(!(velX > 0.1f)){
                camSpeed = 0;
            }
            if (dx > camSpeed * delta) {
                dx = camSpeed * delta;
            }
        } else {
            if(velX >= -0.1f && velX <= 0 && !((targetPos.x - target.getAttributes().bodyWidth / 2) - (windowPos.x - leftR) > 0.1f)) {
                camSpeed = this.camSpeed / 8;
            } else if(!(velX < -0.1f)) {
                camSpeed = 0;
            }
            if(dx < -camSpeed * delta) {
                dx = -camSpeed * delta;
            }
        }
        boolean leftBorderCheck = cameraPos.x + dx > leftBorder + width / 3;
        boolean rightBorderCheck = cameraPos.x + dx < rightBorder - width / 3;
        if(!leftBorderCheck) {
            dx = leftBorder + width / 3 - cameraPos.x;
        } else if(!rightBorderCheck) {
            dx = rightBorder - width / 3 - cameraPos.x;
        }

        camSpeed = this.camSpeed / 2;
        float dy = (windowPos.y + verticalCamOffset) - cameraPos.y;
        if(dy < 0) {
            camSpeed = Math.abs(target.getBody().getLinearVelocity().y) * 4f;
            if(0 == camSpeed) {
                camSpeed = this.camSpeed;
            }
        }
        if(dy > camSpeed * delta) dy = camSpeed * delta;
        if(dy < -camSpeed * delta) dy = -camSpeed * delta;
        boolean downBorderCheck = cameraPos.y + dy >= downBorder + height / 3;
        boolean upBorderCheck = cameraPos.y + dy <= upBorder - height / 3;
        if(!downBorderCheck) {
            dy = downBorder + height / 3 - cameraPos.y;
        } else if(!upBorderCheck) {
            dy = upBorder - height / 3 - cameraPos.y;
        }
        if(dx != 0 || dy != 0) {
            camera.translate(dx, dy);
        }
    }

    public void attachToPlatform(Arena platform) {
        float surfaceHeight = platform.surfaceHeight();
        if(surfaceHeight > windowPos.y || surfaceHeight <= windowPos.y - downR) {
            windowPos.y = surfaceHeight;
        }
    }

    @Override
    public void update(float delta) {
        if(null == target) return;

        targetPos = target.getBody().getPosition();
        cameraPos.set(camera.position.x, camera.position.y);

        checkWindow();
        checkCamera(delta);
    }


}
