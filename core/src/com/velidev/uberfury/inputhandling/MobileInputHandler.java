package com.velidev.uberfury.inputhandling;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.physics.ActionHandler;

/**
 * Created by Luka on 1/22/14.
 */
public class MobileInputHandler extends InputHandler {

    private class MovementListener extends InputListener {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if(x > Gdx.graphics.getWidth() / 2) actionHandler.jump(playerID);
            return true;
        }

        @Override
        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            if(touchpad.getKnobPercentX() > 0) {
                actionHandler.stopMovingLeft(playerID);
                actionHandler.moveRight(playerID);
           } else if(touchpad.getKnobPercentX() < 0) {
                actionHandler.stopMovingRight(playerID);
                actionHandler.moveLeft(playerID);
            }
        }

        @Override
        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            actionHandler.stopMovingRight(playerID);
            actionHandler.stopMovingLeft(playerID);
        }
    }

    private Skin touchpadSkin = new Skin();
    private Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle();
    private Drawable touchBackground, touchKnob;
    public Touchpad touchpad;

    public Button jumpButton;

    public MobileInputHandler(UberFury igra, ActionHandler actionHandler) {
        super(igra, actionHandler);
        playerID = 0;
        initTouchpad();
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(x > Gdx.graphics.getWidth() / 2)
                    MobileInputHandler.this.actionHandler.jump(MobileInputHandler.this.playerID);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    public void initTouchpad() {
        touchpadSkin.add("touchBackground", new Texture(Gdx.files.internal("data/touchBackground.png")));
        touchpadSkin.add("touchKnob", new Texture(Gdx.files.internal("data/touchKnob.png")));

        touchBackground = touchpadSkin.getDrawable("touchBackground");
        touchKnob = touchpadSkin.getDrawable("touchKnob");

        touchpadStyle.background = touchBackground;
        touchpadStyle.knob = touchKnob;

        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();

        touchKnob.setMinWidth(screenHeight / 7);
        touchKnob.setMinHeight(screenHeight / 7);

        touchpad = new Touchpad(screenHeight / 12, touchpadStyle);
        touchpad.setBounds(screenWidth / 30, screenHeight / 20, screenHeight / 3, screenHeight / 3);

        touchBackground.setMinHeight(screenHeight / 5);
        touchBackground.setMinWidth(screenHeight / 5);

        addActor(touchpad);

        touchpad.addListener(new MovementListener());
    }

    public void initJumpButton() {
        jumpButton = new Button(touchKnob);
        jumpButton.setPosition(Gdx.graphics.getWidth() * 4 / 5, Gdx.graphics.getHeight() * 1 / 5);
    }

    @Override
    public void update(float delta) {
    }
}
