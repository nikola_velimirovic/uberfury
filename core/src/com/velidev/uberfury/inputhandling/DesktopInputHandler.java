package com.velidev.uberfury.inputhandling;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.physics.ActionHandler;

/**
 * Created by Luka on 1/21/14.
 */
public class DesktopInputHandler extends InputHandler {

    public DesktopInputHandler(UberFury igra, ActionHandler actionHandler) {
        super(igra, actionHandler);
    }

    @Override
    public synchronized boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.A:
                actionHandler.moveLeft(0);
                break;
            case Input.Keys.W:
                actionHandler.lookUp(0);
                break;
            case Input.Keys.S:
                actionHandler.lookDown(0);
                break;
            case Input.Keys.D:
                actionHandler.moveRight(0);
                break;
        }
        if (keycode == Input.Keys.V) {
            actionHandler.slash(0);
        }
        if (keycode == Input.Keys.B) {
            actionHandler.jump(0);
        }
        if (keycode == Input.Keys.N) {
            actionHandler.shoot(0);
        }

        switch (keycode) {
            case Input.Keys.NUMPAD_4:
                actionHandler.moveLeft(1);
                break;
            case Input.Keys.NUMPAD_8:
                actionHandler.lookUp(1);
                break;
            case Input.Keys.NUMPAD_5:
                actionHandler.lookDown(1);
                break;
            case Input.Keys.NUMPAD_6:
                actionHandler.moveRight(1);
                break;
        }
        if (keycode == Input.Keys.I) {
            actionHandler.slash(1);
        }

        if (keycode == Input.Keys.O) {
            actionHandler.jump(1);
        }
        if (keycode == Input.Keys.P) {
            actionHandler.shoot(1);
        }

        if (keycode == Input.Keys.ENTER) {
            Gdx.input.setInputProcessor(igra.mainMenuScreen);
            igra.setScreen(igra.mainMenuScreen);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode) {
            case Input.Keys.A:
                actionHandler.stopMovingLeft(0);
                break;
            case Input.Keys.D:
                actionHandler.stopMovingRight(0);
                break;
            case Input.Keys.W:
                actionHandler.stopLookingUp(0);
                break;
            case Input.Keys.S:
                actionHandler.stopLookingDown(0);
                break;
        }

        switch(keycode) {
            case Input.Keys.NUMPAD_4:
                actionHandler.stopMovingLeft(1);
                break;
            case Input.Keys.NUMPAD_6:
                actionHandler.stopMovingRight(1);
                break;
            case Input.Keys.NUMPAD_8:
                actionHandler.stopLookingUp(1);
                break;
            case Input.Keys.NUMPAD_5:
                actionHandler.stopLookingDown(1);
                break;
        }
        return false;
    }
}
