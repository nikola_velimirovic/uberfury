package com.velidev.uberfury.inputhandling;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.physics.ActionHandler;
import com.velidev.uberfury.physics.Updateable;

/**
 * Created by Luka on 2/8/14.
 */
public abstract class InputHandler extends Stage implements Updateable {
    protected UberFury igra;
    protected ActionHandler actionHandler;
    protected int playerID;

    public InputHandler(UberFury igra, ActionHandler actionHandler) {
        this.igra = igra;
        this.actionHandler = actionHandler;
    }

    public void setActionHandler(ActionHandler actionHandler) {
        this.actionHandler = actionHandler;
    }

    public void setPlayer(int playerID) {
        this.playerID = playerID;
    }

    @Override
    public void update(float delta) {}
}
