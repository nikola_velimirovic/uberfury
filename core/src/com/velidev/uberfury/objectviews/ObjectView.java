package com.velidev.uberfury.objectviews;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.velidev.uberfury.gameobjects.states.ObjectState;
import com.velidev.uberfury.rendering.Renderable;

/**
 * Created by Luka on 1/24/14.
 */
public abstract class ObjectView implements Renderable {
    protected Sprite sprite;//da li sprite ovde ili odvojeno u izvedene klase
    protected ObjectState state;

    public void refresh(ObjectState state) {
        applyState(state);
    }

    protected abstract void applyState(ObjectState state);
}
