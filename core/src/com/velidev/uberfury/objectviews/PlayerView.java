package com.velidev.uberfury.objectviews;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.gameobjects.states.ObjectState;

/**
 * Created by Luka on 1/23/14.
 */
public class PlayerView extends DynamicAnimatedView {
    private Sprite sprite;
    Texture texture = UberFury.assetManager.get("characters/ninja.png", Texture.class);
    int i;
    int p;

    public PlayerView() {

        sprite = new Sprite(texture, 0, 0, 50, 77);
        sprite.setSize(1f, 1f);
    }

    @Override
    public void render() {
        UberFury.batch.begin();
        sprite.draw(UberFury.batch);
        UberFury.batch.end();
    }

    @Override
    public void refresh(ObjectState state) {
        if(i == 7) {
            sprite.setRegion(p * 50, 77, 50, 77);
            i = 0;
            p++;
            if(p == 8) p=1;
        } else {
            i++;
        }

        sprite.setPosition(state.position.x - 0.5f, state.position.y - 0.5f);
    }

    @Override
    protected void applyState(ObjectState state) {

    }

    @Override
    public void update(float delta) {

    }
}
