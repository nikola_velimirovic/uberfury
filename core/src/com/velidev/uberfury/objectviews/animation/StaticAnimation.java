package com.velidev.uberfury.objectviews.animation;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.physics.Updateable;

/**
 * Created by Luka on 4/19/2014.
 */
public class StaticAnimation implements Updateable {
    protected Animation animation;
    protected float stateTime;

    StaticAnimation(float frameDuration, Array<TextureRegion> frames, Animation.PlayMode playMode) {
        animation = new Animation(frameDuration, frames, playMode);
    }

    @Override
    public void update(float delta) {
        stateTime += delta;
    }

    public void resetStateTime() {
        stateTime = 0;
    }

    public TextureRegion currentFrame() {
        return animation.getKeyFrame(stateTime);//mozda treba looping
    }
}
