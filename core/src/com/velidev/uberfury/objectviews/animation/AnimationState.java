package com.velidev.uberfury.objectviews.animation;

/**
 * Created by Luka on 4/19/2014.
 */
public enum AnimationState {
    /*Player animation states*/

    /*Idle*/
    IDLE_LOOKING_RIGHT, IDLE_LOOKING_LEFT, IDLE_LOOKING_UP, IDLE_LOOKING_DOWN,

    /*Running*/
    RUNNING_RIGHT, RUNNING_LEFT, RUNNING_RIGHT_LOOKING_UP, RUNNING_RIGHT_LOOKING_DOWN, RUNNING_LEFT_LOOKING_UP, RUNNING_LEFT_LOOKING_DOWN,

    /*Jumping*/
    JUMPING_LOOKING_RIGHT, JUMPING_LOOKING_LEFT, JUMPING_LOOKING_UP, JUMPING_LOOKING_DOWN,

    /*Sliding down the wall*/
    SLIDING_DOWN_RIGHT_WALL, SLIDING_DOWN_LEFT_WALL
}
