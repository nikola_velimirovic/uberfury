package com.velidev.uberfury.objectviews.animation;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;
import com.velidev.uberfury.physics.Updateable;

/**
 * Created by Luka on 4/19/2014.
 */
public class DynamicAnimation implements Updateable {
    protected ObjectMap<AnimationState, StaticAnimation> animations;
    protected StaticAnimation currentAnimation;

    public DynamicAnimation(int initialCapacity) {
        animations = new ObjectMap<AnimationState, StaticAnimation>(initialCapacity);
    }

    public void setCurrentState(AnimationState state) {
        currentAnimation = animations.get(state);
    }

    public void addAnimationState(AnimationState state, StaticAnimation a) {
        animations.put(state, a);//mozda treba dodati proveru da li vec postoji animacija pod datim kljucem
    }


    public TextureRegion currentFrame() {
        return currentAnimation.currentFrame();
    }

    @Override
    public void update(float delta) {
        currentAnimation.update(delta);
    }
}
