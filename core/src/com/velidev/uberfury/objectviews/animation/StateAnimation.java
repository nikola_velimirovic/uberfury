package com.velidev.uberfury.objectviews.animation;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Luka on 5/3/2014.
 */
public class StateAnimation extends StaticAnimation {
    protected AnimationState state;

    StateAnimation(float frameDuration, Array<TextureRegion> frames, Animation.PlayMode playMode, AnimationState state) {
        super(frameDuration, frames, playMode);
        this.state = state;
    }
}
