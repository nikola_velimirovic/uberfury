package com.velidev.uberfury.objectviews;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.objectviews.animation.StaticAnimation;

/**
 * Created by Luka on 4/19/2014.
 */
public abstract class StaticAnimatedView extends AnimatedView {
    protected StaticAnimation staticAnimation;

    @Override
    public void update(float delta) {
        staticAnimation.update(delta);
    }

    @Override
    public void render() {
        TextureRegion tr = staticAnimation.currentFrame();
        sprite.setRegion(tr);
        sprite.draw(UberFury.batch);
    }
}
