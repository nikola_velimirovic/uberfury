package com.velidev.uberfury.objectviews;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.velidev.uberfury.game.UberFury;
import com.velidev.uberfury.objectviews.animation.DynamicAnimation;

/**
 * Created by Luka on 4/19/2014.
 */
public abstract class DynamicAnimatedView extends AnimatedView {
    protected DynamicAnimation dynamicAnimation;

    public DynamicAnimatedView() {

    }


    @Override
    public void render() {
        dynamicAnimation.update(0);
        TextureRegion tr = dynamicAnimation.currentFrame();
        sprite.setRegion(tr.getU(), tr.getV(), tr.getU2(), tr.getV2());
        sprite.draw(UberFury.batch);
    }
}
