package com.velidev.uberfury.gameobjects;

/**
 * Created by Luka on 1/28/14.
 * Razliciti smerovi prema kojima moze biti okrenut igrac ili nesto drugo.
 */
public enum Direction {
    UP, DOWN, LEFT, RIGHT;
}
