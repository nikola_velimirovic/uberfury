package com.velidev.uberfury.gameobjects;

/**
 * Created by Luka on 4/3/2014.
 */
public class DirectionManager {

    public static Direction reverse(Direction d) {
        switch (d) {
            case UP:
                return Direction.DOWN;
            case DOWN:
                return Direction.UP;
            case LEFT:
                return Direction.RIGHT;
            case RIGHT:
                return Direction.LEFT;
        }
        return d;//ovo mora zbog sintakse
    }
}
