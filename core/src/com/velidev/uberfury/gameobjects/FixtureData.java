package com.velidev.uberfury.gameobjects;

/**
 * Created by Luka on 1/28/14.
 * Sadrzi podatke o tipu objekta kojem pripada fixtura i tipu fixture. Postavlja se kao userData kod fixture, i izvlaci se
 * pri sudaru i proveri sudarenih objekata (fixtura).
 */
public class FixtureData {
    private FixtureType myType;
    private ObjectTypes ownerType;

    public FixtureData(FixtureType fixType, ObjectTypes ownerType) {
        this.myType = fixType;
        this.ownerType = ownerType;
    }

    public FixtureType getType() {return myType;}

    public ObjectTypes getOwnerType() {return ownerType;}
}
