package com.velidev.uberfury.gameobjects.players.player;


import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.RenderableObject;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.gameobjects.FixtureData;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.arena.Arena;
import com.velidev.uberfury.gameobjects.players.player.corpse.Corpse;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.gameobjects.weapons.melee.MeleeWeapon;
import com.velidev.uberfury.gameobjects.weapons.ranged.RangedWeapon;
import com.velidev.uberfury.physics.MovementHandler;
import com.velidev.uberfury.rendering.RenderWorld;
import com.velidev.uberfury.screens.camera.CameraHandler;

/**
 * Created by Luka on 1/22/14.
 * Predstavlja igraca u igri. Player implementira sve akcije koje definise interfejs ActionHandler. LocalWorld je isto ActionHandler
 * i on samo prosledjuje poziv za nekom akcijom odredjenom igracu.
 */
public abstract class Player extends RenderableObject {
    /*Niz mrtvih igraca koje treba izbrisati prilikom sledeceg update-a*/
    public static Array<Player> playersToDie = new Array<Player>();

    /*Definicija za telo igraca, daje poziciju na kojoj se stvara telo*/
    protected static BodyDef bodyDef = new BodyDef();
    static {
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;
    }
    /*Delovi tela...telo (body) igraca se prosledjuje ovim objektima i oni ga popunjavaju fixturama*/
    private Torso torso;
    private Legs legs;
    private MeleeWeapon meleeWeapon;
    private RangedWeapon rangedWeapon;
    private Corpse corpse;
    /*Smer u kojem je trenutno okrenut igrac*/
    private Direction lookDirection = Direction.RIGHT; //ovo treba da se stavlja kod kreiranja
    private boolean lookingUp, lookingDown, requestedToLookUp, requestedToLookDown;
    /*Objekat hit koji igrac prima prilikom udarca, sadrzi podatke o impulsu...*/
    private Hit hit;
    /*Provera da li je mrtav, i kada je poginuo radi ponovnog radjanja*/
    private boolean dead;
    private long timeOfDeath;
    /*Provera da li je igrac izvrsio udarac macem i trenutno se kliza po povrsini usled trzaja*/
    private boolean sliding;
    private long pushedBefore;
    /*Atributi igraca (masa, visina, energija, stit...)*/
    public PlayerAttributes attributes;

    //PRIVREMENO
    public CameraHandler cameraHandler;

    public void setCameraHandler(CameraHandler cameraHandler) {
        this.cameraHandler = cameraHandler;
    }

    /*Kreiranje i stvaranje igraca (summon)*/

    public Player(World b2World, RenderWorld renderWorld) {
        super(b2World, renderWorld);
        attributes = createAttributes();
        torso = createTorso();
        legs = createLegs();
        meleeWeapon = createMeleeWeapon();
        rangedWeapon = createRangedWeapon();
        corpse = createCorpse();
    }

    /*Fabricke metode*/
    protected abstract PlayerAttributes createAttributes();

    protected abstract MeleeWeapon createMeleeWeapon();

    protected abstract RangedWeapon createRangedWeapon();

    protected Torso createTorso() {
        return new Torso(b2World, this);
    }

    protected Legs createLegs() {
        return new Legs(b2World, this);
    }

    protected Corpse createCorpse() {
        return new Corpse(b2World, this);
    }

    /*Dohvatanje delova tela*/
    public Torso getTorso() {
        return torso;
    }

    public Legs getLegs() {
        return legs;
    }

    public MeleeWeapon getMeleeWeapon() {
        return meleeWeapon;
    }

    public RangedWeapon getRangedWeapon() {
        return rangedWeapon;
    }

    /*Metode za stvaranje igraca na ekranu*/
    public void summon(float x, float y) {
        summonBody(x, y);
        torso.summon();
        legs.summon();
        meleeWeapon.summon();
        dead = false;
    }

    private void summonBody(float x, float y) {
        bodyDef.position.set(x, y);
        body = b2World.createBody(bodyDef);
        body.setUserData(this);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.PLAYER;
    }

    @Override
    public void update(float delta) {
        if(dead) {
            checkRespawn();
            return;
        }
        meleeWeapon.update(delta);
        rangedWeapon.update(delta);
        if(sliding) {
            if(System.currentTimeMillis() - pushedBefore >= DefaultAttributes.SLIDING_TIME) {
                MovementHandler.stopXMovement(body);
                sliding = false;
            }
        } else {
            legs.update(delta);
        }
        corpse.update(delta);
    }

    public void checkRespawn() {
        if(System.currentTimeMillis() - timeOfDeath >= DefaultAttributes.RESPAWN_TIME) {
            summon(15f, 1f);
            dead = false;
        }
    }

    /*Akcije*/

    public void moveLeft() {
        if(dead) return;
        legs.moveLeft();
        if(!lookingUp && !lookingDown) {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void moveRight() {
        if(dead) return;
        legs.moveRight();
        if(!lookingUp && !lookingDown) {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void lookUp() {
        if(dead) return;
        if(!lookingDown) {
            lookDirection = Direction.UP;
            lookingUp = true;
            requestedToLookUp = false;
        } else {
            requestedToLookUp = true;
        }
    }

    public void lookDown() {
        if(dead) return;
        if(!lookingUp) {
            lookDirection = Direction.DOWN;
            lookingDown = true;
            requestedToLookDown = false;
        } else {
            requestedToLookDown = true;
        }
    }

    public void stopMovingLeft() {
        if(dead) return;
        legs.stopMovingLeft();
        if(!lookingUp && !lookingDown) {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void stopMovingRight() {
        if(dead) return;
        legs.stopMovingRight();
        if(!lookingUp && !lookingDown) {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void stopLookingUp() {
        if(dead) return;
        lookingUp = false;
        requestedToLookUp = false;
        if(requestedToLookDown) {
            lookDown();
        } else {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void stopLookingDown() {
        if(dead) return;
        lookingDown = false;
        requestedToLookDown = false;
        if(requestedToLookUp) {
            lookUp();
        } else {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void jump() {
        if(dead) return;
        legs.jump();
        if(!lookingDown && !lookingUp) {
            lookDirection = legs.getMoveDirection();
        }
    }

    public void slash() {
        if(dead) return;
        meleeWeapon.slash(lookDirection);
    }

    public void shoot() {
        if(dead) return;
        rangedWeapon.shoot(lookDirection);
    }

    @Override
    public void takeAHit(Hit hit) {
        if(dead) return;

        this.hit = hit;
        playersToDie.add(this);
        dead = true;
        timeOfDeath = System.currentTimeMillis();
    }

    /**Kako box2d pri svakom frejmu obavlja fizicke kalkulacije u metodi step (b2World.step(...)) nije sigurno
     * izbrisati objekat u okviru tog jednog step-a. Metode za proveru sudara beginContact...se pozivaju
     * u svakom step-u, a u njima se definise kada se igrac sudario npr s macem protivnika, pa se on tamo
     * samo doda u listu mrtvih (playersToDie) i onda se nakon step-a prodje kroz listu i pozove ova metoda
     * koja konacno brise telo (body) igraca iz igre i stvara njegov les.*/
    public void die() {
        playersToDie.removeValue(this, true);
        corpse.summon();
        corpse.takeAHit(hit);
        hit.getMeleeWeapon().removeFromRange(this);
        b2World.destroyBody(body);
    }

    /**Poziva se pri upotrebi melee oruzja, moze da se ubaci i kod ranged oruzja, predstavlja mali trzaj
     * koji igrac dozivi. Ukradeno iz samurai gunn*/
    public void takeAPush(float speed, Direction direction) {
        if(dead) return;
        float x = body.getWorldCenter().x;
        float y = body.getWorldCenter().y;
        switch (direction) {
            case UP:
                body.applyLinearImpulse(0, body.getMass()*speed, x, y, true);
                break;
            case DOWN:
                body.applyLinearImpulse(0, -body.getMass()*speed, x, y, true);
                break;
            case LEFT:
                body.applyLinearImpulse(-body.getMass()*speed, 0, x, y, true);
                if(legs.downPlatforms > 0) {
                    sliding = true;
                    pushedBefore = System.currentTimeMillis();
                }
                break;
            case RIGHT:
                body.applyLinearImpulse(body.getMass()*speed, 0, x, y, true);
                if(legs.downPlatforms > 0) {
                    sliding = true;
                    pushedBefore = System.currentTimeMillis();
                }
                break;
        }
    }

    public boolean isGrounded() {
        return legs.isGrounded();
    }

    public PlayerAttributes getAttributes() {
        return attributes;
    }

    /**Pri pocetku sudara igrac ovde samo dalje prosledjuje odredjenom delu koji je sudaren, pa taj deo resava sudar.*/
    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        ObjectTypes ownerType = ((FixtureData) fa.getUserData()).getOwnerType();
        switch (ownerType) {
            case PLAYER:
                break;
            case LEGS:
                legs.beginContact(g, fa, fb, contact, first);
                break;
            case MELEE_WEAPON:
                meleeWeapon.beginContact(g, fa, fb, contact, first);
                break;
            case RANGED_WEAPON:
                break;
        }
    }

    /**Pri kraju dodira igrac ovde samo dalje prosledjuje odredjenom delu koji je bio u dodiru, pa taj deo resava kraj dodira.*/
    @Override
    public void endContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        ObjectTypes ownerType = ((FixtureData) fa.getUserData()).getOwnerType();
        switch (ownerType) {
            case PLAYER:
                break;
            case LEGS:
                legs.endContact(g, fa, fb, contact, first);
                break;
            case MELEE_WEAPON:
                meleeWeapon.endContact(g, fa, fb, contact, first);
                break;
            case RANGED_WEAPON:
                break;
        }
    }
}
