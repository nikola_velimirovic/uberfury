package com.velidev.uberfury.gameobjects.players.player.corpse;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.contact.ContactCategories;

/**
 * Created by Luka on 1/31/14.
 */
public class Limb extends GameObject {
    private SeveredPart severedLimb;

    protected static BodyDef limbBodyDef = new BodyDef();
    static {
        limbBodyDef.type = BodyDef.BodyType.DynamicBody;
        limbBodyDef.fixedRotation = false;
    }
    protected static PolygonShape limbShape = new PolygonShape();
    protected static FixtureDef limbFixDef = new FixtureDef();
    static {
        limbFixDef.shape = limbShape;
        limbFixDef.density = 10f;
        limbFixDef.filter.categoryBits = ContactCategories.CORPSE;
        limbFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.MELEE_WEAPON;
        limbFixDef.friction = 0.2f;
        limbFixDef.restitution = 0.5f;
    }

    private LimbType limbType;

    Player owner;

    public Limb(World b2World, Player owner, LimbType limbType) {
        super(b2World);
        this.owner = owner;

        PlayerAttributes a = owner.attributes;

        this.limbType = limbType;
        float w = a.bodyWidth;
        float h = a.bodyHeight;
        switch (limbType) {
            case LEFT_ARM: case RIGHT_ARM:
                //dve ruke + sirina tela jednake su visini, zato oduzmemo sirinu (h-w)
                // podelimo sa dva da dobijemo jednu ruku, i podelimo jos sa dva da bi dobili radius ruke, zato je podeljeno sa 4
                limbShape.setAsBox(h / 6, h / 40);
                break;
            case LEFT_LEG: case RIGHT_LEG:
                //duzina noge = polovina visine, jos podeljeno sa dva zato se trazi radijus
                limbShape.setAsBox(w / 40, h / 4);
                break;
        }
        severedLimb = new SeveredPart(b2World);
    }

    public void summon() {
        PlayerAttributes a = owner.attributes;

        if(body != null) {
            body.setUserData(Corpse.dummy);
        }
        float x = owner.getBody().getPosition().x;
        float y = owner.getBody().getPosition().y;
        float w = a.bodyWidth;
        float h = a.bodyHeight;
        switch (limbType) {
            case LEFT_ARM:
                //pozicija leve ruke: x koordinata = centar tela - sirina / 2, y koord = centar tela + visina / 4
                limbBodyDef.position.set(x - (w / 2), y + (h / 4));
                break;
            case RIGHT_ARM:
                //pozicija desne ruke: x koordinata = centar tela + sirina / 2, y koord = centar tela + visina / 4
                limbBodyDef.position.set(x + (w / 2), y + (h / 4));
                break;
            case LEFT_LEG:
                limbBodyDef.position.set(x - (w / 4), y - (h / 4));
                break;
            case RIGHT_LEG:
                limbBodyDef.position.set(x + (w / 4), y - (h / 4));
                break;
        }
        body = b2World.createBody(limbBodyDef);
        body.setUserData(severedLimb);
        severedLimb.setBody(body);
        body.createFixture(limbFixDef);
        Corpse.activeBodyParts.add(body);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.CORPSE;
    }

    @Override
    public void takeAHit(Hit hit) {
        Vector2 imp = new Vector2(hit.getImpulse());
        imp.set(hit.getImpulse().x, 4f);
        body.applyLinearImpulse(imp, body.getWorldCenter(), true);
        float angularImpulse = (float) (Math.random() * 0.1f);
        body.applyAngularImpulse(angularImpulse, true);
        hit.getMeleeWeapon().removeFromRange(severedLimb);
    }
}
