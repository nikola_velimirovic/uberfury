package com.velidev.uberfury.gameobjects.players.player;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.velidev.uberfury.gameobjects.*;
import com.velidev.uberfury.physics.MovementHandler;
import com.velidev.uberfury.physics.Updateable;
import com.velidev.uberfury.physics.contact.ContactCategories;

/**
 * Created by Luka on 1/28/14.
 * Klasa koja resava kretanje igraca.
 */
public class Legs extends GameObject implements Updateable {
    /**Podaci za odredjene senzore, tj fixture koje predstavljaju noge. Fixturi se postavi flag isSensor na true
     * i onda se ona ne sudara sa ostalim objektima, (nema fizickog dodira koji postoji izmedju igraca i pod-a npr)
     * vec se samo pozivaju odredjene metode beginContact, endContact...kako bi znali da se nesto nalazi u tom senzoru.
     * Senzor se sudario s necim, etj registrovao nesto. Senzori kod nogu nam govore da li je igrac
     * trenutno u dodiru sa nekom povrsinom i da li moze da skoci. Imamo 3 senzora jedan za skakanje sa horizontalne povrsine,
     * jedan za odbijanje od levog i jedan za odbijanje od desnog zida.*/
    public FixtureData leftSensorData = new FixtureData(FixtureType.LEFT_JUMP_SENSOR, type());
    public FixtureData rightSensorData = new FixtureData(FixtureType.RIGHT_JUMP_SENSOR, type());
    public FixtureData downSensorData = new FixtureData(FixtureType.DOWN_JUMP_SENSOR, type());

    /**Oblik senzora je obican pravougaonik.*/
    protected static PolygonShape sensorShape = new PolygonShape();
    /**Centar odredjenog senzora (postavi se pri kreiranju pojedinacnih senzora) u lokalnim koordinatama*/
    protected static Vector2 sensorCenter = new Vector2();
    /**Definicija fixture pojedinacnih senzora. Definise fixturu (senzor) koja ce biti kreirana, i s kojim ce
     * drugim objektima ta fixtura dolaziti u contact preko category i mask bit-ova ({@link ContactCategories}*/
    protected static FixtureDef sensorFixDef = new FixtureDef();
    static {
        sensorFixDef.isSensor = true;
        sensorFixDef.shape = sensorShape;
        sensorFixDef.filter.categoryBits = ContactCategories.SENSOR;
    }
    //kretanje
    private boolean movingLeft, movingRight, requestedToMoveLeft, requestedToMoveRight;
    private Direction moveDirection = Direction.RIGHT;
    //skakanje
    public int downPlatforms, rightPlatforms, leftPlatforms; // PRIVREMENO PUBLIC
    public boolean jumped;//PRIVREMENO PUBLIC
    private long bounceTime;

    private Player owner;

    public Legs(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
    }

    /**Kreira box2d telo, tj fixture (senzor) za kretanje, nad telom (body) igraca.*/
    public void summon() {
        body = owner.getBody();
        PlayerAttributes a = owner.attributes;

        float sensorWidth = a.bodyWidth * 0.8f;
        float sensorHeight = a.bodyHeight / 4;

        //down sensor
        sensorFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLATFORM;
        sensorCenter.set(0, -a.bodyHeight / 2);
        sensorShape.setAsBox(sensorWidth / 2, sensorHeight / 2, sensorCenter, 0);
        body.createFixture(sensorFixDef).setUserData(downSensorData);
        //left sensor
        sensorFixDef.filter.maskBits = ContactCategories.ARENA;
        sensorCenter.set(-a.bodyWidth / 2, -(a.bodyHeight / 2) * 0.2f);
        sensorShape.setAsBox(sensorWidth / 3, sensorHeight / 2, sensorCenter, 90 * MathUtils.degreesToRadians);
        body.createFixture(sensorFixDef).setUserData(leftSensorData);
        //right sensor
        sensorFixDef.filter.maskBits = ContactCategories.ARENA;
        sensorCenter.set(a.bodyWidth / 2, -(a.bodyHeight / 2) * 0.2f);
        sensorShape.setAsBox(sensorWidth / 3, sensorHeight / 2, sensorCenter, 90 * MathUtils.degreesToRadians);
        body.createFixture(sensorFixDef).setUserData(rightSensorData);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.LEGS;
    }

    @Override
    public void update(float delta) {
        PlayerAttributes a = owner.attributes;

        if(movingLeft) {
            if(System.currentTimeMillis() - bounceTime > a.minBounceDuration) {
                MovementHandler.moveLeft(body, a.moveSpeed);
            }
            checkFriction();
            return;
        }
        if(movingRight) {
            if(System.currentTimeMillis() - bounceTime > a.minBounceDuration) {
                MovementHandler.moveRight(body, a.moveSpeed);
            }
            checkFriction();
            return;
        }

        if(downPlatforms > 0 && !jumped) {
            body.setAwake(false);
        }
    }

    public Direction getMoveDirection() { return moveDirection; }

    /**Sluzi za proveru klizanja niz zid. Ako je npr igrac prislonjen na levi zid i drzi dugme da se krece u levo
     * on ce kliziti niz zid sporije nego slobodan pad, ovde definisemo brzinu kojom klizi.*/
    private void checkFriction() {
        if((leftPlatforms > 0 || rightPlatforms > 0) && 0 == downPlatforms) {
            if(body.getLinearVelocity().y <= 0) {
                MovementHandler.moveDown(body, 1.5f);//napravi konstantu ovde
            }
        }
    }

    public void moveLeft() {
        if(!movingRight) {
            movingLeft = true;
            moveDirection = Direction.LEFT;
            requestedToMoveLeft = false;
        } else {
            requestedToMoveLeft = true;
        }
    }

    public void moveRight() {
        if(!movingLeft) {
            movingRight = true;
            moveDirection = Direction.RIGHT;
            requestedToMoveRight = false;
        } else {
            requestedToMoveRight = true;
        }
    }

    public void stopMovingLeft() {
        movingLeft = false;
        requestedToMoveLeft = false;
        MovementHandler.stopXMovement(body);
        if(requestedToMoveRight) {
            moveRight();
        }
    }

    public void stopMovingRight() {
        movingRight = false;
        requestedToMoveRight = false;
        MovementHandler.stopXMovement(body);
        if(requestedToMoveLeft) {
            moveLeft();
        }
    }

    public void jump() {
        PlayerAttributes a = owner.attributes;

        if(downPlatforms > 0) {
            MovementHandler.jumpUp(body, a.jumpSpeed);
            jumped = true;
            return;
        }

        if(leftPlatforms > 0) {
            bounceTime = System.currentTimeMillis();
            MovementHandler.jumpRight(body, a.jumpSpeed);
            jumped = true;
            return;
        }

        if(rightPlatforms > 0) {
            bounceTime = System.currentTimeMillis();
            MovementHandler.jumpLeft(body, a.jumpSpeed);
            jumped = true;
            return;
        }

    }

    public boolean isGrounded() {
        return downPlatforms > 0;
    }

    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        FixtureType ftype = ((FixtureData) fa.getUserData()).getType();
        switch (ftype) {
            case DOWN_JUMP_SENSOR:
                downPlatforms++;
                MovementHandler.stopXMovement(body);
            break;
            case LEFT_JUMP_SENSOR:
                leftPlatforms++;
                break;
            case RIGHT_JUMP_SENSOR:
                rightPlatforms++;
                break;
        }
    }

    @Override
    public void endContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        FixtureType ftype = ((FixtureData) fa.getUserData()).getType();
        switch (ftype) {
            case LEFT_JUMP_SENSOR:
                leftPlatforms--;
                break;
            case DOWN_JUMP_SENSOR:
                downPlatforms--;
                break;
            case RIGHT_JUMP_SENSOR:
                rightPlatforms--;
                break;
        }
    }
}
