package com.velidev.uberfury.gameobjects.players.player.corpse;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;

/**
 * Created by Luka on 2/1/14.
 */
public class Dummy extends GameObject {
    public Dummy(World b2World) {
        super(b2World);
    }

    @Override
    public ObjectTypes type() {
        return null;
    }
}
