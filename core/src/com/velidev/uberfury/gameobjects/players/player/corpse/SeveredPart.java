package com.velidev.uberfury.gameobjects.players.player.corpse;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.weapons.Hit;

/**
 * Created by Luka on 2/1/14.
 */
public class SeveredPart extends GameObject {
    public SeveredPart(World b2World) {
        super(b2World);
    }

    @Override
    public ObjectTypes type() {
        return null;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public void takeAHit(Hit hit) {
        if(hit.getImpulse().y == 0) {
            hit.getImpulse().set(hit.getImpulse().x, hit.getImpulse().x / 2);
        }
        body.applyLinearImpulse(hit.getImpulse(), body.getWorldCenter(), true);
    }
}
