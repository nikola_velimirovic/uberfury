package com.velidev.uberfury.gameobjects.players.player.corpse;

/**
 * Created by Luka on 1/31/14.
 */
public enum LimbType {
    LEFT_ARM, RIGHT_ARM, LEFT_LEG, RIGHT_LEG;
}
