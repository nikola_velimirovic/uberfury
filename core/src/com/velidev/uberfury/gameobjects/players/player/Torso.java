package com.velidev.uberfury.gameobjects.players.player;

import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.FixtureData;
import com.velidev.uberfury.gameobjects.FixtureType;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.physics.contact.ContactCategories;

/**
 * Created by Luka on 1/30/14.
 */
public class Torso extends GameObject {
    protected PolygonShape bodyShape = new PolygonShape();
    protected FixtureDef bodyFixDef = new FixtureDef();

    protected FixtureData bodyFixData = new FixtureData(FixtureType.PLAYER_BODY, type());

    Player owner;

    public Torso(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
        bodyFixDef.shape = bodyShape;
    }

    public void summon() {
        body = owner.getBody();
        PlayerAttributes a = owner.attributes;

        /*Ovde verovatno treba malo slozeniji oblik (shape) tela*/
        bodyShape.setAsBox(a.bodyWidth / 2, a.bodyHeight / 2);

        bodyFixDef.density = a.bodyFixDensity;
        bodyFixDef.restitution = a.bodyFixRestitution;
        bodyFixDef.friction = a.bodyFixFriction;

        bodyFixDef.filter.categoryBits = ContactCategories.PLAYER;
        bodyFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLATFORM | ContactCategories.MELEE_WEAPON;

        body.createFixture(bodyFixDef).setUserData(bodyFixData);
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.PLAYER;
    }
}
