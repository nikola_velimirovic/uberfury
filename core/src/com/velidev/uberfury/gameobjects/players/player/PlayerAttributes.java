package com.velidev.uberfury.gameobjects.players.player;

/**
 * Created by Luka on 3/31/2014.
 */
public class PlayerAttributes {
    /*Velicina tela*/
    public float bodyWidth = DefaultAttributes.BODY_WIDTH;
    public float bodyHeight = DefaultAttributes.BODY_HEIGHT;

    /*Fixture*/
    public float bodyMass = DefaultAttributes.BODY_MASS;
    public float bodyFixDensity = bodyMass / (bodyWidth * bodyHeight);
    public float bodyFixRestitution = DefaultAttributes.BODY_FIX_RESTITUTION;
    public float bodyFixFriction = DefaultAttributes.BODY_FIX_FRICTION;

    /*Velicine senzora*/
    /*Skok*/
    public float jumpSensorWidth = bodyWidth * 0.8f;
    public float jumpSensorHeight = bodyHeight / 4;
    /*Brzine*/
    public float moveSpeed = DefaultAttributes.MOVE_SPEED;
    public float jumpSpeed = DefaultAttributes.JUMP_SPEED;

    /*Klizanje*/
    public float slidingTime = DefaultAttributes.SLIDING_TIME;

    /*Ponovno radjanje*/
    public float respawnTime = DefaultAttributes.RESPAWN_TIME;

    /*Vreme u vazduhu nakon odbijanja od vertikalnog zida*/
    public long minBounceDuration = DefaultAttributes.MIN_BOUNCE_DURATION;

    /*Energija i stit*/
    public float health;
    public float shield;
}
