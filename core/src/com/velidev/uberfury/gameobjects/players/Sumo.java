package com.velidev.uberfury.gameobjects.players;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.states.ObjectState;
import com.velidev.uberfury.gameobjects.weapons.melee.MeleeWeapon;
import com.velidev.uberfury.gameobjects.weapons.melee.Sword;
import com.velidev.uberfury.gameobjects.weapons.ranged.Bow;
import com.velidev.uberfury.gameobjects.weapons.ranged.RangedWeapon;
import com.velidev.uberfury.rendering.RenderWorld;


/**
 * Created by Luka on 3/22/14.
 */
public class Sumo extends Player {

    public Sumo(World b2World, RenderWorld renderWorld) {
        super(b2World, renderWorld);
    }

    @Override
    protected ObjectState createState() {
        return new ObjectState();
    }

    @Override
    protected PlayerAttributes createAttributes() {
        return new PlayerAttributes();
    }

    @Override
    protected MeleeWeapon createMeleeWeapon() {
        return new Sword(b2World, this);
    }

    @Override
    protected RangedWeapon createRangedWeapon() {
        return new Bow(b2World, this);
    }
}
