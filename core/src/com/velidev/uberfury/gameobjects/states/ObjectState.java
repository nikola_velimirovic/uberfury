package com.velidev.uberfury.gameobjects.states;

import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;

/**
 * Created by Luka on 1/25/14.
 */
public class ObjectState implements Serializable {
    public Vector2 position = new Vector2();
}
