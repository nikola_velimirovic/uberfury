package com.velidev.uberfury.gameobjects.weapons.melee;

/**
 * Created by Luka on 4/2/2014.
 */
public class MeleeAttributes {
    /*Domet*/
    public float rangeLength = MeleeDefault.RANGE_LENGTH;
    public float rangeWidth = MeleeDefault.RANGE_WIDTH;

    /*Brzina udarca*/
    public long slashPeriod = MeleeDefault.SLASH_PERIOD;
    public long timeToReachTheTarget = slashPeriod / 4;
    /*Jacina udarca*/
    public float hitImpulseSpeed = MeleeDefault.HIT_IMPULSE_SPEED;
    /*Jacina trzaja*/
    public float recoilImpulseSpeed = MeleeDefault.RECOIL_IMPULSE_SPEED;
    /*Broj udaraca u kombou*/
    public short maxNumOfComboSlashes = MeleeDefault.MAX_NUM_OF_COMBO_SLASHES;
    /*Minimalno vreme da se udarac racuna u kombu*/
    public long inComboTime = MeleeDefault.IN_COMBO_TIME;
}
