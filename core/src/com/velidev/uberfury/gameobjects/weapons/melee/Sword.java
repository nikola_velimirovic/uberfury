package com.velidev.uberfury.gameobjects.weapons.melee;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.players.player.Player;

/**
 * Created by Luka on 1/27/14.
 */
public class Sword extends MeleeWeapon {
    public Sword(World b2World, Player owner) {
        super(b2World, owner);//izmeni ovo
    }

    @Override
    protected MeleeAttributes createAttributes() {
        return new MeleeAttributes();
    }
}
