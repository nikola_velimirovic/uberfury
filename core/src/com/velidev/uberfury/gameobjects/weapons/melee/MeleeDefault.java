package com.velidev.uberfury.gameobjects.weapons.melee;

/**
 * Created by Luka on 1/30/14.
 */
public class MeleeDefault {
    /*Domet*/
    public static final float RANGE_LENGTH = 1f;
    public static final float RANGE_WIDTH = 1f;
    /*Brzina udarca*/
    public static final long SLASH_PERIOD = 100;
    /*Jacina udarca*/
    public static final float HIT_IMPULSE_SPEED = 10f;
    /*Jacina trzaja*/
    public static final float RECOIL_IMPULSE_SPEED = 7f;
    /*Broj udaraca u kombu*/
    public static final short MAX_NUM_OF_COMBO_SLASHES = 5;
    /*Vreme potrebno da se udarac racuna u kombu*/
    public static final long IN_COMBO_TIME = 4 * SLASH_PERIOD;
}
