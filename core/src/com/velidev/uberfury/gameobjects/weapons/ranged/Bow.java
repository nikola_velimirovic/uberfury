package com.velidev.uberfury.gameobjects.weapons.ranged;

import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Ammo;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Arrow;

/**
 * Created by Luka on 2/2/14.
 */
public class Bow extends RangedWeapon {
    public Bow(World b2World, Player owner) {
        super(b2World, owner);
    }

    @Override
    public Ammo loadAmmo() {
        return new Arrow(b2World);
    }

    @Override
    public void update(float delta) {
        ammo.update(delta);
    }
}
