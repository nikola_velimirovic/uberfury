package com.velidev.uberfury.gameobjects.weapons.ranged;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;
import com.velidev.uberfury.gameobjects.players.player.PlayerAttributes;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.gameobjects.weapons.ranged.ammo.Ammo;

/**
 * Created by Luka on 1/27/14.
 */
public abstract class RangedWeapon extends GameObject {
    public static final float GUN_DISTANCE_FROM_BODY = 0.1f;
    public static final float DEFAULT_BULLET_SPEED = 70f;

    protected Ammo ammo;
    protected Hit hit = new Hit();
    protected float hitImpulseSpeed = DEFAULT_BULLET_SPEED;

    protected Player owner;

    public RangedWeapon(World b2World, Player owner) {
        super(b2World);
        this.owner = owner;
        ammo = loadAmmo();
    }

    public abstract Ammo loadAmmo();

    @Override
    public ObjectTypes type() {
        return ObjectTypes.RANGED_WEAPON;
    }

    public void shoot(Direction direction) {
        PlayerAttributes a = owner.attributes;

        float x = 0f;
        float y = 0f;
        float width = a.bodyWidth;
        float height = a.bodyHeight;
        Body body = owner.getBody();

        switch (direction) {
            case UP:
                x = body.getPosition().x;
                y = body.getPosition().y + height / 2 + GUN_DISTANCE_FROM_BODY;
                hit.setImpulse(0, hitImpulseSpeed);
                break;
            case DOWN:
                x = body.getPosition().x;
                y = body.getPosition().y - height / 2 - GUN_DISTANCE_FROM_BODY;
                hit.setImpulse(0, -hitImpulseSpeed);
                break;
            case LEFT:
                x = body.getPosition().x - width / 2 - GUN_DISTANCE_FROM_BODY;
                y = body.getPosition().y;
                hit.setImpulse(-hitImpulseSpeed, 0);
                break;
            case RIGHT:
                x = body.getPosition().x + width / 2 + GUN_DISTANCE_FROM_BODY;
                y = body.getPosition().y;
                hit.setImpulse(hitImpulseSpeed, 0);
                break;
        }
        ammo.fire(x, y, hit);
    }
}
