package com.velidev.uberfury.gameobjects.weapons.ranged.ammo;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.weapons.Hit;
import com.velidev.uberfury.physics.contact.ContactCategories;

/**
 * Created by Luka on 2/2/14.
 */
public class Arrow extends Ammo {
    private static final float DRAG_CONSTANT = 100f;

    protected static Array<Body> firedArrows = new Array<Body>();

    protected static PolygonShape arrowShape = new PolygonShape();
    protected static Vector2[] arrowVertices = new Vector2[4];
    static {
        for (short i = 0; i < 4; i++) {
            arrowVertices[i] = new Vector2();
        }
        arrowVertices[0].set(-0.7f, 0f);
        arrowVertices[1].set(0f, 0.1f);
        arrowVertices[2].set(0.3f, 0f);
        arrowVertices[3].set(0f, -0.1f);
        arrowShape.set(arrowVertices);
    }
    private static FixtureDef arrowFixDef = new FixtureDef();
    static {
        arrowFixDef.density = 10f;
        arrowFixDef.shape = arrowShape;
        arrowFixDef.restitution = 0.5f;
        arrowFixDef.friction = 0.5f;
        arrowFixDef.filter.categoryBits = ContactCategories.AMMO;
        arrowFixDef.filter.maskBits = ContactCategories.ARENA | ContactCategories.PLAYER;
    }

    private static Vector2 localPointingDirection = new Vector2(1, 0);
    private static Vector2 localArrowTailPosition = new Vector2(-0.7f, 0);

    public Arrow(World b2World) {
        super(b2World);
    }

    /*
    b2Vec2 pointingDirection = arrowBody->GetWorldVector( b2Vec2( 1, 0 ) );
    b2Vec2 flightDirection = arrowBody->GetLinearVelocity();
    float flightSpeed = flightDirection.Normalize();//normalizes and returns length

    float dot = b2Dot( flightDirection, pointingDirection );
    float dragForceMagnitude = (1 - fabs(dot)) * flightSpeed * flightSpeed * dragConstant * arrowBody->GetMass();

    b2Vec2 arrowTailPosition = arrowBody->GetWorldPoint( b2Vec2( -1.4, 0 ) );
    arrowBody->ApplyForce( dragForceMagnitude * -flightDirection, arrowTailPosition );*/

    @Override
    public void update(float delta) {
        for (Body firedArrow : firedArrows) {
            Vector2 pointingDirection = firedArrow.getWorldVector(localPointingDirection);
            Vector2 flightDirection = firedArrow.getLinearVelocity();
            float flightSpeed = flightDirection.nor().len();

            float dot = flightDirection.dot(pointingDirection);
            float dragForceMagnitude = (1 - Math.abs(dot)) * flightSpeed * flightSpeed * DRAG_CONSTANT * firedArrow.getMass();
            Vector2 arrowTailPosition = firedArrow.getWorldPoint(localArrowTailPosition);
            firedArrow.applyForce(flightDirection.scl(-dragForceMagnitude), arrowTailPosition, true);
        }
    }

    @Override
    public void fire(float x, float y, Hit hit) {
        ammoBodyDef.position.set(x + 1f, y + 1f);
        ammoBodyDef.angle = 90 * MathUtils.degreesToRadians;
        body = b2World.createBody(ammoBodyDef);
        body.setUserData(this);
        body.createFixture(arrowFixDef);
        body.setAngularDamping(3f);
        firedArrows.add(body);
        hit.setImpulse(0, 25);
        body.applyLinearImpulse(hit.getImpulse(), body.getWorldCenter(), true);
        System.out.println(body.getWorldCenter());
        System.out.println(body.getLocalCenter());
    }
}
