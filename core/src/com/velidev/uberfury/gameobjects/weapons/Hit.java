package com.velidev.uberfury.gameobjects.weapons;

import com.badlogic.gdx.math.Vector2;
import com.velidev.uberfury.gameobjects.Direction;
import com.velidev.uberfury.gameobjects.weapons.melee.MeleeWeapon;
import com.velidev.uberfury.gameobjects.weapons.ranged.RangedWeapon;

/**
 * Created by Luka on 1/28/14.
 */
public class Hit {
    private HitType hitType;
    private Direction hitDirection;
    private MeleeSwingDirection swingDirection;
    private float impulseSpeed;
    private Vector2 impulse = new Vector2();
    private MeleeWeapon meleeWeapon;
    private RangedWeapon rangedWeapon;

    public HitType getHitType() { return hitType; }

    public void setHitType(HitType hitType) { this.hitType = hitType; }

    public MeleeSwingDirection getSwingDirection() { return swingDirection; }

    public void setSwingDirection(MeleeSwingDirection swingDirection) { this.swingDirection = swingDirection; }

    public void setImpulse(Vector2 impulse) { setImpulse(impulse.x, impulse.y); }

    public void setImpulse(float x, float y) { this.impulse.set(x, y); }

    public Vector2 getImpulse() {
        return impulse;
    }

    public MeleeWeapon getMeleeWeapon() {
        return meleeWeapon;
    }

    public void setMeleeWeapon(MeleeWeapon meleeWeapon) {
        this.meleeWeapon = meleeWeapon;
    }

    public RangedWeapon getRangedWeapon() { return rangedWeapon; }

    public void setRangedWeapon(RangedWeapon rangedWeapon) { this.rangedWeapon = rangedWeapon; }

    public Direction getHitDirection() { return hitDirection; }

    public void setHitDirection(Direction hitDirection) { this.hitDirection = hitDirection; }

    public float getImpulseSpeed() {
        return impulseSpeed;
    }

    public void setImpulseSpeed(float impulseSpeed) {
        this.impulseSpeed = impulseSpeed;
    }
}
