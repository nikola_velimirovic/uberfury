package com.velidev.uberfury.gameobjects.weapons;

/**
 * Created by Luka on 2/1/14.
 */
public enum MeleeSwingDirection {
    HORIZONTAL, VERTICAL;
}
