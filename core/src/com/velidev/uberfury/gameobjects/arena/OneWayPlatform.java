package com.velidev.uberfury.gameobjects.arena;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.velidev.uberfury.gameobjects.GameObject;
import com.velidev.uberfury.gameobjects.ObjectTypes;
import com.velidev.uberfury.gameobjects.players.player.Player;

/**
 * Created by Luka on 7/18/2014.
 */
public class OneWayPlatform extends Arena {
    public static Array<Contact> contacts = new Array<Contact>();

    public OneWayPlatform(World b2World) {
        super(b2World);
        platformHeight = 1;
    }

    @Override
    public ObjectTypes type() {
        return ObjectTypes.ONE_WAY_PLATFORM;
    }

    @Override
    public void preSolve(GameObject g, Fixture fa, Fixture fb, Contact contact, Manifold oldManifold) {
        for(Contact c : OneWayPlatform.contacts) {
            c.setEnabled(false);
        }
    }

    @Override
    public void beginContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        Body playerBody = g.getBody();

        for(Vector2 point : contact.getWorldManifold().getPoints()) {
            Vector2 platformVel = this.body.getLinearVelocityFromWorldPoint(point);
            Vector2 playerVel = playerBody.getLinearVelocityFromWorldPoint(point);

            Vector2 relative = this.body.getLocalVector(playerVel.sub(platformVel));
            if(relative.y < 0.1f) {
                ((Player)g).cameraHandler.attachToPlatform(this);
                ((Player)g).getLegs().jumped = false;
                return;
            } else if(relative.y < 0.1f) {
                Vector2 relativePoint = this.body.getLocalPoint(point);
                float surfaceHeight = platformHeight / 2;
                if(relativePoint.y > surfaceHeight - 0.05) {
                    return;
                }
            }
        }

        contact.setEnabled(false);
        contacts.add(contact);
    }

    @Override
    public void endContact(GameObject g, Fixture fa, Fixture fb, Contact contact, boolean first) {
        contacts.removeValue(contact, false);
    }
}
