package com.velidev.uberfury.tween;

import com.badlogic.gdx.graphics.OrthographicCamera;

public class CameraTweenAccessor {//implements TweenAccessor<OrthographicCamera> {
    public static final int X = 1;
    public static final int Y = 2;
    public static final int XY = 3;
    public static final int ROTATION = 4;

    public int getValues(OrthographicCamera camera, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case X:
                returnValues[0] = camera.position.x;
                return 1;
            case Y:
                returnValues[0] = camera.position.y;
                return 1;
            case XY:
                returnValues[0] = camera.position.x;
                returnValues[1] = camera.position.y;
                return 2;

            case ROTATION:
                returnValues[0] = 0;
                return 1;
        }
        return 0;
    }

    public void setValues(OrthographicCamera camera, int tweenType, float[] newValues) {
        switch (tweenType) {
            case X:
                camera.position.set(newValues[0], camera.position.y, 0);
                break;
            case Y:
                camera.position.set(camera.position.x, newValues[0], 0);
                break;
            case XY:
                camera.position.set(newValues[0], newValues[1], 0);
                break;
            case ROTATION:

                break;
        }
    }
}
